﻿namespace SpiderMonkey
{
    public enum JSType
    {
        Void,
        Object,
        Function,
        String,
        Number,
        Boolean,
        Null,
        XML,
        Limit
    }
}
