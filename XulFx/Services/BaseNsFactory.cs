﻿using Gecko.Interfaces;
using System;
using System.Runtime.InteropServices;

namespace Gecko.Services
{
	public class BaseNsFactory<TFactory>
		where TFactory : nsIFactory, new()
	{
		#region Inner Classes
		public class FactoryDetails
		{
			public Guid classID;
			public nsIFactory factoryInstance;
		}
		#endregion

		// If you are using resharper it will generate warning because
		// BaseNsFactory<T1>._isRegistered and BaseNsFactory<T2>._isRegistered are different fields
		// but for us this is good :)
		// ReSharper disable StaticFieldInGenericType
		private static bool _isRegistered;
		private static string _contractID;
		private static string _factoryTypeName;
		// ReSharper restore StaticFieldInGenericType

		protected BaseNsFactory()
		{

		}

		/// <summary>
		/// Helper method that return the value of the ContractID attribute on this class if it exists.
		/// </summary>
		/// <returns></returns>
		protected static string ReadContractIdAttribute()
		{
			var factoryType = typeof(TFactory);
			var attributes = factoryType.GetCustomAttributes(typeof(ContractIDAttribute), true);
			if (attributes.Length <= 0)
				return null;

			var attribute = (ContractIDAttribute)attributes[0];
			return attribute.ContractID;
		}

		/// <summary>
		/// Registration by default (using ContractIDAttribute)
		/// </summary>
		public static void Register()
		{
			if (_isRegistered) return;

			var factoryType = typeof(TFactory);
			_contractID = ReadContractIdAttribute();
			if (_contractID == null)
				return;

			_factoryTypeName = factoryType.FullName;

			Xpcom.RegisterFactory(factoryType.GUID, _factoryTypeName, _contractID, new TFactory());

			_isRegistered = true;
		}

		/// <summary>
		/// Registration with given contractID
		/// </summary>
		/// <param name="contractID"></param>
		public static void Register(string contractID)
		{
			if (_isRegistered) return;

			var factoryType = typeof(TFactory);
			_factoryTypeName = factoryType.FullName;
			Xpcom.RegisterFactory(factoryType.GUID, _factoryTypeName, _contractID, new TFactory());
			_contractID = contractID;
			_isRegistered = true;
		}

		/// <summary>
		/// Registration with given Factory details
		/// </summary>
		/// <param name="factory"></param>
		public static void Register(FactoryDetails factory)
		{
			Xpcom.RegisterFactory(factory.classID, String.Empty, String.Empty, factory.factoryInstance);
		}

		/// <summary>
		/// If an existing factory already exists, unregister it and return its details.
		/// Returns null if 
		/// </summary>
		public static FactoryDetails Unregister()
		{
			Xpcom.AssertCorrectThread();

			_contractID = ReadContractIdAttribute();
			if (_contractID == null)
				throw new InvalidOperationException("Custom Factory missing ContractId attribute.");

			var factoryType = typeof(TFactory);

			var classID = factoryType.GUID;
			var g = typeof(nsIFactory).GUID;
			var oldFactoryPtr = Xpcom.ComponentManager.Instance.GetClassObject(ref classID, ref g);

			if (oldFactoryPtr == IntPtr.Zero)
				return null;

			var oldFactory = (nsIFactory)Marshal.GetObjectForIUnknown(oldFactoryPtr);
			Xpcom.UnregisterFactory(classID, oldFactory);
			_factoryTypeName = factoryType.FullName;

			_isRegistered = false;

			return new FactoryDetails { classID = classID, factoryInstance = oldFactory };
		}


		/// <summary>
		/// Returns ContractID if registered or null if not registered.
		/// </summary>
		/// <returns></returns>
		public static string GetContractID()
		{
			return _contractID;
		}

		public static string GetFactoryTypeName()
		{
			return _factoryTypeName;
		}
	}
}
