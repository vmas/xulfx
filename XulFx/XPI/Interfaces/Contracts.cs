﻿using System;

namespace Gecko
{
	public static partial class Contracts
	{
		public const string XulfxDOMWindow = "@xulfx/dom/window;1";
		public const string XulfxWebApiPerformance = "@xulfx/webapi/perfomance;1";
	}
}
