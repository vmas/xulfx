@echo off
SET BASEPATH=%~dp0

echo Generating JS-helpers classes...
rem Run XpidlImp.exe (https://bitbucket.org/vmas/xpidlimp)

XpidlImp.exe -L JS -o "%BASEPATH%Content/components/helpers/generated" -n generated -m "%BASEPATH%../map.xml" -M "%BASEPATH%Content/components/helpers/auto.manifest" "%BASEPATH%idl/helpers/auto"
echo.

exit /b 0
