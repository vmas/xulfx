﻿namespace Gecko.Cache
{
	public enum CacheStoragePolicy
	{
		Anywhere = 0,
		InMemory = 1,
		OnDisk = 2,
		OnDiskAsFile = 3,
		Offline = 4
	}
}
