﻿namespace Gecko.Cache
{
	public enum CacheAccessMode
	{
		None = 0,
		Read = 1,
		Write = 2,
		ReadWrite = 3
	}
}
