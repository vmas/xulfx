﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gecko
{
	public enum GeckoExtentionType
	{
		App = 0,
		Extension = 1,
		Skin = 2,
		Bootstrapped = 3
	}
}
