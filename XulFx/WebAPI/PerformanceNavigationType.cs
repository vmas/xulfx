﻿using System;

namespace Gecko.WebAPI
{
	public enum PerformanceNavigationType
	{
		Navigate = 0,
		Reload = 1,
		BackForward = 2,

	}
}
