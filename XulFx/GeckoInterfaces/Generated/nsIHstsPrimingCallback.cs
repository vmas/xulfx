﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIHstsPrimingCallback.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// HSTS priming attempts to prevent mixed-content by looking for the
	/// Strict-Transport-Security header as a signal from the server that it is
	/// safe to upgrade HTTP to HTTPS.
	/// 
	/// Since mixed-content blocking happens very early in the process in AsyncOpen2,
	/// the status of mixed-content blocking is stored in the LoadInfo and then used
	/// to determine whether to send a priming request or not.
	/// 
	/// This interface is implemented by nsHttpChannel so that it can receive the
	/// result of HSTS priming.
	/// </summary>
	[ComImport]
	[Guid("eca6daca-3f2a-4a2a-b3bf-9f24f79bc999")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIHstsPrimingCallback
	{
		
		/// <summary>
		/// HSTS priming has succeeded with an STS header, and the site asserts it is
		/// safe to upgrade the request from HTTP to HTTPS. The request may still be
		/// blocked based on the user&apos;s preferences.
		/// 
		/// May be invoked synchronously if HSTS priming has already been performed
		/// for the host.
		/// </summary>
		/// <param name="aCached">
		/// whether the result was already in the HSTS cache
		/// </param>
		[ThisCallCallingConvention]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnHSTSPrimingSucceeded([MarshalAs(UnmanagedType.U1)] bool aCached);
		
		/// <summary>
		/// HSTS priming has seen no STS header, the request itself has failed,
		/// or some other failure which does not constitute a positive signal that the
		/// site can be upgraded safely to HTTPS. The request may still be allowed
		/// based on the user&apos;s preferences.
		/// 
		/// May be invoked synchronously if HSTS priming has already been performed
		/// for the host.
		/// </summary>
		/// <param name="aError">
		/// The error which caused this failure, or NS_ERROR_CONTENT_BLOCKED
		/// </param>
		/// <param name="aCached">
		/// whether the result was already in the HSTS cache
		/// </param>
		[ThisCallCallingConvention]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnHSTSPrimingFailed(int aError, [MarshalAs(UnmanagedType.U1)] bool aCached);
	}
}
