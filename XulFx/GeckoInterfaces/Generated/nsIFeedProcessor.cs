﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIFeedProcessor.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// An nsIFeedProcessor parses feeds, triggering callbacks based on
	/// their contents.
	/// </summary>
	[ComImport]
	[Guid("8a0b2908-21b0-45d7-b14d-30df0f92afc7")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIFeedProcessor : nsIStreamListener
	{
		
		#region nsIStreamListener Members

		
		#region nsIRequestObserver Members

		
		/// <summary>
		/// Called to signify the beginning of an asynchronous request.
		/// </summary>
		/// <param name="aRequest">
		/// request being observed
		/// </param>
		/// <param name="aContext">
		/// user defined context
		/// 
		/// An exception thrown from onStartRequest has the side-effect of
		/// causing the request to be canceled.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void OnStartRequest([MarshalAs(UnmanagedType.Interface)] nsIRequest aRequest, [MarshalAs(UnmanagedType.Interface)] nsISupports aContext);
		
		/// <summary>
		/// Called to signify the end of an asynchronous request.  This
		/// call is always preceded by a call to onStartRequest.
		/// </summary>
		/// <param name="aRequest">
		/// request being observed
		/// </param>
		/// <param name="aContext">
		/// user defined context
		/// </param>
		/// <param name="aStatusCode">
		/// reason for stopping (NS_OK if completed successfully)
		/// 
		/// An exception thrown from onStopRequest is generally ignored.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void OnStopRequest([MarshalAs(UnmanagedType.Interface)] nsIRequest aRequest, [MarshalAs(UnmanagedType.Interface)] nsISupports aContext, int aStatusCode);
		
		#endregion

		
		/// <summary>
		/// Called when the next chunk of data (corresponding to the request) may
		/// be read without blocking the calling thread.  The onDataAvailable impl
		/// must read exactly |aCount| bytes of data before returning.
		/// </summary>
		/// <param name="aRequest">
		/// request corresponding to the source of the data
		/// </param>
		/// <param name="aContext">
		/// user defined context
		/// </param>
		/// <param name="aInputStream">
		/// input stream containing the data chunk
		/// </param>
		/// <param name="aOffset">
		/// Number of bytes that were sent in previous onDataAvailable calls
		/// for this request. In other words, the sum of all previous count
		/// parameters.
		/// </param>
		/// <param name="aCount">
		/// number of bytes available in the stream
		/// 
		/// NOTE: The aInputStream parameter must implement readSegments.
		/// 
		/// An exception thrown from onDataAvailable has the side-effect of
		/// causing the request to be canceled.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void OnDataAvailable([MarshalAs(UnmanagedType.Interface)] nsIRequest aRequest, [MarshalAs(UnmanagedType.Interface)] nsISupports aContext, [MarshalAs(UnmanagedType.Interface)] nsIInputStream aInputStream, ulong aOffset, uint aCount);
		
		#endregion

		
		/// <summary>
		/// The listener that will respond to feed events.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIFeedResultListener GetListenerAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetListenerAttribute([MarshalAs(UnmanagedType.Interface)] nsIFeedResultListener value);
		
		/// <summary>
		/// Parse a feed from an nsIInputStream.
		/// </summary>
		/// <param name="stream">
		/// The input stream.
		/// </param>
		/// <param name="uri">
		/// The base URI.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ParseFromStream([MarshalAs(UnmanagedType.Interface)] nsIInputStream stream, [MarshalAs(UnmanagedType.Interface)] nsIURI uri);
		
		/// <summary>
		/// Parse a feed from a string.
		/// </summary>
		/// <param name="str">
		/// The string to parse.
		/// </param>
		/// <param name="uri">
		/// The base URI.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ParseFromString([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase str, [MarshalAs(UnmanagedType.Interface)] nsIURI uri);
		
		/// <summary>
		/// Parse a feed asynchronously. The caller must then call the
		/// nsIFeedProcessor&apos;s nsIStreamListener methods to drive the
		/// parse. Do not call the other parse methods during an asynchronous
		/// parse.
		/// </summary>
		/// <param name="requestObserver">
		/// The observer to notify on start/stop. This
		/// argument can be null.
		/// </param>
		/// <param name="uri">
		/// The base URI.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ParseAsync([MarshalAs(UnmanagedType.Interface)] nsIRequestObserver requestObserver, [MarshalAs(UnmanagedType.Interface)] nsIURI uri);
	}
}
