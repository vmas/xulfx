﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIDOMSVGLength.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsIDOMSVGLength ( "2596325c-aed0-487e-96a1-0a6d589b9c6b" ) interface
	// </summary>
	public sealed class nsIDOMSVGLengthConsts
	{
		
		public const ushort SVG_LENGTHTYPE_UNKNOWN = 0;
		
		public const ushort SVG_LENGTHTYPE_NUMBER = 1;
		
		public const ushort SVG_LENGTHTYPE_PERCENTAGE = 2;
		
		public const ushort SVG_LENGTHTYPE_EMS = 3;
		
		public const ushort SVG_LENGTHTYPE_EXS = 4;
		
		public const ushort SVG_LENGTHTYPE_PX = 5;
		
		public const ushort SVG_LENGTHTYPE_CM = 6;
		
		public const ushort SVG_LENGTHTYPE_MM = 7;
		
		public const ushort SVG_LENGTHTYPE_IN = 8;
		
		public const ushort SVG_LENGTHTYPE_PT = 9;
		
		public const ushort SVG_LENGTHTYPE_PC = 10;
	}
	
	[ComImport]
	[Guid("2596325c-aed0-487e-96a1-0a6d589b9c6b")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIDOMSVGLength
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		ushort GetUnitTypeAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		float GetValueAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetValueAttribute(float value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		float GetValueInSpecifiedUnitsAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetValueInSpecifiedUnitsAttribute(float value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetValueAsStringAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetValueAsStringAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void NewValueSpecifiedUnits(ushort unitType, float valueInSpecifiedUnits);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ConvertToSpecifiedUnits(ushort unitType);
	}
}
