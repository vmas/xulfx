﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIInputStreamPump.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// nsIInputStreamPump
	/// 
	/// This interface provides a means to configure and use a input stream pump
	/// instance.  The input stream pump will asynchronously read from an input
	/// stream, and push data to an nsIStreamListener instance.  It utilizes the
	/// current thread&apos;s nsIEventTarget in order to make reading from the stream
	/// asynchronous. A different thread can be used if the pump also implements
	/// nsIThreadRetargetableRequest.
	/// 
	/// If the given stream supports nsIAsyncInputStream, then the stream pump will
	/// call the stream&apos;s AsyncWait method to drive the stream listener.  Otherwise,
	/// the stream will be read on a background thread utilizing the stream
	/// transport service.  More details are provided below.
	/// </summary>
	[ComImport]
	[Guid("400f5468-97e7-4d2b-9c65-a82aecc7ae82")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIInputStreamPump : nsIRequest
	{
		
		#region nsIRequest Members

		
		/// <summary>
		/// The name of the request.  Often this is the URI of the request.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void GetNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase result);
		
		/// <summary>
		/// Indicates whether the request is pending. nsIRequest::isPending is
		/// true when there is an outstanding asynchronous event that will make
		/// the request no longer be pending.  Requests do not necessarily start
		/// out pending; in some cases, requests have to be explicitly initiated
		/// (e.g. nsIChannel implementations are only pending once asyncOpen
		/// returns successfully).
		/// 
		/// Requests can become pending multiple times during their lifetime.
		/// </summary>
		/// <returns>
		/// FALSE if the request has reached completion (e.g., after
		/// OnStopRequest has fired).
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool IsPending();
		
		/// <summary>
		/// The error status associated with the request.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new int GetStatusAttribute();
		
		/// <summary>
		/// Cancels the current request.  This will close any open input or
		/// output streams and terminate any async requests.  Users should
		/// normally pass NS_BINDING_ABORTED, although other errors may also
		/// be passed.  The error passed in will become the value of the
		/// status attribute.
		/// 
		/// Implementations must not send any notifications (e.g. via
		/// nsIRequestObserver) synchronously from this function. Similarly,
		/// removal from the load group (if any) must also happen asynchronously.
		/// 
		/// Requests that use nsIStreamListener must not call onDataAvailable
		/// anymore after cancel has been called.
		/// </summary>
		/// <param name="aStatus">
		/// the reason for canceling this request.
		/// 
		/// NOTE: most nsIRequest implementations expect aStatus to be a
		/// failure code; however, some implementations may allow aStatus to
		/// be a success code such as NS_OK.  In general, aStatus should be
		/// a failure code.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Cancel(int aStatus);
		
		/// <summary>
		/// Suspends the current request.  This may have the effect of closing
		/// any underlying transport (in order to free up resources), although
		/// any open streams remain logically opened and will continue delivering
		/// data when the transport is resumed.
		/// 
		/// Calling cancel() on a suspended request must not send any
		/// notifications (such as onstopRequest) until the request is resumed.
		/// 
		/// NOTE: some implementations are unable to immediately suspend, and
		/// may continue to deliver events already posted to an event queue. In
		/// general, callers should be capable of handling events even after
		/// suspending a request.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Suspend();
		
		/// <summary>
		/// Resumes the current request.  This may have the effect of re-opening
		/// any underlying transport and will resume the delivery of data to
		/// any open streams.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Resume();
		
		/// <summary>
		/// The load group of this request.  While pending, the request is a
		/// member of the load group.  It is the responsibility of the request
		/// to implement this policy.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsILoadGroup GetLoadGroupAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void SetLoadGroupAttribute([MarshalAs(UnmanagedType.Interface)] nsILoadGroup value);
		
		/// <summary>
		/// The load flags of this request.  Bits 0-15 are reserved.
		/// 
		/// When added to a load group, this request&apos;s load flags are merged with
		/// the load flags of the load group.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new uint GetLoadFlagsAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void SetLoadFlagsAttribute(uint value);
		
		#endregion

		
		/// <summary>
		/// Initialize the input stream pump.
		/// </summary>
		/// <param name="aStream">
		/// contains the data to be read.  if the input stream is non-blocking,
		/// then it will be QI&apos;d to nsIAsyncInputStream.  if the QI succeeds
		/// then the stream will be read directly.  otherwise, it will be read
		/// on a background thread using the stream transport service.
		/// </param>
		/// <param name="aStreamPos">
		/// specifies the stream offset from which to start reading.  the
		/// offset value is absolute.  pass -1 to specify the current offset.
		/// NOTE: this parameter is ignored if the underlying stream does not
		/// implement nsISeekableStream.
		/// </param>
		/// <param name="aStreamLen">
		/// specifies how much data to read from the stream.  pass -1 to read
		/// all data available in the stream.
		/// </param>
		/// <param name="aSegmentSize">
		/// if the stream transport service is used, then this parameter
		/// specifies the segment size for the stream transport&apos;s buffer.
		/// pass 0 to specify the default value.
		/// </param>
		/// <param name="aSegmentCount">
		/// if the stream transport service is used, then this parameter
		/// specifies the segment count for the stream transport&apos;s buffer.
		/// pass 0 to specify the default value.
		/// </param>
		/// <param name="aCloseWhenDone">
		/// if true, the input stream will be closed after it has been read.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Init([MarshalAs(UnmanagedType.Interface)] nsIInputStream aStream, long aStreamPos, long aStreamLen, uint aSegmentSize, uint aSegmentCount, [MarshalAs(UnmanagedType.U1)] bool aCloseWhenDone);
		
		/// <summary>
		/// asyncRead causes the input stream to be read in chunks and delivered
		/// asynchronously to the listener via OnDataAvailable.
		/// </summary>
		/// <param name="aListener">
		/// receives notifications.
		/// </param>
		/// <param name="aListenerContext">
		/// passed to listener methods.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AsyncRead([MarshalAs(UnmanagedType.Interface)] nsIStreamListener aListener, [MarshalAs(UnmanagedType.Interface)] nsISupports aListenerContext);
	}
}
