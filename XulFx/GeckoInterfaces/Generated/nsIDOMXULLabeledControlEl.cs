﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIDOMXULLabeledControlEl.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	[ComImport]
	[Guid("c58a159f-e27d-40c8-865a-d4dcfd928f62")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIDOMXULLabeledControlElement : nsIDOMXULControlElement
	{
		
		#region nsIDOMXULControlElement Members

		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool GetDisabledAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void SetDisabledAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new int GetTabIndexAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void SetTabIndexAttribute(int value);
		
		#endregion

		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetCropAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetCropAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetImageAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetImageAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetLabelAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetLabelAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetAccessKeyAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetAccessKeyAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetCommandAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetCommandAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase value);
	}
}
