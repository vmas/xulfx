﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIHttpActivityObserver.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsIHttpActivityObserver ( "412880c8-6c36-48d8-bf8f-84f91f892503" ) interface
	// </summary>
	public sealed class nsIHttpActivityObserverConsts
	{
		
		public const uint ACTIVITY_TYPE_SOCKET_TRANSPORT = 1;
		
		public const uint ACTIVITY_TYPE_HTTP_TRANSACTION = 2;
		
		public const uint ACTIVITY_SUBTYPE_REQUEST_HEADER = 20481;
		
		public const uint ACTIVITY_SUBTYPE_REQUEST_BODY_SENT = 20482;
		
		public const uint ACTIVITY_SUBTYPE_RESPONSE_START = 20483;
		
		public const uint ACTIVITY_SUBTYPE_RESPONSE_HEADER = 20484;
		
		public const uint ACTIVITY_SUBTYPE_RESPONSE_COMPLETE = 20485;
		
		public const uint ACTIVITY_SUBTYPE_TRANSACTION_CLOSE = 20486;
	}
	
	/// <summary>
	/// nsIHttpActivityObserver
	/// 
	/// This interface provides a way for http activities to be reported
	/// to observers.
	/// </summary>
	[ComImport]
	[Guid("412880c8-6c36-48d8-bf8f-84f91f892503")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIHttpActivityObserver
	{
		
		/// <summary>
		/// observe activity from the http transport
		/// </summary>
		/// <param name="aHttpChannel">
		/// nsISupports interface for the the http channel that
		/// generated this activity
		/// </param>
		/// <param name="aActivityType">
		/// The value of this aActivityType will be one of
		/// ACTIVITY_TYPE_SOCKET_TRANSPORT or
		/// ACTIVITY_TYPE_HTTP_TRANSACTION
		/// </param>
		/// <param name="aActivitySubtype">
		/// The value of this aActivitySubtype, will be depend
		/// on the value of aActivityType. When aActivityType
		/// is ACTIVITY_TYPE_SOCKET_TRANSPORT
		/// aActivitySubtype will be one of the
		/// nsISocketTransport::STATUS_???? values defined in
		/// nsISocketTransport.idl
		/// OR when aActivityType
		/// is ACTIVITY_TYPE_HTTP_TRANSACTION
		/// aActivitySubtype will be one of the
		/// nsIHttpActivityObserver::ACTIVITY_SUBTYPE_???? values
		/// defined below
		/// </param>
		/// <param name="aTimestamp">
		/// microseconds past the epoch of Jan 1, 1970
		/// </param>
		/// <param name="aExtraSizeData">
		/// Any extra size data optionally available with
		/// this activity
		/// </param>
		/// <param name="aExtraStringData">
		/// Any extra string data optionally available with
		/// this activity
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ObserveActivity([MarshalAs(UnmanagedType.Interface)] nsISupports aHttpChannel, uint aActivityType, uint aActivitySubtype, ulong aTimestamp, ulong aExtraSizeData, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aExtraStringData);
		
		/// <summary>
		/// This attribute is true when this interface is active and should
		/// observe http activities. When false, observeActivity() should not
		/// be called. It is present for compatibility reasons and should be
		/// implemented only by nsHttpActivityDistributor.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetIsActiveAttribute();
	}
	
	/// <summary>
	/// nsIHttpActivityDistributor
	/// 
	/// This interface provides a way to register and unregister observers to the
	/// http activities.
	/// </summary>
	[ComImport]
	[Guid("7c512cb8-582a-4625-b5b6-8639755271b5")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIHttpActivityDistributor : nsIHttpActivityObserver
	{
		
		#region nsIHttpActivityObserver Members

		
		/// <summary>
		/// observe activity from the http transport
		/// </summary>
		/// <param name="aHttpChannel">
		/// nsISupports interface for the the http channel that
		/// generated this activity
		/// </param>
		/// <param name="aActivityType">
		/// The value of this aActivityType will be one of
		/// ACTIVITY_TYPE_SOCKET_TRANSPORT or
		/// ACTIVITY_TYPE_HTTP_TRANSACTION
		/// </param>
		/// <param name="aActivitySubtype">
		/// The value of this aActivitySubtype, will be depend
		/// on the value of aActivityType. When aActivityType
		/// is ACTIVITY_TYPE_SOCKET_TRANSPORT
		/// aActivitySubtype will be one of the
		/// nsISocketTransport::STATUS_???? values defined in
		/// nsISocketTransport.idl
		/// OR when aActivityType
		/// is ACTIVITY_TYPE_HTTP_TRANSACTION
		/// aActivitySubtype will be one of the
		/// nsIHttpActivityObserver::ACTIVITY_SUBTYPE_???? values
		/// defined below
		/// </param>
		/// <param name="aTimestamp">
		/// microseconds past the epoch of Jan 1, 1970
		/// </param>
		/// <param name="aExtraSizeData">
		/// Any extra size data optionally available with
		/// this activity
		/// </param>
		/// <param name="aExtraStringData">
		/// Any extra string data optionally available with
		/// this activity
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void ObserveActivity([MarshalAs(UnmanagedType.Interface)] nsISupports aHttpChannel, uint aActivityType, uint aActivitySubtype, ulong aTimestamp, ulong aExtraSizeData, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aExtraStringData);
		
		/// <summary>
		/// This attribute is true when this interface is active and should
		/// observe http activities. When false, observeActivity() should not
		/// be called. It is present for compatibility reasons and should be
		/// implemented only by nsHttpActivityDistributor.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool GetIsActiveAttribute();
		
		#endregion

		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AddObserver([MarshalAs(UnmanagedType.Interface)] nsIHttpActivityObserver aObserver);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void RemoveObserver([MarshalAs(UnmanagedType.Interface)] nsIHttpActivityObserver aObserver);
	}
}
