﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIHttpAuthManager.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// nsIHttpAuthManager
	/// 
	/// This service provides access to cached HTTP authentication
	/// user credentials (domain, username, password) for sites
	/// visited during the current browser session.
	/// 
	/// This interface exists to provide other HTTP stacks with the
	/// ability to share HTTP authentication credentials with Necko.
	/// This is currently used by the Java plugin (version 1.5 and
	/// higher) to avoid duplicate authentication prompts when the
	/// Java client fetches content from a HTTP site that the user
	/// has already logged into.
	/// </summary>
	[ComImport]
	[Guid("54f90444-c52b-4d2d-8916-c59a2bb25938")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIHttpAuthManager
	{
		
		/// <summary>
		/// Lookup auth identity.
		/// </summary>
		/// <param name="aScheme">
		/// the URL scheme (e.g., &quot;http&quot;).  NOTE: for proxy authentication,
		/// this should be &quot;http&quot; (this includes authentication for CONNECT
		/// tunneling).
		/// </param>
		/// <param name="aHost">
		/// the host of the server issuing a challenge (ASCII only).
		/// </param>
		/// <param name="aPort">
		/// the port of the server issuing a challenge.
		/// </param>
		/// <param name="aAuthType">
		/// optional string identifying auth type used (e.g., &quot;basic&quot;)
		/// </param>
		/// <param name="aRealm">
		/// optional string identifying auth realm.
		/// </param>
		/// <param name="aPath">
		/// optional string identifying auth path. empty for proxy auth.
		/// </param>
		/// <param name="aUserDomain">
		/// return value containing user domain.
		/// </param>
		/// <param name="aUserName">
		/// return value containing user name.
		/// </param>
		/// <param name="aUserPassword">
		/// return value containing user password.
		/// </param>
		/// <param name="aIsPrivate">
		/// whether to look up a private or public identity (they are
		/// stored separately, for use by private browsing)
		/// </param>
		/// <param name="aPrincipal">
		/// the principal from which to derive information about which
		/// app/mozbrowser is in use for this request
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetAuthIdentity([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aScheme, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aHost, int aPort, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aAuthType, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aRealm, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aPath, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aUserDomain, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aUserName, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aUserPassword, [MarshalAs(UnmanagedType.U1)] bool aIsPrivate, [MarshalAs(UnmanagedType.Interface)] nsIPrincipal aPrincipal);
		
		/// <summary>
		/// Store auth identity.
		/// </summary>
		/// <param name="aScheme">
		/// the URL scheme (e.g., &quot;http&quot;).  NOTE: for proxy authentication,
		/// this should be &quot;http&quot; (this includes authentication for CONNECT
		/// tunneling).
		/// </param>
		/// <param name="aHost">
		/// the host of the server issuing a challenge (ASCII only).
		/// </param>
		/// <param name="aPort">
		/// the port of the server issuing a challenge.
		/// </param>
		/// <param name="aAuthType">
		/// optional string identifying auth type used (e.g., &quot;basic&quot;)
		/// </param>
		/// <param name="aRealm">
		/// optional string identifying auth realm.
		/// </param>
		/// <param name="aPath">
		/// optional string identifying auth path. empty for proxy auth.
		/// </param>
		/// <param name="aUserDomain">
		/// optional string containing user domain.
		/// </param>
		/// <param name="aUserName">
		/// optional string containing user name.
		/// </param>
		/// <param name="aUserPassword">
		/// optional string containing user password.
		/// </param>
		/// <param name="aIsPrivate">
		/// whether to store a private or public identity (they are
		/// stored separately, for use by private browsing)
		/// </param>
		/// <param name="aPrincipal">
		/// the principal from which to derive information about which
		/// app/mozbrowser is in use for this request
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetAuthIdentity([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aScheme, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aHost, int aPort, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aAuthType, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aRealm, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aPath, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aUserDomain, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aUserName, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aUserPassword, [MarshalAs(UnmanagedType.U1)] bool aIsPrivate, [MarshalAs(UnmanagedType.Interface)] nsIPrincipal aPrincipal);
		
		/// <summary>
		/// Clear all auth cache.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ClearAll();
	}
}
