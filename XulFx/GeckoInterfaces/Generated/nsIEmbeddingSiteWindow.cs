﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIEmbeddingSiteWindow.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsIEmbeddingSiteWindow ( "0b976267-4aaa-4f36-a2d4-27b5ca8d73bb" ) interface
	// </summary>
	public sealed class nsIEmbeddingSiteWindowConsts
	{
		
		public const uint DIM_FLAGS_POSITION = 1;
		
		public const uint DIM_FLAGS_SIZE_INNER = 2;
		
		public const uint DIM_FLAGS_SIZE_OUTER = 4;
		
		public const uint DIM_FLAGS_IGNORE_X = 8;
		
		public const uint DIM_FLAGS_IGNORE_Y = 16;
		
		public const uint DIM_FLAGS_IGNORE_CX = 32;
		
		public const uint DIM_FLAGS_IGNORE_CY = 64;
	}
	
	/// <summary>
	/// The nsIEmbeddingSiteWindow is implemented by the embedder to provide
	/// Gecko with the means to call up to the host to resize the window,
	/// hide or show it and set/get its title.
	/// </summary>
	[ComImport]
	[Guid("0b976267-4aaa-4f36-a2d4-27b5ca8d73bb")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIEmbeddingSiteWindow
	{
		
		/// <summary>
		/// Sets the dimensions for the window; the position &amp; size. The
		/// flags to indicate what the caller wants to set and whether the size
		/// refers to the inner or outer area. The inner area refers to just
		/// the embedded area, wheras the outer area can also include any
		/// surrounding chrome, window frame, title bar, and so on.
		/// </summary>
		/// <param name="flags">
		/// Combination of position, inner and outer size flags.
		/// The ignore flags are telling the parent to use the
		/// current values for those dimensions and ignore the
		/// corresponding parameters the child sends.
		/// </param>
		/// <param name="x">
		/// Left hand corner of the outer area.
		/// </param>
		/// <param name="y">
		/// Top corner of the outer area.
		/// </param>
		/// <param name="cx">
		/// Width of the inner or outer area.
		/// </param>
		/// <param name="cy">
		/// Height of the inner or outer area.
		/// </param>
		/// <returns>
		/// &lt;code&gt;NS_OK&lt;/code&gt; if operation was performed correctly;
		/// &lt;code&gt;NS_ERROR_UNEXPECTED&lt;/code&gt; if window could not be
		/// destroyed;
		/// &lt;code&gt;NS_ERROR_INVALID_ARG&lt;/code&gt; for bad flag combination
		/// or illegal dimensions.
		/// </returns>
		/// <remarks>
		/// See DIM_FLAGS_SIZE_INNER
		/// </remarks>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetDimensions(uint flags, int x, int y, int cx, int cy);
		
		/// <summary>
		/// Gets the dimensions of the window. The caller may pass
		/// &lt;CODE&gt;nullptr&lt;/CODE&gt; for any value it is uninterested in receiving.
		/// </summary>
		/// <param name="flags">
		/// Combination of position, inner and outer size flag .
		/// </param>
		/// <param name="x">
		/// Left hand corner of the outer area; or &lt;CODE&gt;nullptr&lt;/CODE&gt;.
		/// </param>
		/// <param name="y">
		/// Top corner of the outer area; or &lt;CODE&gt;nullptr&lt;/CODE&gt;.
		/// </param>
		/// <param name="cx">
		/// Width of the inner or outer area; or &lt;CODE&gt;nullptr&lt;/CODE&gt;.
		/// </param>
		/// <param name="cy">
		/// Height of the inner or outer area; or &lt;CODE&gt;nullptr&lt;/CODE&gt;.
		/// </param>
		/// <remarks>
		/// See DIM_FLAGS_SIZE_INNER
		/// </remarks>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		unsafe void GetDimensions(uint flags, int* x, int* y, int* cx, int* cy);
		
		/// <summary>
		/// Give the window focus.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetFocus();
		
		/// <summary>
		/// Visibility of the window.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetVisibilityAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetVisibilityAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		/// <summary>
		/// Title of the window.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))]
		string GetTitleAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetTitleAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string value);
		
		/// <summary>
		/// Native window for the site&apos;s window. The implementor should copy the
		/// native window object into the address supplied by the caller. The
		/// type of the native window that the address refers to is  platform
		/// and OS specific as follows:
		/// 
		/// &lt;ul&gt;
		/// &lt;li&gt;On Win32 it is an &lt;CODE&gt;HWND&lt;/CODE&gt;.&lt;/li&gt;
		/// &lt;li&gt;On MacOS this is a &lt;CODE&gt;WindowPtr&lt;/CODE&gt;.&lt;/li&gt;
		/// &lt;li&gt;On GTK this is a &lt;CODE&gt;GtkWidget*&lt;/CODE&gt;.&lt;/li&gt;
		/// &lt;/ul&gt;
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr GetSiteWindowAttribute();
		
		/// <summary>
		/// Blur the window. This should unfocus the window and send an onblur event.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Blur();
	}
}
