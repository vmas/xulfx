﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIXPConnect.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// 
	/// </summary>
	[ComImport]
	[Guid("73e6ff4a-ab99-4d99-ac00-ba39ccb8e4d7")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIXPConnectJSObjectHolder
	{
		
		[PreserveSig]
		[ThisCallCallingConvention]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr GetJSObject();
	}
	
	[ComImport]
	[Guid("e787be29-db5d-4a45-a3d6-1de1d6b85c30")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIXPConnectWrappedNative : nsIXPConnectJSObjectHolder
	{
		
		#region nsIXPConnectJSObjectHolder Members

		
		[PreserveSig]
		[ThisCallCallingConvention]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new System.IntPtr GetJSObject();
		
		#endregion

		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports GetNativeAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr GetJSObjectPrototypeAttribute();
		
		/// <summary>
		/// These are here as an aid to nsIXPCScriptable implementors
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIInterfaceInfo FindInterfaceWithMember(System.IntPtr nameID);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIInterfaceInfo FindInterfaceWithName(System.IntPtr nameID);
		
		[PreserveSig]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool HasNativeMember(System.IntPtr name);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void DebugDump(short depth);
	}
	
	[ComImport]
	[Guid("3a01b0d6-074b-49ed-bac3-08c76366cae4")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIXPConnectWrappedJS : nsIXPConnectJSObjectHolder
	{
		
		#region nsIXPConnectJSObjectHolder Members

		
		[PreserveSig]
		[ThisCallCallingConvention]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new System.IntPtr GetJSObject();
		
		#endregion

		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIInterfaceInfo GetInterfaceInfoAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Struct)]
		UUID GetInterfaceIIDAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void DebugDump(short depth);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr AggregatedQueryInterface(ref System.Guid uuid);
	}
	
	[ComImport]
	[Guid("c02a0ce6-275f-4ea1-9c23-08494898b070")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIXPConnectWrappedJSUnmarkGray : nsIXPConnectWrappedJS
	{
		
		#region nsIXPConnectWrappedJS Members

		
		#region nsIXPConnectJSObjectHolder Members

		
		[PreserveSig]
		[ThisCallCallingConvention]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new System.IntPtr GetJSObject();
		
		#endregion

		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsIInterfaceInfo GetInterfaceInfoAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Struct)]
		new UUID GetInterfaceIIDAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void DebugDump(short depth);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new System.IntPtr AggregatedQueryInterface(ref System.Guid uuid);
		
		#endregion

	}
	
	/// <summary>
	/// This is a sort of a placeholder interface. It is not intended to be
	/// implemented. It exists to give the nsIXPCSecurityManager an iid on
	/// which to gate a specific activity in XPConnect.
	/// 
	/// That activity is...
	/// 
	/// When JavaScript code uses a component that is itself implemented in
	/// JavaScript then XPConnect will build a wrapper rather than directly
	/// expose the JSObject of the component. This allows components implemented
	/// in JavaScript to &apos;look&apos; just like any other xpcom component (from the
	/// perspective of the JavaScript caller). This insulates the component from
	/// the caller and hides any properties or methods that are not part of the
	/// interface as declared in xpidl. Usually this is a good thing.
	/// 
	/// However, in some cases it is useful to allow the JS caller access to the
	/// JS component&apos;s underlying implementation. In order to facilitate this
	/// XPConnect supports the &apos;wrappedJSObject&apos; property. The caller code can do:
	/// 
	/// &apos;foo&apos; is some xpcom component (that might be implemented in JS).
	/// try {
	/// var bar = foo.wrappedJSObject;
	/// if(bar) {
	/// bar is the underlying JSObject. Do stuff with it here.
	/// }
	/// } catch(e) {
	/// security exception?
	/// }
	/// 
	/// Recall that &apos;foo&apos; above is an XPConnect wrapper, not the underlying JS
	/// object. The property get &quot;foo.wrappedJSObject&quot; will only succeed if three
	/// conditions are met:
	/// 
	/// 1) &apos;foo&apos; really is an XPConnect wrapper around a JSObject.
	/// 2) The underlying JSObject actually implements a &quot;wrappedJSObject&quot;
	/// property that returns a JSObject. This is called by XPConnect. This
	/// restriction allows wrapped objects to only allow access to the underlying
	/// JSObject if they choose to do so. Ususally this just means that &apos;foo&apos;
	/// would have a property tht looks like:
	/// this.wrappedJSObject = this.
	/// 3) The implemementation of nsIXPCSecurityManager (if installed) allows
	/// a property get on the interface below. Although the JSObject need not
	/// implement &apos;nsIXPCWrappedJSObjectGetter&apos;, XPConnect will ask the
	/// security manager if it is OK for the caller to access the only method
	/// in nsIXPCWrappedJSObjectGetter before allowing the activity. This fits
	/// in with the security manager paradigm and makes control over accessing
	/// the property on this interface the control factor for getting the
	/// underlying wrapped JSObject of a JS component from JS code.
	/// 
	/// Notes:
	/// 
	/// a) If &apos;foo&apos; above were the underlying JSObject and not a wrapper at all,
	/// then this all just works and XPConnect is not part of the picture at all.
	/// b) One might ask why &apos;foo&apos; should not just implement an interface through
	/// which callers might get at the underlying object. There are three reasons:
	/// i)   XPConnect would still have to do magic since JSObject is not a
	/// scriptable type.
	/// ii)  JS Components might use aggregation (like C++ objects) and have
	/// different JSObjects for different interfaces &apos;within&apos; an aggregate
	/// object. But, using an additional interface only allows returning one
	/// underlying JSObject. However, this allows for the possibility that
	/// each of the aggregte JSObjects could return something different.
	/// Note that one might do: this.wrappedJSObject = someOtherObject;
	/// iii) Avoiding the explicit interface makes it easier for both the caller
	/// and the component.
	/// 
	/// Anyway, some future implementation of nsIXPCSecurityManager might want
	/// do special processing on &apos;nsIXPCSecurityManager::CanGetProperty&apos; when
	/// the interface id is that of nsIXPCWrappedJSObjectGetter.
	/// </summary>
	[ComImport]
	[Guid("254bb2e0-6439-11d4-8fe0-0010a4e73d9a")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIXPCWrappedJSObjectGetter
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports GetNeverCalledAttribute();
	}
	
	/// <summary>
	/// 
	/// </summary>
	[ComImport]
	[Guid("f5f84b70-92eb-41f1-a1dd-2eaac0ed564c")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIXPCFunctionThisTranslator
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports TranslateThis([MarshalAs(UnmanagedType.Interface)] nsISupports aInitialThis);
	}
	
	// <summary>
	// Constants for nsIXPConnect ( "768507b5-b981-40c7-8276-f6a1da502a24" ) interface
	// </summary>
	public sealed class nsIXPConnectConsts
	{
		
		public const uint INIT_JS_STANDARD_CLASSES = (1 << 0);
		
		public const uint DONT_FIRE_ONNEWGLOBALHOOK = (1 << 1);
		
		public const uint OMIT_COMPONENTS_OBJECT = (1 << 2);
	}
	
	[ComImport]
	[Guid("768507b5-b981-40c7-8276-f6a1da502a24")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIXPConnect
	{
		
		/// <summary>
		/// Creates a new global object using the given aCOMObj as the global
		/// object. The object will be set up according to the flags (defined
		/// below). If you do not pass INIT_JS_STANDARD_CLASSES, then aCOMObj
		/// must implement nsIXPCScriptable so it can resolve the standard
		/// classes when asked by the JS engine.
		/// </summary>
		/// <param name="aJSContext">
		/// the context to use while creating the global object.
		/// </param>
		/// <param name="aCOMObj">
		/// the native object that represents the global object.
		/// </param>
		/// <param name="aPrincipal">
		/// the principal of the code that will run in this
		/// compartment. Can be null if not on the main thread.
		/// </param>
		/// <param name="aFlags">
		/// one of the flags below specifying what options this
		/// global object wants.
		/// </param>
		/// <param name="aOptions">
		/// JSAPI-specific options for the new compartment.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIXPConnectJSObjectHolder InitClassesWithNewWrappedGlobal(System.IntPtr aJSContext, [MarshalAs(UnmanagedType.Interface)] nsISupports aCOMObj, [MarshalAs(UnmanagedType.Interface)] nsIPrincipal aPrincipal, uint aFlags, System.IntPtr aOptions);
		
		/// <summary>
		/// wrapNative will create a new JSObject or return an existing one.
		/// 
		/// This method now correctly deals with cases where the passed in xpcom
		/// object already has an associated JSObject for the cases:
		/// 1) The xpcom object has already been wrapped for use in the same scope
		/// as an nsIXPConnectWrappedNative.
		/// 2) The xpcom object is in fact a nsIXPConnectWrappedJS and thus already
		/// has an underlying JSObject.
		/// 
		/// It *might* be possible to QueryInterface the nsIXPConnectJSObjectHolder
		/// returned by the method into a nsIXPConnectWrappedNative or a
		/// nsIXPConnectWrappedJS.
		/// 
		/// This method will never wrap the JSObject involved in an
		/// XPCNativeWrapper before returning.
		/// 
		/// Returns:
		/// success:
		/// NS_OK
		/// failure:
		/// NS_ERROR_XPC_BAD_CONVERT_NATIVE
		/// NS_ERROR_XPC_CANT_GET_JSOBJECT_OF_DOM_OBJECT
		/// NS_ERROR_FAILURE
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr WrapNative(System.IntPtr aJSContext, System.IntPtr aScope, [MarshalAs(UnmanagedType.Interface)] nsISupports aCOMObj, ref System.Guid aIID);
		
		/// <summary>
		/// Same as wrapNative, but it returns the JSObject in an nsIXPConnectJSObjectHolder.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIXPConnectJSObjectHolder WrapNativeHolder(System.IntPtr aJSContext, System.IntPtr aScope, [MarshalAs(UnmanagedType.Interface)] nsISupports aCOMObj, ref System.Guid aIID);
		
		/// <summary>
		/// Same as wrapNative, but it returns the JSObject in aVal. C++ callers
		/// must ensure that aVal is rooted.
		/// aIID may be null, it means the same as passing in
		/// &amp;NS_GET_IID(nsISupports) but when passing in null certain shortcuts
		/// can be taken because we know without comparing IIDs that the caller is
		/// asking for an nsISupports wrapper.
		/// If aAllowWrapper, then the returned value will be wrapped in the proper
		/// type of security wrapper on top of the XPCWrappedNative (if needed).
		/// This method doesn&apos;t push aJSContext on the context stack, so the caller
		/// is required to push it if the top of the context stack is not equal to
		/// aJSContext.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void WrapNativeToJSVal(System.IntPtr aJSContext, System.IntPtr aScope, [MarshalAs(UnmanagedType.Interface)] nsISupports aCOMObj, System.IntPtr aCache, [MarshalAs(UnmanagedType.Struct)] [In] [Out] UUID aIID, [MarshalAs(UnmanagedType.U1)] bool aAllowWrapper, ref JSVal aVal);
		
		/// <summary>
		/// wrapJS will yield a new or previously existing xpcom interface pointer
		/// to represent the JSObject passed in.
		/// 
		/// This method now correctly deals with cases where the passed in JSObject
		/// already has an associated xpcom interface for the cases:
		/// 1) The JSObject has already been wrapped as a nsIXPConnectWrappedJS.
		/// 2) The JSObject is in fact a nsIXPConnectWrappedNative and thus already
		/// has an underlying xpcom object.
		/// 3) The JSObject is of a jsclass which supports getting the nsISupports
		/// from the JSObject directly. This is used for idlc style objects
		/// (e.g. DOM objects).
		/// 
		/// It *might* be possible to QueryInterface the resulting interface pointer
		/// to nsIXPConnectWrappedJS.
		/// 
		/// Returns:
		/// success:
		/// NS_OK
		/// failure:
		/// NS_ERROR_XPC_BAD_CONVERT_JS
		/// NS_ERROR_FAILURE
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr WrapJS(System.IntPtr aJSContext, System.IntPtr aJSObj, ref System.Guid aIID);
		
		/// <summary>
		/// Wraps the given jsval in a nsIVariant and returns the new variant.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIVariant JSValToVariant(System.IntPtr cx, ref JSVal aJSVal);
		
		/// <summary>
		/// This only succeeds if the JSObject is a nsIXPConnectWrappedNative.
		/// A new wrapper is *never* constructed.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIXPConnectWrappedNative GetWrappedNativeOfJSObject(System.IntPtr aJSContext, System.IntPtr aJSObj);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIStackFrame GetCurrentJSStackAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr GetCurrentNativeCallContextAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void DebugDump(short depth);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void DebugDumpObject([MarshalAs(UnmanagedType.Interface)] nsISupports aCOMObj, short depth);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void DebugDumpJSStack([MarshalAs(UnmanagedType.U1)] bool showArgs, [MarshalAs(UnmanagedType.U1)] bool showLocals, [MarshalAs(UnmanagedType.U1)] bool showThisProps);
		
		/// <summary>
		/// wrapJSAggregatedToNative is just like wrapJS except it is used in cases
		/// where the JSObject is also aggregated to some native xpcom Object.
		/// At present XBL is the only system that might want to do this.
		/// 
		/// XXX write more!
		/// 
		/// Returns:
		/// success:
		/// NS_OK
		/// failure:
		/// NS_ERROR_XPC_BAD_CONVERT_JS
		/// NS_ERROR_FAILURE
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr WrapJSAggregatedToNative([MarshalAs(UnmanagedType.Interface)] nsISupports aOuter, System.IntPtr aJSContext, System.IntPtr aJSObj, ref System.Guid aIID);
		
		/// <summary>
		/// This only succeeds if the native object is already wrapped by xpconnect.
		/// A new wrapper is *never* constructed.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIXPConnectWrappedNative GetWrappedNativeOfNativeObject(System.IntPtr aJSContext, System.IntPtr aScope, [MarshalAs(UnmanagedType.Interface)] nsISupports aCOMObj, ref System.Guid aIID);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetFunctionThisTranslator(ref System.Guid aIID, [MarshalAs(UnmanagedType.Interface)] nsIXPCFunctionThisTranslator aTranslator);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr GetWrappedNativePrototype(System.IntPtr aJSContext, System.IntPtr aScope, [MarshalAs(UnmanagedType.Interface)] nsIClassInfo aClassInfo);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		JSVal VariantToJS(System.IntPtr ctx, System.IntPtr scope, [MarshalAs(UnmanagedType.Interface)] nsIVariant value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIVariant JSToVariant(System.IntPtr ctx, ref JSVal value);
		
		/// <summary>
		/// Create a sandbox for evaluating code in isolation using
		/// evalInSandboxObject().
		/// </summary>
		/// <param name="cx">
		/// A context to use when creating the sandbox object.
		/// </param>
		/// <param name="principal">
		/// The principal (or NULL to use the null principal)
		/// to use when evaluating code in this sandbox.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr CreateSandbox(System.IntPtr cx, [MarshalAs(UnmanagedType.Interface)] nsIPrincipal principal);
		
		/// <summary>
		/// Evaluate script in a sandbox, completely isolated from all
		/// other running scripts.
		/// </summary>
		/// <param name="source">
		/// The source of the script to evaluate.
		/// </param>
		/// <param name="filename">
		/// The filename of the script. May be null.
		/// </param>
		/// <param name="cx">
		/// The context to use when setting up the evaluation of
		/// the script. The actual evaluation will happen on a new
		/// temporary context.
		/// </param>
		/// <param name="sandbox">
		/// The sandbox object to evaluate the script in.
		/// </param>
		/// <param name="version">
		/// The JavaScript version to use for evaluating the script.
		/// Should be a valid JSVersion from jspubtd.h.
		/// </param>
		/// <returns>
		/// The result of the evaluation as a jsval. If the caller
		/// intends to use the return value from this call the caller
		/// is responsible for rooting the jsval before making a call
		/// to this method.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		JSVal EvalInSandboxObject([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase source, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string filename, System.IntPtr cx, System.IntPtr sandbox, int version);
		
		/// <summary>
		/// Trigger a JS garbage collection.
		/// Use a js::gcreason::Reason from jsfriendapi.h for the kind.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GarbageCollect(uint reason);
		
		/// <summary>
		/// Signals a good place to do an incremental GC slice, because the
		/// browser is drawing a frame.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void NotifyDidPaint();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void WriteScript([MarshalAs(UnmanagedType.Interface)] nsIObjectOutputStream aStream, System.IntPtr aJSContext, System.IntPtr aJSScript);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr ReadScript([MarshalAs(UnmanagedType.Interface)] nsIObjectInputStream aStream, System.IntPtr aJSContext);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void WriteFunction([MarshalAs(UnmanagedType.Interface)] nsIObjectOutputStream aStream, System.IntPtr aJSContext, System.IntPtr aJSObject);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr ReadFunction([MarshalAs(UnmanagedType.Interface)] nsIObjectInputStream aStream, System.IntPtr aJSContext);
	}
}
