﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIASN1Object.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsIASN1Object ( "ba8bf582-1dd1-11b2-898c-f40246bc9a63" ) interface
	// </summary>
	public sealed class nsIASN1ObjectConsts
	{
		
		public const uint ASN1_END_CONTENTS = 0;
		
		public const uint ASN1_BOOLEAN = 1;
		
		public const uint ASN1_INTEGER = 2;
		
		public const uint ASN1_BIT_STRING = 3;
		
		public const uint ASN1_OCTET_STRING = 4;
		
		public const uint ASN1_NULL = 5;
		
		public const uint ASN1_OBJECT_ID = 6;
		
		public const uint ASN1_ENUMERATED = 10;
		
		public const uint ASN1_UTF8_STRING = 12;
		
		public const uint ASN1_SEQUENCE = 16;
		
		public const uint ASN1_SET = 17;
		
		public const uint ASN1_PRINTABLE_STRING = 19;
		
		public const uint ASN1_T61_STRING = 20;
		
		public const uint ASN1_IA5_STRING = 22;
		
		public const uint ASN1_UTC_TIME = 23;
		
		public const uint ASN1_GEN_TIME = 24;
		
		public const uint ASN1_VISIBLE_STRING = 26;
		
		public const uint ASN1_UNIVERSAL_STRING = 28;
		
		public const uint ASN1_BMP_STRING = 30;
		
		public const uint ASN1_HIGH_TAG_NUMBER = 31;
		
		public const uint ASN1_CONTEXT_SPECIFIC = 32;
		
		public const uint ASN1_APPLICATION = 33;
		
		public const uint ASN1_PRIVATE = 34;
	}
	
	/// <summary>
	/// This represents an ASN.1 object,
	/// where ASN.1 is &quot;Abstract Syntax Notation number One&quot;.
	/// 
	/// The additional state information carried in this interface
	/// makes it fit for being used as the data structure
	/// when working with visual reprenstation of ASN.1 objects
	/// in a human user interface, like in a tree widget
	/// where open/close state of nodes must be remembered.
	/// </summary>
	[ComImport]
	[Guid("ba8bf582-1dd1-11b2-898c-f40246bc9a63")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIASN1Object
	{
		
		/// <summary>
		/// &quot;type&quot; will be equal to one of the defined object identifiers.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetTypeAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetTypeAttribute(uint value);
		
		/// <summary>
		/// This contains a tag as explained in ASN.1 standards documents.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetTagAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetTagAttribute(uint value);
		
		/// <summary>
		/// &quot;displayName&quot; contains a human readable explanatory label.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetDisplayNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetDisplayNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase value);
		
		/// <summary>
		/// &quot;displayValue&quot; contains the human readable value.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetDisplayValueAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetDisplayValueAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase value);
	}
}
