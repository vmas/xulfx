﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIAccessibleVirtualCursorChangeEvent.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	[ComImport]
	[Guid("a58693b1-009e-4cc9-ae93-9c7d8f85cfdf")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIAccessibleVirtualCursorChangeEvent : nsIAccessibleEvent
	{
		
		#region nsIAccessibleEvent Members

		
		/// <summary>
		/// The type of event, based on the enumerated event values
		/// defined in this interface.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new uint GetEventTypeAttribute();
		
		/// <summary>
		/// The nsIAccessible associated with the event.
		/// May return null if no accessible is available
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsIAccessible GetAccessibleAttribute();
		
		/// <summary>
		/// The nsIAccessibleDocument that the event target nsIAccessible
		/// resides in. This can be used to get the DOM window,
		/// the DOM document and the window handler, among other things.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsIAccessibleDocument GetAccessibleDocumentAttribute();
		
		/// <summary>
		/// The nsIDOMNode associated with the event
		/// May return null if accessible for event has been shut down
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsIDOMNode GetDOMNodeAttribute();
		
		/// <summary>
		/// Returns true if the event was caused by explicit user input,
		/// as opposed to purely originating from a timer or mouse movement
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool GetIsFromUserInputAttribute();
		
		#endregion

		
		/// <summary>
		/// Previous object pointed at by virtual cursor. null if none.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIAccessible GetOldAccessibleAttribute();
		
		/// <summary>
		/// Previous start offset of pivot. -1 if none.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetOldStartOffsetAttribute();
		
		/// <summary>
		/// Previous end offset of pivot. -1 if none.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetOldEndOffsetAttribute();
		
		/// <summary>
		/// Reason for virtual cursor move.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		short GetReasonAttribute();
	}
}
