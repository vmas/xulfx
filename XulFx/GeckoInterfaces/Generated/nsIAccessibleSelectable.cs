﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIAccessibleSelectable.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// An accessibility interface for selectable widgets.
	/// </summary>
	[ComImport]
	[Guid("8efb03d4-1354-4875-94cf-261336057626")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIAccessibleSelectable
	{
		
		/// <summary>
		/// Return an nsIArray of selected items within the widget.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIArray GetSelectedItemsAttribute();
		
		/// <summary>
		/// Return the number of currently selected items.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetSelectedItemCountAttribute();
		
		/// <summary>
		/// Return a nth selected item within the widget.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIAccessible GetSelectedItemAt(uint index);
		
		/// <summary>
		/// Return true if the given item is selected.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool IsItemSelected(uint index);
		
		/// <summary>
		/// Adds the specified item to the widget&apos;s selection.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AddItemToSelection(uint index);
		
		/// <summary>
		/// Removes the specified item from the widget&apos;s selection.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void RemoveItemFromSelection(uint index);
		
		/// <summary>
		/// Select all items.
		/// </summary>
		/// <returns>
		/// false if the object does not accept multiple selection,
		/// otherwise true.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool SelectAll();
		
		/// <summary>
		/// Unselect all items.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void UnselectAll();
	}
}
