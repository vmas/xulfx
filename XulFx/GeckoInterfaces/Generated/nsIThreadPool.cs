﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIThreadPool.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	[ComImport]
	[Guid("ef194cab-3f86-4b61-b132-e5e96a79e5d1")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIThreadPoolListener
	{
		
		/// <summary>
		/// Called when a new thread is created by the thread pool. The notification
		/// happens on the newly-created thread.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnThreadCreated();
		
		/// <summary>
		/// Called when a thread is about to be destroyed by the thread pool. The
		/// notification happens on the thread that is about to be destroyed.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnThreadShuttingDown();
	}
	
	/// <summary>
	/// An interface to a thread pool.  A thread pool creates a limited number of
	/// anonymous (unnamed) worker threads.  An event dispatched to the thread pool
	/// will be run on the next available worker thread.
	/// </summary>
	[ComImport]
	[Guid("76ce99c9-8e43-489a-9789-f27cc4424965")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIThreadPool : nsIEventTarget
	{
		
		#region nsIEventTarget Members

		
		/// <summary>
		/// Check to see if this event target is associated with the current thread.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool IsOnCurrentThread();
		
		/// <summary>
		/// Dispatch an event to this event target.  This function may be called from
		/// any thread, and it may be called re-entrantly.
		/// </summary>
		/// <param name="event">
		/// The alreadyAddRefed&lt;&gt; event to dispatch.
		/// NOTE that the event will be leaked if it fails to dispatch.
		/// </param>
		/// <param name="flags">
		/// The flags modifying event dispatch.  The flags are described in detail
		/// below.
		/// </param>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_INVALID_ARG: Indicates that event is null.
		/// NS_ERROR_UNEXPECTED: Indicates that the thread is shutting down and has finished processing
		/// events, so this event would never run and has not been dispatched.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void DispatchFromC(System.IntPtr @event, uint flags);
		
		/// <summary>
		/// Version of Dispatch to expose to JS, which doesn&apos;t require an alreadyAddRefed&lt;&gt;
		/// (it will be converted to that internally)
		/// </summary>
		/// <param name="event">
		/// The (raw) event to dispatch.
		/// </param>
		/// <param name="flags">
		/// The flags modifying event dispatch.  The flags are described in detail
		/// below.
		/// </param>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_INVALID_ARG: Indicates that event is null.
		/// NS_ERROR_UNEXPECTED: Indicates that the thread is shutting down and has finished processing
		/// events, so this event would never run and has not been dispatched.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Dispatch([MarshalAs(UnmanagedType.Interface)] nsIRunnable @event, uint flags);
		
		/// <summary>
		/// Dispatch an event to this event target, but do not run it before delay
		/// milliseconds have passed.  This function may be called from any thread.
		/// </summary>
		/// <param name="event">
		/// The alreadyAddrefed&lt;&gt; event to dispatch.
		/// </param>
		/// <param name="delay">
		/// The delay (in ms) before running the event.  If event does not rise to
		/// the top of the event queue before the delay has passed, it will be set
		/// aside to execute once the delay has passed.  Otherwise, it will be
		/// executed immediately.
		/// </param>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_INVALID_ARG: Indicates that event is null.
		/// NS_ERROR_UNEXPECTED: Indicates that the thread is shutting down and has finished processing
		/// events, so this event would never run and has not been dispatched, or
		/// that delay is zero.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void DelayedDispatch(System.IntPtr @event, uint delay);
		
		#endregion

		
		/// <summary>
		/// Shutdown the thread pool.  This method may not be executed from any thread
		/// in the thread pool.  Instead, it is meant to be executed from another
		/// thread (usually the thread that created this thread pool).  When this
		/// function returns, the thread pool and all of its threads will be shutdown,
		/// and it will no longer be possible to dispatch tasks to the thread pool.
		/// 
		/// As a side effect, events on the current thread will be processed.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Shutdown();
		
		/// <summary>
		/// Get/set the maximum number of threads allowed at one time in this pool.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetThreadLimitAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetThreadLimitAttribute(uint value);
		
		/// <summary>
		/// Get/set the maximum number of idle threads kept alive.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetIdleThreadLimitAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetIdleThreadLimitAttribute(uint value);
		
		/// <summary>
		/// Get/set the amount of time in milliseconds before an idle thread is
		/// destroyed.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetIdleThreadTimeoutAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetIdleThreadTimeoutAttribute(uint value);
		
		/// <summary>
		/// Get/set the number of bytes reserved for the stack of all threads in
		/// the pool. By default this is nsIThreadManager::DEFAULT_STACK_SIZE.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetThreadStackSizeAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetThreadStackSizeAttribute(uint value);
		
		/// <summary>
		/// An optional listener that will be notified when a thread is created or
		/// destroyed in the course of the thread pool&apos;s operation.
		/// 
		/// A listener will only receive notifications about threads created after the
		/// listener is set so it is recommended that the consumer set the listener
		/// before dispatching the first event. A listener that receives an
		/// onThreadCreated() notification is guaranteed to always receive the
		/// corresponding onThreadShuttingDown() notification.
		/// 
		/// The thread pool takes ownership of the listener and releases it when the
		/// shutdown() method is called. Threads created after the listener is set will
		/// also take ownership of the listener so that the listener will be kept alive
		/// long enough to receive the guaranteed onThreadShuttingDown() notification.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIThreadPoolListener GetListenerAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetListenerAttribute([MarshalAs(UnmanagedType.Interface)] nsIThreadPoolListener value);
		
		/// <summary>
		/// Set the label for threads in the pool. All threads will be named
		/// &quot;&lt;aName&gt; #&lt;n&gt;&quot;, where &lt;n&gt; is a serial number.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetName([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aName);
	}
}
