﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIContentPolicyBase.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsIContentPolicyBase ( "17418187-d86f-48dd-92d1-238838df0a4e" ) interface
	// </summary>
	public sealed class nsIContentPolicyBaseConsts
	{
		
		public const uint TYPE_INVALID = 0;
		
		public const uint TYPE_OTHER = 1;
		
		public const uint TYPE_SCRIPT = 2;
		
		public const uint TYPE_IMAGE = 3;
		
		public const uint TYPE_STYLESHEET = 4;
		
		public const uint TYPE_OBJECT = 5;
		
		public const uint TYPE_DOCUMENT = 6;
		
		public const uint TYPE_SUBDOCUMENT = 7;
		
		public const uint TYPE_REFRESH = 8;
		
		public const uint TYPE_XBL = 9;
		
		public const uint TYPE_PING = 10;
		
		public const uint TYPE_XMLHTTPREQUEST = 11;
		
		public const uint TYPE_DATAREQUEST = 11;
		
		public const uint TYPE_OBJECT_SUBREQUEST = 12;
		
		public const uint TYPE_DTD = 13;
		
		public const uint TYPE_FONT = 14;
		
		public const uint TYPE_MEDIA = 15;
		
		public const uint TYPE_WEBSOCKET = 16;
		
		public const uint TYPE_CSP_REPORT = 17;
		
		public const uint TYPE_XSLT = 18;
		
		public const uint TYPE_BEACON = 19;
		
		public const uint TYPE_FETCH = 20;
		
		public const uint TYPE_IMAGESET = 21;
		
		public const uint TYPE_WEB_MANIFEST = 22;
		
		public const uint TYPE_INTERNAL_SCRIPT = 23;
		
		public const uint TYPE_INTERNAL_WORKER = 24;
		
		public const uint TYPE_INTERNAL_SHARED_WORKER = 25;
		
		public const uint TYPE_INTERNAL_EMBED = 26;
		
		public const uint TYPE_INTERNAL_OBJECT = 27;
		
		public const uint TYPE_INTERNAL_FRAME = 28;
		
		public const uint TYPE_INTERNAL_IFRAME = 29;
		
		public const uint TYPE_INTERNAL_AUDIO = 30;
		
		public const uint TYPE_INTERNAL_VIDEO = 31;
		
		public const uint TYPE_INTERNAL_TRACK = 32;
		
		public const uint TYPE_INTERNAL_XMLHTTPREQUEST = 33;
		
		public const uint TYPE_INTERNAL_EVENTSOURCE = 34;
		
		public const uint TYPE_INTERNAL_SERVICE_WORKER = 35;
		
		public const uint TYPE_INTERNAL_SCRIPT_PRELOAD = 36;
		
		public const uint TYPE_INTERNAL_IMAGE = 37;
		
		public const uint TYPE_INTERNAL_IMAGE_PRELOAD = 38;
		
		public const uint TYPE_INTERNAL_STYLESHEET = 39;
		
		public const uint TYPE_INTERNAL_STYLESHEET_PRELOAD = 40;
		
		public const uint TYPE_INTERNAL_IMAGE_FAVICON = 41;
		
		public const short REJECT_REQUEST = -1;
		
		public const short REJECT_TYPE = -2;
		
		public const short REJECT_SERVER = -3;
		
		public const short REJECT_OTHER = -4;
		
		public const short ACCEPT = 1;
	}
	
	/// <summary>
	/// Interface for content policy mechanism.  Implementations of this
	/// interface can be used to control loading of various types of out-of-line
	/// content, or processing of certain types of in-line content.
	/// 
	/// WARNING: do not block the caller from shouldLoad or shouldProcess (e.g.,
	/// by launching a dialog to prompt the user for something).
	/// </summary>
	[ComImport]
	[Guid("17418187-d86f-48dd-92d1-238838df0a4e")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIContentPolicyBase
	{
	}
}
