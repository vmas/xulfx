﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsITabParent.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	[ComImport]
	[Guid("8e49f7b0-1f98-4939-bf91-e9c39cd56434")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsITabParent
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetChildProcessOffset(out int aCssX, out int aCssY);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetUseAsyncPanZoomAttribute();
		
		/// <summary>
		/// Manages the docshell active state of the remote browser.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetDocShellIsActiveAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetDocShellIsActiveAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		/// <summary>
		/// Whether this tabParent is in prerender mode.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetIsPrerenderedAttribute();
		
		/// <summary>
		/// As an optimisation, setting the docshell&apos;s active state to
		/// inactive also triggers a layer invalidation to free up some
		/// potentially unhelpful memory usage. Calling preserveLayers
		/// will cause the layers to be preserved even for inactive
		/// docshells.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void PreserveLayers([MarshalAs(UnmanagedType.U1)] bool aPreserveLayers);
		
		/// <summary>
		/// During interactions where painting performance
		/// is more important than scrolling, we may temporarily
		/// suppress the displayport. Each enable called must be matched
		/// with a disable call.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SuppressDisplayport([MarshalAs(UnmanagedType.U1)] bool aEnabled);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		ulong GetTabIdAttribute();
		
		/// <summary>
		/// The OS level process Id of the related child process.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetOsPidAttribute();
		
		/// <summary>
		/// Navigate by key. If aForDocumentNavigation is true, navigate by document.
		/// If aForDocumentNavigation is false, navigate by element.
		/// 
		/// If aForward is true, navigate to the first focusable element or document.
		/// If aForward is false, navigate to the last focusable element or document.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void NavigateByKey([MarshalAs(UnmanagedType.U1)] bool aForward, [MarshalAs(UnmanagedType.U1)] bool aForDocumentNavigation);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetHasContentOpenerAttribute();
	}
}
