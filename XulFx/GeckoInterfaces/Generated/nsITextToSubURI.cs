﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsITextToSubURI.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	[ComImport]
	[Guid("8b042e24-6f87-11d3-b3c8-00805f8a6670")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsITextToSubURI
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))]
		string ConvertAndEscape([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string charset, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string text);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))]
		string UnEscapeAndConvert([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string charset, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string text);
		
		/// <summary>
		/// Unescapes the given URI fragment (for UI purpose only)
		/// Note:
		/// &lt;ul&gt;
		/// &lt;li&gt; escaping back the result (unescaped string) is not guaranteed to
		/// give the original escaped string
		/// &lt;li&gt; In case of a conversion error, the URI fragment (escaped) is
		/// assumed to be in UTF-8 and converted to AString (UTF-16)
		/// &lt;li&gt; In case of successful conversion any resulting character listed
		/// in network.IDN.blacklist_chars (except space) is escaped
		/// &lt;li&gt; Always succeeeds (callers don&apos;t need to do error checking)
		/// &lt;/ul&gt;
		/// </summary>
		/// <param name="aCharset">
		/// the charset to convert from
		/// </param>
		/// <param name="aURIFragment">
		/// the URI (or URI fragment) to unescape
		/// </param>
		/// <returns>
		/// Unescaped aURIFragment  converted to unicode
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void UnEscapeURIForUI([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aCharset, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase aURIFragment, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// Unescapes only non ASCII characters in the given URI fragment
		/// note: this method assumes the URI as UTF-8 and fallbacks to the given
		/// charset if the charset is an ASCII superset
		/// </summary>
		/// <param name="aCharset">
		/// the charset to convert from
		/// </param>
		/// <param name="aURIFragment">
		/// the URI (or URI fragment) to unescape
		/// </param>
		/// <returns>
		/// Unescaped aURIFragment  converted to unicode
		/// </returns>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_UCONV_NOCONV: when there is no decoder for aCharset
		/// or error code of nsIUnicodeDecoder in case of conversion failure
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void UnEscapeNonAsciiURI([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aCharset, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase aURIFragment, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
	}
}
