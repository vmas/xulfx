﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIHttpChannelInternal.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// The callback interface for nsIHttpChannelInternal::HTTPUpgrade()
	/// </summary>
	[ComImport]
	[Guid("5b515449-ab64-4dba-b3cd-da8fc2f83064")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIHttpUpgradeListener
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnTransportAvailable([MarshalAs(UnmanagedType.Interface)] nsISocketTransport aTransport, [MarshalAs(UnmanagedType.Interface)] nsIAsyncInputStream aSocketIn, [MarshalAs(UnmanagedType.Interface)] nsIAsyncOutputStream aSocketOut);
	}
	
	// <summary>
	// Constants for nsIHttpChannelInternal ( "4e28263d-1e03-46f4-aa5c-9512f91957f9" ) interface
	// </summary>
	public sealed class nsIHttpChannelInternalConsts
	{
		
		public const uint THIRD_PARTY_FORCE_ALLOW = (1 << 0);
		
		public const uint CORS_MODE_SAME_ORIGIN = 0;
		
		public const uint CORS_MODE_NO_CORS = 1;
		
		public const uint CORS_MODE_CORS = 2;
		
		public const uint CORS_MODE_NAVIGATE = 3;
		
		public const uint REDIRECT_MODE_FOLLOW = 0;
		
		public const uint REDIRECT_MODE_ERROR = 1;
		
		public const uint REDIRECT_MODE_MANUAL = 2;
		
		public const uint FETCH_CACHE_MODE_DEFAULT = 0;
		
		public const uint FETCH_CACHE_MODE_NO_STORE = 1;
		
		public const uint FETCH_CACHE_MODE_RELOAD = 2;
		
		public const uint FETCH_CACHE_MODE_NO_CACHE = 3;
		
		public const uint FETCH_CACHE_MODE_FORCE_CACHE = 4;
		
		public const uint FETCH_CACHE_MODE_ONLY_IF_CACHED = 5;
	}
	
	/// <summary>
	/// Dumping ground for http.  This interface will never be frozen.  If you are
	/// using any feature exposed by this interface, be aware that this interface
	/// will change and you will be broken.  You have been warned.
	/// </summary>
	[ComImport]
	[Guid("4e28263d-1e03-46f4-aa5c-9512f91957f9")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIHttpChannelInternal
	{
		
		/// <summary>
		/// An http channel can own a reference to the document URI
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIURI GetDocumentURIAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetDocumentURIAttribute([MarshalAs(UnmanagedType.Interface)] nsIURI value);
		
		/// <summary>
		/// Get the major/minor version numbers for the request
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetRequestVersion(out uint major, out uint minor);
		
		/// <summary>
		/// Get the major/minor version numbers for the response
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetResponseVersion(out uint major, out uint minor);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void TakeAllSecurityMessages(System.IntPtr aMessages);
		
		/// <summary>
		/// Helper method to set a cookie with a consumer-provided
		/// cookie header, _but_ using the channel&apos;s other information
		/// (URI&apos;s, prompters, date headers etc).
		/// </summary>
		/// <param name="aCookieHeader">
		/// The cookie header to be parsed.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetCookie([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aCookieHeader);
		
		/// <summary>
		/// Setup this channel as an application cache fallback channel.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetupFallbackChannel([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aFallbackKey);
		
		/// <summary>
		/// When set, these flags modify the algorithm used to decide whether to
		/// send 3rd party cookies for a given channel.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetThirdPartyFlagsAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetThirdPartyFlagsAttribute(uint value);
		
		/// <summary>
		/// This attribute was added before the &quot;flags&quot; above and is retained here
		/// for compatibility. When set to true, has the same effect as
		/// THIRD_PARTY_FORCE_ALLOW, described above.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetForceAllowThirdPartyCookieAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetForceAllowThirdPartyCookieAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		/// <summary>
		/// True iff the channel has been canceled.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetCanceledAttribute();
		
		/// <summary>
		/// External handlers may set this to true to notify the channel
		/// that it is open on behalf of a download.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetChannelIsForDownloadAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetChannelIsForDownloadAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		/// <summary>
		/// The local IP address to which this channel is bound, in the
		/// format produced by PR_NetAddrToString. May be IPv4 or IPv6.
		/// Note: in the presence of NAT, this may not be the same as the
		/// address that the remote host thinks it&apos;s talking to.
		/// 
		/// May throw NS_ERROR_NOT_AVAILABLE if accessed when the channel&apos;s
		/// endpoints are not yet determined, or in any case when
		/// nsIHttpActivityObserver.isActive is false. See bugs 534698 and 526207.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetLocalAddressAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase result);
		
		/// <summary>
		/// The local port number to which this channel is bound.
		/// 
		/// May throw NS_ERROR_NOT_AVAILABLE if accessed when the channel&apos;s
		/// endpoints are not yet determined, or in any case when
		/// nsIHttpActivityObserver.isActive is false. See bugs 534698 and 526207.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetLocalPortAttribute();
		
		/// <summary>
		/// The IP address of the remote host that this channel is
		/// connected to, in the format produced by PR_NetAddrToString.
		/// 
		/// May throw NS_ERROR_NOT_AVAILABLE if accessed when the channel&apos;s
		/// endpoints are not yet determined, or in any case when
		/// nsIHttpActivityObserver.isActive is false. See bugs 534698 and 526207.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetRemoteAddressAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase result);
		
		/// <summary>
		/// The remote port number that this channel is connected to.
		/// 
		/// May throw NS_ERROR_NOT_AVAILABLE if accessed when the channel&apos;s
		/// endpoints are not yet determined, or in any case when
		/// nsIHttpActivityObserver.isActive is false. See bugs 534698 and 526207.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetRemotePortAttribute();
		
		/// <summary>
		/// Transfer chain of redirected cache-keys.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetCacheKeysRedirectChain(System.IntPtr cacheKeys);
		
		/// <summary>
		/// HTTPUpgrade allows for the use of HTTP to bootstrap another protocol
		/// via the RFC 2616 Upgrade request header in conjunction with a 101 level
		/// response. The nsIHttpUpgradeListener will have its
		/// onTransportAvailable() method invoked if a matching 101 is processed.
		/// The arguments to onTransportAvailable provide the new protocol the low
		/// level tranport streams that are no longer used by HTTP.
		/// 
		/// The onStartRequest and onStopRequest events are still delivered and the
		/// listener gets full control over the socket if and when onTransportAvailable
		/// is delievered.
		/// </summary>
		/// <param name="aProtocolName">
		/// The value of the HTTP Upgrade request header
		/// </param>
		/// <param name="aListener">
		/// The callback object used to handle a successful upgrade
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void HTTPUpgrade([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aProtocolName, [MarshalAs(UnmanagedType.Interface)] nsIHttpUpgradeListener aListener);
		
		/// <summary>
		/// Enable/Disable Spdy negotiation on per channel basis.
		/// The network.http.spdy.enabled preference is still a pre-requisite
		/// for starting spdy.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetAllowSpdyAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetAllowSpdyAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		/// <summary>
		/// This attribute en/disables the timeout for the first byte of an HTTP
		/// response. Enabled by default.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetResponseTimeoutEnabledAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetResponseTimeoutEnabledAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		/// <summary>
		/// If the underlying transport supports RWIN manipulation, this is the
		/// intiial window value for the channel. HTTP/2 implements this.
		/// 0 means no override from system default. Set before opening channel.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetInitialRwinAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetInitialRwinAttribute(uint value);
		
		/// <summary>
		/// Get value of the URI passed to nsIHttpChannel.redirectTo() if any.
		/// May return null when redirectTo() has not been called.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIURI GetApiRedirectToURIAttribute();
		
		/// <summary>
		/// Enable/Disable use of Alternate Services with this channel.
		/// The network.http.altsvc.enabled preference is still a pre-requisite.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetAllowAltSvcAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetAllowAltSvcAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		/// <summary>
		/// If true, do not use newer protocol features that might have interop problems
		/// on the Internet. Intended only for use with critical infra like the updater.
		/// default is false.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetBeConservativeAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetBeConservativeAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		ulong GetLastModifiedTimeAttribute();
		
		/// <summary>
		/// Force a channel that has not been AsyncOpen&apos;ed to skip any check for possible
		/// interception and proceed immediately to open a previously-synthesized cache
		/// entry using the provided ID.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ForceIntercepted(ulong aInterceptionID);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetResponseSynthesizedAttribute();
		
		/// <summary>
		/// Set by nsCORSListenerProxy if credentials should be included in
		/// cross-origin requests. false indicates &quot;same-origin&quot;, users should still
		/// check flag LOAD_ANONYMOUS!
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetCorsIncludeCredentialsAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetCorsIncludeCredentialsAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		/// <summary>
		/// Set by nsCORSListenerProxy to indicate CORS load type. Defaults to CORS_MODE_NO_CORS.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetCorsModeAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetCorsModeAttribute(uint value);
		
		/// <summary>
		/// Set to indicate Request.redirect mode exposed during ServiceWorker
		/// interception. No policy enforcement is performed by the channel for this
		/// value.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetRedirectModeAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetRedirectModeAttribute(uint value);
		
		/// <summary>
		/// Set to indicate Request.cache mode, which simulates the fetch API
		/// semantics, and is also used for exposing this value to the Web page
		/// during service worker interception.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetFetchCacheModeAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetFetchCacheModeAttribute(uint value);
		
		/// <summary>
		/// The URI of the top-level window that&apos;s associated with this channel.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIURI GetTopWindowURIAttribute();
		
		/// <summary>
		/// The network interface id that&apos;s associated with this channel.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetNetworkInterfaceIdAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase result);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetNetworkInterfaceIdAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase value);
		
		/// <summary>
		/// Read the proxy URI, which, if non-null, will be used to resolve
		/// proxies for this channel.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIURI GetProxyURIAttribute();
		
		/// <summary>
		/// Make cross-origin CORS loads happen with a CORS preflight, and specify
		/// the CORS preflight parameters.
		/// </summary>
		[PreserveSig]
		[ThisCallCallingConvention]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetCorsPreflightParameters(System.IntPtr unsafeHeaders);
		
		/// <summary>
		/// When set to true, the channel will not pop any authentication prompts up
		/// to the user.  When provided or cached credentials lead to an
		/// authentication failure, that failure will be propagated to the channel
		/// listener.  Must be called before opening the channel, otherwise throws.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetBlockAuthPromptAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetBlockAuthPromptAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		/// <summary>
		/// Set to indicate Request.integrity.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetIntegrityMetadataAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetIntegrityMetadataAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase value);
		
		/// <summary>
		/// The connection info&apos;s hash key. We use it to test connection separation.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetConnectionInfoHashKeyAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase result);
		
		/// <summary>
		/// Returns nsHttpChannel (self) when this actually is implementing nsHttpChannel.
		/// </summary>
		[PreserveSig]
		[ThisCallCallingConvention]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr QueryHttpChannelImpl();
	}
}
