﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIThread.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// This interface provides a high-level abstraction for an operating system
	/// thread.
	/// 
	/// Threads have a built-in event queue, and a thread is an event target that
	/// can receive nsIRunnable objects (events) to be processed on the thread.
	/// 
	/// See nsIThreadManager for the API used to create and locate threads.
	/// </summary>
	[ComImport]
	[Guid("5801d193-29d1-4964-a6b7-70eb697ddf2b")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIThread : nsIEventTarget
	{
		
		#region nsIEventTarget Members

		
		/// <summary>
		/// Check to see if this event target is associated with the current thread.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool IsOnCurrentThread();
		
		/// <summary>
		/// Dispatch an event to this event target.  This function may be called from
		/// any thread, and it may be called re-entrantly.
		/// </summary>
		/// <param name="event">
		/// The alreadyAddRefed&lt;&gt; event to dispatch.
		/// NOTE that the event will be leaked if it fails to dispatch.
		/// </param>
		/// <param name="flags">
		/// The flags modifying event dispatch.  The flags are described in detail
		/// below.
		/// </param>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_INVALID_ARG: Indicates that event is null.
		/// NS_ERROR_UNEXPECTED: Indicates that the thread is shutting down and has finished processing
		/// events, so this event would never run and has not been dispatched.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void DispatchFromC(System.IntPtr @event, uint flags);
		
		/// <summary>
		/// Version of Dispatch to expose to JS, which doesn&apos;t require an alreadyAddRefed&lt;&gt;
		/// (it will be converted to that internally)
		/// </summary>
		/// <param name="event">
		/// The (raw) event to dispatch.
		/// </param>
		/// <param name="flags">
		/// The flags modifying event dispatch.  The flags are described in detail
		/// below.
		/// </param>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_INVALID_ARG: Indicates that event is null.
		/// NS_ERROR_UNEXPECTED: Indicates that the thread is shutting down and has finished processing
		/// events, so this event would never run and has not been dispatched.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Dispatch([MarshalAs(UnmanagedType.Interface)] nsIRunnable @event, uint flags);
		
		/// <summary>
		/// Dispatch an event to this event target, but do not run it before delay
		/// milliseconds have passed.  This function may be called from any thread.
		/// </summary>
		/// <param name="event">
		/// The alreadyAddrefed&lt;&gt; event to dispatch.
		/// </param>
		/// <param name="delay">
		/// The delay (in ms) before running the event.  If event does not rise to
		/// the top of the event queue before the delay has passed, it will be set
		/// aside to execute once the delay has passed.  Otherwise, it will be
		/// executed immediately.
		/// </param>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_INVALID_ARG: Indicates that event is null.
		/// NS_ERROR_UNEXPECTED: Indicates that the thread is shutting down and has finished processing
		/// events, so this event would never run and has not been dispatched, or
		/// that delay is zero.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void DelayedDispatch(System.IntPtr @event, uint delay);
		
		#endregion

		
		/// <summary>
		/// 
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr GetPRThreadAttribute();
		
		/// <summary>
		/// 
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetCanInvokeJSAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetCanInvokeJSAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		/// <summary>
		/// Shutdown the thread.  This method prevents further dispatch of events to
		/// the thread, and it causes any pending events to run to completion before
		/// the thread joins (see PR_JoinThread) with the current thread.  During this
		/// method call, events for the current thread may be processed.
		/// 
		/// This method MAY NOT be executed from the thread itself.  Instead, it is
		/// meant to be executed from another thread (usually the thread that created
		/// this thread or the main application thread).  When this function returns,
		/// the thread will be shutdown, and it will no longer be possible to dispatch
		/// events to the thread.
		/// </summary>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_UNEXPECTED: Indicates that this method was erroneously called when this thread was
		/// the current thread, that this thread was not created with a call to
		/// nsIThreadManager::NewThread, or if this method was called more than once
		/// on the thread object.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Shutdown();
		
		/// <summary>
		/// This method may be called to determine if there are any events ready to be
		/// processed.  It may only be called when this thread is the current thread.
		/// 
		/// Because events may be added to this thread by another thread, a &quot;false&quot;
		/// result does not mean that this thread has no pending events.  It only
		/// means that there were no pending events when this method was called.
		/// </summary>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_UNEXPECTED: Indicates that this method was erroneously called when this thread was
		/// not the current thread.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool HasPendingEvents();
		
		/// <summary>
		/// Process the next event.  If there are no pending events, then this method
		/// may wait -- depending on the value of the mayWait parameter -- until an
		/// event is dispatched to this thread.  This method is re-entrant but may
		/// only be called if this thread is the current thread.
		/// </summary>
		/// <param name="mayWait">
		/// A boolean parameter that if &quot;true&quot; indicates that the method may block
		/// the calling thread to wait for a pending event.
		/// </param>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_UNEXPECTED: Indicates that this method was erroneously called when this thread was
		/// not the current thread.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool ProcessNextEvent([MarshalAs(UnmanagedType.U1)] bool mayWait);
		
		/// <summary>
		/// Shutdown the thread asynchronously.  This method immediately prevents
		/// further dispatch of events to the thread, and it causes any pending events
		/// to run to completion before this thread joins with the current thread.
		/// 
		/// UNLIKE shutdown() this does not process events on the current thread.
		/// Instead it merely ensures that the current thread continues running until
		/// this thread has shut down.
		/// 
		/// This method MAY NOT be executed from the thread itself.  Instead, it is
		/// meant to be executed from another thread (usually the thread that created
		/// this thread or the main application thread).  When this function returns,
		/// the thread will continue running until it exhausts its event queue.
		/// </summary>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_UNEXPECTED: Indicates that this method was erroneously called when this thread was
		/// the current thread, that this thread was not created with a call to
		/// nsIThreadManager::NewThread, or if this method was called more than once
		/// on the thread object.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AsyncShutdown();
		
		/// <summary>
		/// Register an instance of nsIIdlePeriod which works as a facade of
		/// the abstract notion of a &quot;current idle period&quot;. The
		/// nsIIdlePeriod should always represent the &quot;current&quot; idle period
		/// with regard to expected work for the thread. The thread is free
		/// to use this when there are no higher prioritized tasks to process
		/// to determine if it is reasonable to schedule tasks that could run
		/// when the thread is idle. The responsibility of the registered
		/// nsIIdlePeriod is to answer with an estimated deadline at which
		/// the thread should expect that it could receive new higher
		/// priority tasks.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void RegisterIdlePeriod(System.IntPtr aIdlePeriod);
		
		/// <summary>
		/// Dispatch an event to the thread&apos;s idle queue.  This function may be called
		/// from any thread, and it may be called re-entrantly.
		/// </summary>
		/// <param name="event">
		/// The alreadyAddRefed&lt;&gt; event to dispatch.
		/// NOTE that the event will be leaked if it fails to dispatch.
		/// </param>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_INVALID_ARG: Indicates that event is null.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void IdleDispatch(System.IntPtr @event);
	}
}
