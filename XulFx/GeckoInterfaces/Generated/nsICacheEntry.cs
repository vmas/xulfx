﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsICacheEntry.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsICacheEntry ( "607c2a2c-0a48-40b9-a956-8cf2bb9857cf" ) interface
	// </summary>
	public sealed class nsICacheEntryConsts
	{
		
		public const uint NO_EXPIRATION_TIME = 4294967295;
	}
	
	[ComImport]
	[Guid("607c2a2c-0a48-40b9-a956-8cf2bb9857cf")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsICacheEntry
	{
		
		/// <summary>
		/// Get the key identifying the cache entry.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetKeyAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase result);
		
		/// <summary>
		/// Whether the entry is memory/only or persisted to disk.
		/// Note: private browsing entries are reported as persistent for consistency
		/// while are not actually persisted to disk.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetPersistentAttribute();
		
		/// <summary>
		/// Get the number of times the cache entry has been opened.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetFetchCountAttribute();
		
		/// <summary>
		/// Get the last time the cache entry was opened (in seconds since the Epoch).
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetLastFetchedAttribute();
		
		/// <summary>
		/// Get the last time the cache entry was modified (in seconds since the Epoch).
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetLastModifiedAttribute();
		
		/// <summary>
		/// Get the expiration time of the cache entry (in seconds since the Epoch).
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetExpirationTimeAttribute();
		
		/// <summary>
		/// Set the time at which the cache entry should be considered invalid (in
		/// seconds since the Epoch).
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetExpirationTime(uint expirationTime);
		
		/// <summary>
		/// This method is intended to override the per-spec cache validation
		/// decisions for a duration specified in seconds. The current state can
		/// be examined with isForcedValid (see below). This value is not persisted,
		/// so it will not survive session restart. Cache entries that are forced valid
		/// will not be evicted from the cache for the duration of forced validity.
		/// This means that there is a potential problem if the number of forced valid
		/// entries grows to take up more space than the cache size allows.
		/// 
		/// NOTE: entries that have been forced valid will STILL be ignored by HTTP
		/// channels if they have expired AND the resource in question requires
		/// validation after expiring. This is to avoid using known-stale content.
		/// </summary>
		/// <param name="aSecondsToTheFuture">
		/// the number of seconds the default cache validation behavior will be
		/// overridden before it returns to normal
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ForceValidFor(uint aSecondsToTheFuture);
		
		/// <summary>
		/// The state variable for whether this entry is currently forced valid.
		/// Defaults to false for normal cache validation behavior, and will return
		/// true if the number of seconds set by forceValidFor() has yet to be reached.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetIsForcedValidAttribute();
		
		/// <summary>
		/// Open blocking input stream to cache data.  Use the stream transport
		/// service to asynchronously read this stream on a background thread.
		/// The returned stream MAY implement nsISeekableStream.
		/// </summary>
		/// <param name="offset">
		/// read starting from this offset into the cached data.  an offset
		/// beyond the end of the stream has undefined consequences.
		/// </param>
		/// <returns>
		/// non-blocking, buffered input stream.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIInputStream OpenInputStream(long offset);
		
		/// <summary>
		/// Open non-blocking output stream to cache data.  The returned stream
		/// MAY implement nsISeekableStream.
		/// 
		/// If opening an output stream to existing cached data, the data will be
		/// truncated to the specified offset.
		/// </summary>
		/// <param name="offset">
		/// write starting from this offset into the cached data.  an offset
		/// beyond the end of the stream has undefined consequences.
		/// </param>
		/// <returns>
		/// blocking, buffered output stream.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIOutputStream OpenOutputStream(long offset);
		
		/// <summary>
		/// Stores the Content-Length specified in the HTTP header for this
		/// entry. Checked before we write to the cache entry, to prevent ever
		/// taking up space in the cache for an entry that we know up front
		/// is going to have to be evicted anyway. See bug 588507.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		long GetPredictedDataSizeAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetPredictedDataSizeAttribute(long value);
		
		/// <summary>
		/// Get/set security info on the cache entry for this descriptor.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports GetSecurityInfoAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetSecurityInfoAttribute([MarshalAs(UnmanagedType.Interface)] nsISupports value);
		
		/// <summary>
		/// Get the size of the cache entry data, as stored. This may differ
		/// from the entry&apos;s dataSize, if the entry is compressed.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetStorageDataSizeAttribute();
		
		/// <summary>
		/// Asynchronously doom an entry. Listener will be notified about the status
		/// of the operation. Null may be passed if caller doesn&apos;t care about the
		/// result.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AsyncDoom([MarshalAs(UnmanagedType.Interface)] nsICacheEntryDoomCallback listener);
		
		/// <summary>
		/// Methods for accessing meta data.  Meta data is a table of key/value
		/// string pairs.  The strings do not have to conform to any particular
		/// charset, but they must be null terminated.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))]
		string GetMetaDataElement([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string key);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetMetaDataElement([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string key, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string value);
		
		/// <summary>
		/// Obtain the list of metadata keys this entry keeps.
		/// 
		/// NOTE: The callback is invoked under the CacheFile&apos;s lock.  It means
		/// there should not be made any calls to the entry from the visitor and
		/// if the values need to be processed somehow, it&apos;s better to cache them
		/// and process outside the callback.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void VisitMetaData([MarshalAs(UnmanagedType.Interface)] nsICacheEntryMetaDataVisitor visitor);
		
		/// <summary>
		/// Claims that all metadata on this entry are up-to-date and this entry
		/// now can be delivered to other waiting consumers.
		/// 
		/// We need such method since metadata must be delivered synchronously.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void MetaDataReady();
		
		/// <summary>
		/// Called by consumer upon 304/206 response from the server.  This marks
		/// the entry content as positively revalidated.
		/// Consumer uses this method after the consumer has returned ENTRY_NEEDS_REVALIDATION
		/// result from onCacheEntryCheck and after successfull revalidation with the server.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetValid();
		
		/// <summary>
		/// Returns the size in kilobytes used to store the cache entry on disk.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetDiskStorageSizeInKBAttribute();
		
		/// <summary>
		/// Doom this entry and open a new, empty, entry for write.  Consumer has
		/// to exchange the entry this method is called on for the newly created.
		/// Used on 200 responses to conditional requests.
		/// </summary>
		/// <param name="aMemoryOnly">
		/// - whether the entry is to be created as memory/only regardless how
		/// the entry being recreated persistence is set
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsICacheEntry Recreate([MarshalAs(UnmanagedType.U1)] bool aMemoryOnly);
		
		/// <summary>
		/// Returns the length of data this entry holds.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		long GetDataSizeAttribute();
		
		/// <summary>
		/// Returns the length of data this entry holds.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		long GetAltDataSizeAttribute();
		
		/// <summary>
		/// Opens and returns an output stream that a consumer may use to save an
		/// alternate representation of the data.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIOutputStream OpenAlternativeOutputStream([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase type);
		
		/// <summary>
		/// Opens and returns an input stream that can be used to read the alternative
		/// representation previously saved in the cache.
		/// If this call is made while writing alt-data is still in progress, it is
		/// still possible to read content from the input stream as it&apos;s being written.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIInputStream OpenAlternativeInputStream([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase type);
		
		/// <summary>
		/// 
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Close();
		
		/// <summary>
		/// 
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void MarkValid();
		
		/// <summary>
		/// 
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void MaybeMarkValid();
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="aWriteAllowed">
		/// Consumer indicates whether write to the entry is allowed for it.
		/// Depends on implementation how the flag is handled.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool HasWriteAccess([MarshalAs(UnmanagedType.U1)] bool aWriteAllowed);
	}
	
	/// <summary>
	/// Argument for nsICacheEntry.visitMetaData, provides access to all metadata
	/// keys and values stored on the entry.
	/// </summary>
	[ComImport]
	[Guid("fea3e276-6ba5-4ceb-a581-807d1f43f6d0")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsICacheEntryMetaDataVisitor
	{
		
		/// <summary>
		/// Called over each key / value pair.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnMetaDataElement([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string key, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string value);
	}
}
