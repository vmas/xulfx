﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIComponentRegistrar.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	[ComImport]
	[Guid("2417cbfe-65ad-48a6-b4b6-eb84db174392")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIComponentRegistrar
	{
		
		/// <summary>
		/// autoRegister
		/// 
		/// Register a .manifest file, or an entire directory containing
		/// these files. Registration lasts for this run only, and is not cached.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AutoRegister([MarshalAs(UnmanagedType.Interface)] nsIFile aSpec);
		
		/// <summary>
		/// autoUnregister
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AutoUnregister([MarshalAs(UnmanagedType.Interface)] nsIFile aSpec);
		
		/// <summary>
		/// registerFactory
		/// 
		/// Register a factory with a given ContractID, CID and Class Name.
		/// </summary>
		/// <param name="aClass">
		/// : CID of object
		/// </param>
		/// <param name="aClassName">
		/// : Class Name of CID (unused)
		/// </param>
		/// <param name="aContractID">
		/// : ContractID associated with CID aClass. May be null
		/// if no contract ID is needed.
		/// </param>
		/// <param name="aFactory">
		/// : Factory that will be registered for CID aClass.
		/// If aFactory is null, the contract will be associated
		/// with a previously registered CID.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void RegisterFactory(ref System.Guid aClass, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aClassName, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aContractID, [MarshalAs(UnmanagedType.Interface)] nsIFactory aFactory);
		
		/// <summary>
		/// unregisterFactory
		/// 
		/// Unregister a factory associated with CID aClass.
		/// </summary>
		/// <param name="aClass">
		/// : CID being unregistered
		/// </param>
		/// <param name="aFactory">
		/// : Factory previously registered to create instances of
		/// CID aClass.
		/// </param>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR*: Method failure.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void UnregisterFactory(ref System.Guid aClass, [MarshalAs(UnmanagedType.Interface)] nsIFactory aFactory);
		
		/// <summary>
		/// registerFactoryLocation
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void RegisterFactoryLocation(ref System.Guid aClass, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aClassName, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aContractID, [MarshalAs(UnmanagedType.Interface)] nsIFile aFile, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aLoaderStr, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aType);
		
		/// <summary>
		/// unregisterFactoryLocation
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void UnregisterFactoryLocation(ref System.Guid aClass, [MarshalAs(UnmanagedType.Interface)] nsIFile aFile);
		
		/// <summary>
		/// isCIDRegistered
		/// 
		/// Returns true if a factory is registered for the CID.
		/// </summary>
		/// <param name="aClass">
		/// : CID queried for registeration
		/// </param>
		/// <returns>
		/// : true if a factory is registered for CID
		/// false otherwise.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool IsCIDRegistered(ref System.Guid aClass);
		
		/// <summary>
		/// isContractIDRegistered
		/// 
		/// Returns true if a factory is registered for the contract id.
		/// </summary>
		/// <param name="aClass">
		/// : contract id queried for registeration
		/// </param>
		/// <returns>
		/// : true if a factory is registered for contract id
		/// false otherwise.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool IsContractIDRegistered([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aContractID);
		
		/// <summary>
		/// enumerateCIDs
		/// 
		/// Enumerate the list of all registered CIDs.
		/// </summary>
		/// <returns>
		/// : enumerator for CIDs.  Elements of the enumeration can be QI&apos;ed
		/// for the nsISupportsID interface.  From the nsISupportsID, you
		/// can obtain the actual CID.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISimpleEnumerator EnumerateCIDs();
		
		/// <summary>
		/// enumerateContractIDs
		/// 
		/// Enumerate the list of all registered ContractIDs.
		/// </summary>
		/// <returns>
		/// : enumerator for ContractIDs. Elements of the enumeration can be
		/// QI&apos;ed for the nsISupportsCString interface.  From  the
		/// nsISupportsCString interface, you can obtain the actual
		/// Contract ID string.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISimpleEnumerator EnumerateContractIDs();
		
		/// <summary>
		/// CIDToContractID
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))]
		string CIDToContractID(ref System.Guid aClass);
		
		/// <summary>
		/// contractIDToCID
		/// 
		/// Returns the CID for a given Contract ID, if one exists and is registered.
		/// </summary>
		/// <returns>
		/// : Contract ID.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Struct)]
		UUID ContractIDToCID([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aContractID);
	}
}
