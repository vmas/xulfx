﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIPresentationListener.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	[ComImport]
	[Guid("0105f837-4279-4715-9d5b-2dc3f8b65353")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIPresentationAvailabilityListener
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void NotifyAvailableChange(System.IntPtr urls, [MarshalAs(UnmanagedType.U1)] bool available);
	}
	
	// <summary>
	// Constants for nsIPresentationSessionListener ( "7dd48df8-8f8c-48c7-ac37-7b9fd1acf2f8" ) interface
	// </summary>
	public sealed class nsIPresentationSessionListenerConsts
	{
		
		public const ushort STATE_CONNECTING = 0;
		
		public const ushort STATE_CONNECTED = 1;
		
		public const ushort STATE_CLOSED = 2;
		
		public const ushort STATE_TERMINATED = 3;
	}
	
	[ComImport]
	[Guid("7dd48df8-8f8c-48c7-ac37-7b9fd1acf2f8")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIPresentationSessionListener
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void NotifyStateChange([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase sessionId, ushort state, int reason);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void NotifyMessage([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase sessionId, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase data, [MarshalAs(UnmanagedType.U1)] bool isBinary);
	}
	
	[ComImport]
	[Guid("27f101d7-9ed1-429e-b4f8-43b00e8e111c")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIPresentationRespondingListener
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void NotifySessionConnect(ulong windowId, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase sessionId);
	}
}
