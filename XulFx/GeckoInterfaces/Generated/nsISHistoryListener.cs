﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsISHistoryListener.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// nsISHistoryListener defines the interface one can implement to receive
	/// notifications about activities in session history and to be able to
	/// cancel them.
	/// 
	/// A session history listener will be notified when pages are added, removed
	/// and loaded from session history. It can prevent any action (except adding
	/// a new session history entry) from happening by returning false from the
	/// corresponding callback method.
	/// 
	/// A session history listener can be registered on a particular nsISHistory
	/// instance via the nsISHistory::addSHistoryListener() method.
	/// </summary>
	[ComImport]
	[Guid("125c0833-746a-400e-9b89-d2d18545c08a")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsISHistoryListener
	{
		
		/// <summary>
		/// Called when a new document is added to session history. New documents are
		/// added to session history by docshell when new pages are loaded in a frame
		/// or content area, for example via nsIWebNavigation::loadURI()
		/// </summary>
		/// <param name="aNewURI">
		/// The URI of the document to be added to session history.
		/// </param>
		/// <param name="aOldIndex">
		/// The index of the current history item before the operation.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnHistoryNewEntry([MarshalAs(UnmanagedType.Interface)] nsIURI aNewURI, int aOldIndex);
		
		/// <summary>
		/// Called when navigating to a previous session history entry, for example
		/// due to a nsIWebNavigation::goBack() call.
		/// </summary>
		/// <param name="aBackURI">
		/// The URI of the session history entry being navigated to.
		/// </param>
		/// <returns>
		/// Whether the operation can proceed.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool OnHistoryGoBack([MarshalAs(UnmanagedType.Interface)] nsIURI aBackURI);
		
		/// <summary>
		/// Called when navigating to a next session history entry, for example
		/// due to a nsIWebNavigation::goForward() call.
		/// </summary>
		/// <param name="aForwardURI">
		/// The URI of the session history entry being navigated to.
		/// </param>
		/// <returns>
		/// Whether the operation can proceed.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool OnHistoryGoForward([MarshalAs(UnmanagedType.Interface)] nsIURI aForwardURI);
		
		/// <summary>
		/// Called when the current document is reloaded, for example due to a
		/// nsIWebNavigation::reload() call.
		/// </summary>
		/// <param name="aReloadURI">
		/// The URI of the document to be reloaded.
		/// </param>
		/// <param name="aReloadFlags">
		/// Flags that indicate how the document is to be
		/// refreshed. See constants on the nsIWebNavigation
		/// interface.
		/// </param>
		/// <returns>
		/// Whether the operation can proceed.
		/// </returns>
		/// <remarks>
		/// See nsIWebNavigation
		/// </remarks>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool OnHistoryReload([MarshalAs(UnmanagedType.Interface)] nsIURI aReloadURI, uint aReloadFlags);
		
		/// <summary>
		/// Called when navigating to a session history entry by index, for example,
		/// when nsIWebNavigation::gotoIndex() is called.
		/// </summary>
		/// <param name="aIndex">
		/// The index in session history of the entry to be loaded.
		/// </param>
		/// <param name="aGotoURI">
		/// The URI of the session history entry to be loaded.
		/// </param>
		/// <returns>
		/// Whether the operation can proceed.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool OnHistoryGotoIndex(int aIndex, [MarshalAs(UnmanagedType.Interface)] nsIURI aGotoURI);
		
		/// <summary>
		/// Called when entries are removed from session history. Entries can be
		/// removed from session history for various reasons, for example to control
		/// the memory usage of the browser, to prevent users from loading documents
		/// from history, to erase evidence of prior page loads, etc.
		/// 
		/// To purge documents from session history call nsISHistory::PurgeHistory()
		/// </summary>
		/// <param name="aNumEntries">
		/// The number of entries to be removed from session history.
		/// </param>
		/// <returns>
		/// Whether the operation can proceed.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool OnHistoryPurge(int aNumEntries);
		
		/// <summary>
		/// Called when an entry is replaced in the session history. Entries are
		/// replaced when navigating away from non-persistent history entries (such as
		/// about pages) and when history.replaceState is called.
		/// </summary>
		/// <param name="aIndex">
		/// The index in session history of the entry being
		/// replaced
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnHistoryReplaceEntry(int aIndex);
		
		/// <summary>
		/// Called when nsISHistory::count has been updated. Unlike OnHistoryNewEntry
		/// and OnHistoryPurge which happen before the modifications are actually done
		/// and maybe cancellable, this function is called after these modifications.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnLengthChange(int aCount);
	}
}
