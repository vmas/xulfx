﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file xpcIJSModuleLoader.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	[ComImport]
	[Guid("4f94b21f-2920-4bd9-8251-5fb60fb054b2")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface xpcIJSModuleLoader
	{
		
		/// <summary>
		/// To be called from JavaScript only.
		/// 
		/// Synchronously loads and evaluates the js file located at
		/// aResourceURI with a new, fully privileged global object.
		/// 
		/// If &apos;targetObj&apos; is specified and equal to null, returns the
		/// module&apos;s global object. Otherwise (if &apos;targetObj&apos; is not
		/// specified, or &apos;targetObj&apos; is != null) looks for a property
		/// &apos;EXPORTED_SYMBOLS&apos; on the new global object. &apos;EXPORTED_SYMBOLS&apos;
		/// is expected to be an array of strings identifying properties on
		/// the global object.  These properties will be installed as
		/// properties on &apos;targetObj&apos;, or, if &apos;targetObj&apos; is not specified,
		/// on the caller&apos;s global object. If &apos;EXPORTED_SYMBOLS&apos; is not
		/// found, an error is thrown.
		/// </summary>
		/// <param name="resourceURI">
		/// A resource:// URI string to load the module from.
		/// </param>
		/// <param name="targetObj">
		/// the object to install the exported properties on.
		/// If this parameter is a primitive value, this method throws
		/// an exception.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		JSVal Import([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase aResourceURI, ref JSVal targetObj, System.IntPtr cx, byte argc);
		
		/// <summary>
		/// Imports the JS module at aResourceURI to the JS object
		/// &apos;targetObj&apos; (if != null) as described for importModule() and
		/// returns the module&apos;s global object.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr ImportInto([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase aResourceURI, System.IntPtr targetObj, System.IntPtr cc);
		
		/// <summary>
		/// Unloads the JS module at aResourceURI. Existing references to the module
		/// will continue to work but any subsequent import of the module will
		/// reload it and give new reference. If the JS module hasn&apos;t yet been imported
		/// then this method will do nothing.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Unload([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase aResourceURI);
		
		/// <summary>
		/// Returns true if the js file located at &apos;registryLocation&apos; location has
		/// been loaded previously via the import method above. Returns false
		/// otherwise.
		/// </summary>
		/// <param name="resourceURI">
		/// A resource:// URI string representing the location of
		/// the js file to be checked if it is already loaded or not.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool IsModuleLoaded([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase aResourceURI);
	}
}
