﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIX509Cert.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsIX509Cert ( "bdc3979a-5422-4cd5-8589-696b6e96ea83" ) interface
	// </summary>
	public sealed class nsIX509CertConsts
	{
		
		public const uint UNKNOWN_CERT = 0;
		
		public const uint CA_CERT = (1 << 0);
		
		public const uint USER_CERT = (1 << 1);
		
		public const uint EMAIL_CERT = (1 << 2);
		
		public const uint SERVER_CERT = (1 << 3);
		
		public const uint ANY_CERT = 65535;
		
		public const uint CMS_CHAIN_MODE_CertOnly = 1;
		
		public const uint CMS_CHAIN_MODE_CertChain = 2;
		
		public const uint CMS_CHAIN_MODE_CertChainWithRoot = 3;
	}
	
	/// <summary>
	/// This represents a X.509 certificate.
	/// 
	/// NOTE: Service workers persist x.509 certs in object form on disk.  If you
	/// change this uuid you probably need a hack in nsBinaryInputStream to
	/// read the old uuid.  If you change the format of the object
	/// serialization then more complex changes will be needed.
	/// </summary>
	[ComImport]
	[Guid("bdc3979a-5422-4cd5-8589-696b6e96ea83")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIX509Cert
	{
		
		/// <summary>
		/// A nickname for the certificate.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetNicknameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// The primary email address of the certificate, if present.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetEmailAddressAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// Did this certificate ship with the platform as a built-in root?
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetIsBuiltInRootAttribute();
		
		/// <summary>
		/// Obtain a list of all email addresses
		/// contained in the certificate.
		/// </summary>
		/// <param name="length">
		/// The number of strings in the returned array.
		/// </param>
		/// <returns>
		/// An array of email addresses.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetEmailAddresses(out uint length, out System.IntPtr addresses);
		
		/// <summary>
		/// Check whether a given address is contained in the certificate.
		/// The comparison will convert the email address to lowercase.
		/// The behaviour for non ASCII characters is undefined.
		/// </summary>
		/// <param name="aEmailAddress">
		/// The address to search for.
		/// </param>
		/// <returns>
		/// True if the address is contained in the certificate.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool ContainsEmailAddress([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aEmailAddress);
		
		/// <summary>
		/// The subject owning the certificate.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetSubjectNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// The subject&apos;s common name.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetCommonNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// The subject&apos;s organization.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetOrganizationAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// The subject&apos;s organizational unit.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetOrganizationalUnitAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// The fingerprint of the certificate&apos;s DER encoding,
		/// calculated using the SHA-256 algorithm.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetSha256FingerprintAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// The fingerprint of the certificate&apos;s DER encoding,
		/// calculated using the SHA1 algorithm.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetSha1FingerprintAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// A human readable name identifying the hardware or
		/// software token the certificate is stored on.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetTokenNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// The subject identifying the issuer certificate.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetIssuerNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// The serial number the issuer assigned to this certificate.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetSerialNumberAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// The issuer subject&apos;s common name.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetIssuerCommonNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// The issuer subject&apos;s organization.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetIssuerOrganizationAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// The issuer subject&apos;s organizational unit.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetIssuerOrganizationUnitAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// The certificate used by the issuer to sign this certificate.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIX509Cert GetIssuerAttribute();
		
		/// <summary>
		/// This certificate&apos;s validity period.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIX509CertValidity GetValidityAttribute();
		
		/// <summary>
		/// A unique identifier of this certificate within the local storage.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetDbKeyAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase result);
		
		/// <summary>
		/// A human readable identifier to label this certificate.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetWindowTitleAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// Type of this certificate
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetCertTypeAttribute();
		
		/// <summary>
		/// True if the certificate is self-signed. CA issued
		/// certificates are always self-signed.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetIsSelfSignedAttribute();
		
		/// <summary>
		/// Obtain a list of certificates that contains this certificate
		/// and the issuing certificates of all involved issuers,
		/// up to the root issuer.
		/// </summary>
		/// <returns>
		/// The chain of certifficates including the issuers.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIArray GetChain();
		
		/// <summary>
		/// A comma separated list of localized strings representing the contents of
		/// the certificate&apos;s key usage extension, if present. The empty string if the
		/// certificate doesn&apos;t have the key usage extension, or has an empty extension.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetKeyUsagesAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// This is the attribute which describes the ASN1 layout
		/// of the certificate.  This can be used when doing a
		/// &quot;pretty print&quot; of the certificate&apos;s ASN1 structure.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIASN1Object GetASN1StructureAttribute();
		
		/// <summary>
		/// Obtain a raw binary encoding of this certificate
		/// in DER format.
		/// </summary>
		/// <param name="length">
		/// The number of bytes in the binary encoding.
		/// </param>
		/// <param name="data">
		/// The bytes representing the DER encoded certificate.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetRawDER(out uint length, out System.IntPtr data);
		
		/// <summary>
		/// Test whether two certificate instances represent the
		/// same certificate.
		/// </summary>
		/// <returns>
		/// Whether the certificates are equal
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool Equals([MarshalAs(UnmanagedType.Interface)] nsIX509Cert other);
		
		/// <summary>
		/// The base64 encoding of the DER encoded public key info using the specified
		/// digest.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetSha256SubjectPublicKeyInfoDigestAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase result);
		
		/// <summary>
		/// Obtain the certificate wrapped in a PKCS#7 SignedData structure,
		/// with or without the certificate chain
		/// </summary>
		/// <param name="chainMode">
		/// Whether to include the chain (with or without the root),
		/// see CMS_CHAIN_MODE constants.
		/// </param>
		/// <param name="length">
		/// The number of bytes of the PKCS#7 data.
		/// </param>
		/// <param name="data">
		/// The bytes representing the PKCS#7 wrapped certificate.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ExportAsCMS(uint chainMode, out uint length, out System.IntPtr data);
		
		/// <summary>
		/// Retrieves the NSS certificate object wrapped by this interface
		/// </summary>
		[PreserveSig]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr GetCert();
		
		/// <summary>
		/// Human readable names identifying all hardware or
		/// software tokens the certificate is stored on.
		/// </summary>
		/// <param name="length">
		/// On success, the number of entries in the returned array.
		/// </param>
		/// <returns>
		/// On success, an array containing the names of all tokens
		/// the certificate is stored on (may be empty).
		/// On failure the function throws/returns an error.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetAllTokenNames(out uint length, out System.IntPtr tokenNames);
		
		/// <summary>
		/// Either delete the certificate from all cert databases,
		/// or mark it as untrusted.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void MarkForPermDeletion();
	}
}
