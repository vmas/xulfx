﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIPackageKitService.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsIPackageKitService ( "89bb04f6-ce2a-11e3-8f4f-60a44c717042" ) interface
	// </summary>
	public sealed class nsIPackageKitServiceConsts
	{
		
		public const uint PK_INSTALL_PACKAGE_NAMES = 0;
		
		public const uint PK_INSTALL_MIME_TYPES = 1;
		
		public const uint PK_INSTALL_FONTCONFIG_RESOURCES = 2;
		
		public const uint PK_INSTALL_GSTREAMER_RESOURCES = 3;
		
		public const uint PK_INSTALL_METHOD_COUNT = 4;
	}
	
	[ComImport]
	[Guid("89bb04f6-ce2a-11e3-8f4f-60a44c717042")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIPackageKitService
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void InstallPackages(uint packageKitMethod, [MarshalAs(UnmanagedType.Interface)] nsIArray packageArray, [MarshalAs(UnmanagedType.Interface)] nsIObserver observer);
	}
}
