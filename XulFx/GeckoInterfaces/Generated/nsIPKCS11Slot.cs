﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIPKCS11Slot.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsIPKCS11Slot ( "c2d4f296-ee60-11d4-998b-00b0d02354a0" ) interface
	// </summary>
	public sealed class nsIPKCS11SlotConsts
	{
		
		public const uint SLOT_DISABLED = 0;
		
		public const uint SLOT_NOT_PRESENT = 1;
		
		public const uint SLOT_UNINITIALIZED = 2;
		
		public const uint SLOT_NOT_LOGGED_IN = 3;
		
		public const uint SLOT_LOGGED_IN = 4;
		
		public const uint SLOT_READY = 5;
	}
	
	[ComImport]
	[Guid("c2d4f296-ee60-11d4-998b-00b0d02354a0")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIPKCS11Slot
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase result);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetDescAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase result);
		
		/// <summary>
		/// Manufacturer ID of the slot.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetManIDAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase result);
		
		/// <summary>
		/// Hardware version of the slot.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetHWVersionAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase result);
		
		/// <summary>
		/// Firmware version of the slot.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetFWVersionAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase result);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetStatusAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIPK11Token GetToken();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetTokenNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase result);
	}
}
