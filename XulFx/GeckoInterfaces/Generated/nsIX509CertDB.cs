﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIX509CertDB.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	[ComImport]
	[Guid("fc2b60e5-9a07-47c2-a2cd-b83b68a660ac")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIOpenSignedAppFileCallback
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OpenSignedAppFileFinished(int rv, [MarshalAs(UnmanagedType.Interface)] nsIZipReader aZipReader, [MarshalAs(UnmanagedType.Interface)] nsIX509Cert aSignerCert);
	}
	
	[ComImport]
	[Guid("d5f97827-622a-488f-be08-d850432ac8ec")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIVerifySignedDirectoryCallback
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void VerifySignedDirectoryFinished(int rv, [MarshalAs(UnmanagedType.Interface)] nsIX509Cert aSignerCert);
	}
	
	[ComImport]
	[Guid("3d6a9c87-5c5f-46fc-9410-96da6092f0f2")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIVerifySignedManifestCallback
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void VerifySignedManifestFinished(int rv, [MarshalAs(UnmanagedType.Interface)] nsIX509Cert aSignerCert);
	}
	
	/// <summary>
	/// Callback type for use with asyncVerifyCertAtTime.
	/// If aPRErrorCode is PRErrorCodeSuccess (i.e. 0), aVerifiedChain represents the
	/// verified certificate chain determined by asyncVerifyCertAtTime. aHasEVPolicy
	/// represents whether or not the end-entity certificate verified as EV.
	/// If aPRErrorCode is non-zero, it represents the error encountered during
	/// verification. aVerifiedChain is null in that case and aHasEVPolicy has no
	/// meaning.
	/// </summary>
	[ComImport]
	[Guid("49e16fc8-efac-4f57-8361-956ef6b960a4")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsICertVerificationCallback
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void VerifyCertFinished(int aPRErrorCode, [MarshalAs(UnmanagedType.Interface)] nsIX509CertList aVerifiedChain, [MarshalAs(UnmanagedType.U1)] bool aHasEVPolicy);
	}
	
	// <summary>
	// Constants for nsIX509CertDB ( "5c16cd9b-5a73-47f1-ab0f-11ede7495cce" ) interface
	// </summary>
	public sealed class nsIX509CertDBConsts
	{
		
		public const uint UNTRUSTED = 0;
		
		public const uint TRUSTED_SSL = (1 << 0);
		
		public const uint TRUSTED_EMAIL = (1 << 1);
		
		public const uint TRUSTED_OBJSIGN = (1 << 2);
		
		public const uint AppMarketplaceProdPublicRoot = 1;
		
		public const uint AppMarketplaceProdReviewersRoot = 2;
		
		public const uint AppMarketplaceDevPublicRoot = 3;
		
		public const uint AppMarketplaceDevReviewersRoot = 4;
		
		public const uint AppMarketplaceStageRoot = 5;
		
		public const uint AppXPCShellRoot = 6;
		
		public const uint AddonsPublicRoot = 7;
		
		public const uint AddonsStageRoot = 8;
		
		public const uint PrivilegedPackageRoot = 9;
		
		public const uint DeveloperImportedRoot = 10;
		
		public const uint FLAG_LOCAL_ONLY = (1 << 0);
		
		public const uint FLAG_MUST_BE_EV = (1 << 1);
	}
	
	/// <summary>
	/// This represents a service to access and manipulate
	/// X.509 certificates stored in a database.
	/// </summary>
	[ComImport]
	[Guid("5c16cd9b-5a73-47f1-ab0f-11ede7495cce")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIX509CertDB
	{
		
		/// <summary>
		/// Given a nickname,
		/// locate the matching certificate.
		/// </summary>
		/// <param name="aNickname">
		/// The nickname to be used as the key
		/// to find a certificate.
		/// </param>
		/// <returns>
		/// The matching certificate if found.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIX509Cert FindCertByNickname([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aNickname);
		
		/// <summary>
		/// Will find a certificate based on its dbkey
		/// retrieved by getting the dbKey attribute of
		/// the certificate.
		/// </summary>
		/// <param name="aDBkey">
		/// Database internal key, as obtained using
		/// attribute dbkey in nsIX509Cert.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIX509Cert FindCertByDBKey([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aDBkey);
		
		/// <summary>
		/// Find user&apos;s own email encryption certificate by nickname.
		/// </summary>
		/// <param name="aNickname">
		/// The nickname to be used as the key
		/// to find the certificate.
		/// </param>
		/// <returns>
		/// The matching certificate if found.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIX509Cert FindEmailEncryptionCert([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aNickname);
		
		/// <summary>
		/// Find user&apos;s own email signing certificate by nickname.
		/// </summary>
		/// <param name="aNickname">
		/// The nickname to be used as the key
		/// to find the certificate.
		/// </param>
		/// <returns>
		/// The matching certificate if found.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIX509Cert FindEmailSigningCert([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aNickname);
		
		/// <summary>
		/// Find a certificate by email address.
		/// </summary>
		/// <param name="aEmailAddress">
		/// The email address to be used as the key
		/// to find the certificate.
		/// </param>
		/// <returns>
		/// The matching certificate if found.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIX509Cert FindCertByEmailAddress([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aEmailAddress);
		
		/// <summary>
		/// Use this to import a stream sent down as a mime type into
		/// the certificate database on the default token.
		/// The stream may consist of one or more certificates.
		/// </summary>
		/// <param name="data">
		/// The raw data to be imported
		/// </param>
		/// <param name="length">
		/// The length of the data to be imported
		/// </param>
		/// <param name="type">
		/// The type of the certificate, see constants in nsIX509Cert
		/// </param>
		/// <param name="ctx">
		/// A UI context.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ImportCertificates([MarshalAs(UnmanagedType.LPArray, SizeParamIndex=1)] byte[] data, uint length, uint type, [MarshalAs(UnmanagedType.Interface)] nsIInterfaceRequestor ctx);
		
		/// <summary>
		/// Import another person&apos;s email certificate into the database.
		/// </summary>
		/// <param name="data">
		/// The raw data to be imported
		/// </param>
		/// <param name="length">
		/// The length of the data to be imported
		/// </param>
		/// <param name="ctx">
		/// A UI context.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ImportEmailCertificate([MarshalAs(UnmanagedType.LPArray, SizeParamIndex=1)] byte[] data, uint length, [MarshalAs(UnmanagedType.Interface)] nsIInterfaceRequestor ctx);
		
		/// <summary>
		/// Import a personal certificate into the database, assuming
		/// the database already contains the private key for this certificate.
		/// </summary>
		/// <param name="data">
		/// The raw data to be imported
		/// </param>
		/// <param name="length">
		/// The length of the data to be imported
		/// </param>
		/// <param name="ctx">
		/// A UI context.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ImportUserCertificate([MarshalAs(UnmanagedType.LPArray, SizeParamIndex=1)] byte[] data, uint length, [MarshalAs(UnmanagedType.Interface)] nsIInterfaceRequestor ctx);
		
		/// <summary>
		/// Delete a certificate stored in the database.
		/// </summary>
		/// <param name="aCert">
		/// Delete this certificate.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void DeleteCertificate([MarshalAs(UnmanagedType.Interface)] nsIX509Cert aCert);
		
		/// <summary>
		/// Modify the trust that is stored and associated to a certificate within
		/// a database. Separate trust is stored for
		/// One call manipulates the trust for one trust type only.
		/// See the trust type constants defined within this interface.
		/// </summary>
		/// <param name="cert">
		/// Change the stored trust of this certificate.
		/// </param>
		/// <param name="type">
		/// The type of the certificate. See nsIX509Cert.
		/// </param>
		/// <param name="trust">
		/// A bitmask. The new trust for the possible usages.
		/// See the trust constants defined within this interface.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetCertTrust([MarshalAs(UnmanagedType.Interface)] nsIX509Cert cert, uint type, uint trust);
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="cert">
		/// The certificate for which to modify trust.
		/// </param>
		/// <param name="trustString">
		/// decoded by CERT_DecodeTrustString. 3 comma separated
		/// characters, indicating SSL, Email, and Obj signing
		/// trust.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetCertTrustFromString([MarshalAs(UnmanagedType.Interface)] nsIX509Cert cert, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase trustString);
		
		/// <summary>
		/// Query whether a certificate is trusted for a particular use.
		/// </summary>
		/// <param name="cert">
		/// Obtain the stored trust of this certificate.
		/// </param>
		/// <param name="certType">
		/// The type of the certificate. See nsIX509Cert.
		/// </param>
		/// <param name="trustType">
		/// A single bit from the usages constants defined
		/// within this interface.
		/// </param>
		/// <returns>
		/// Returns true if the certificate is trusted for the given use.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool IsCertTrusted([MarshalAs(UnmanagedType.Interface)] nsIX509Cert cert, uint certType, uint trustType);
		
		/// <summary>
		/// Import certificate(s) from file
		/// </summary>
		/// <param name="aFile">
		/// Identifies a file that contains the certificate
		/// to be imported.
		/// </param>
		/// <param name="aType">
		/// Describes the type of certificate that is going to
		/// be imported. See type constants in nsIX509Cert.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ImportCertsFromFile([MarshalAs(UnmanagedType.Interface)] nsIFile aFile, uint aType);
		
		/// <summary>
		/// Import a PKCS#12 file containing cert(s) and key(s) into the database.
		/// </summary>
		/// <param name="aToken">
		/// Optionally limits the scope of
		/// this function to a token device.
		/// Can be null to mean any token.
		/// </param>
		/// <param name="aFile">
		/// Identifies a file that contains the data
		/// to be imported.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ImportPKCS12File([MarshalAs(UnmanagedType.Interface)] nsISupports aToken, [MarshalAs(UnmanagedType.Interface)] nsIFile aFile);
		
		/// <summary>
		/// Export a set of certs and keys from the database to a PKCS#12 file.
		/// </summary>
		/// <param name="aToken">
		/// Optionally limits the scope of
		/// this function to a token device.
		/// Can be null to mean any token.
		/// </param>
		/// <param name="aFile">
		/// Identifies a file that will be filled with the data
		/// to be exported.
		/// </param>
		/// <param name="count">
		/// The number of certificates to be exported.
		/// </param>
		/// <param name="aCerts">
		/// The array of all certificates to be exported.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ExportPKCS12File([MarshalAs(UnmanagedType.Interface)] nsISupports aToken, [MarshalAs(UnmanagedType.Interface)] nsIFile aFile, uint count, [MarshalAs(UnmanagedType.LPArray, ArraySubType=System.Runtime.InteropServices.UnmanagedType.Interface, SizeParamIndex=2)] nsIX509Cert[] aCerts);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIX509Cert ConstructX509FromBase64([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase base64);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIX509Cert ConstructX509([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string certDER, uint length);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OpenSignedAppFileAsync(uint trustedRoot, [MarshalAs(UnmanagedType.Interface)] nsIFile aJarFile, [MarshalAs(UnmanagedType.Interface)] nsIOpenSignedAppFileCallback callback);
		
		/// <summary>
		/// Verifies the signature on a directory representing an unpacked signed
		/// JAR file. To be considered valid, there must be exactly one signature
		/// on the directory structure and that signature must have signed every
		/// entry. Further, the signature must come from a certificate that
		/// is trusted for code signing.
		/// 
		/// On success NS_OK and the trusted certificate that signed the
		/// unpacked JAR are returned.
		/// 
		/// On failure, an error code is returned.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void VerifySignedDirectoryAsync(uint trustedRoot, [MarshalAs(UnmanagedType.Interface)] nsIFile aUnpackedDir, [MarshalAs(UnmanagedType.Interface)] nsIVerifySignedDirectoryCallback callback);
		
		/// <summary>
		/// Given streams containing a signature and a manifest file, verifies
		/// that the signature is valid for the manifest. The signature must
		/// come from a certificate that is trusted for code signing and that
		/// was issued by the given trusted root.
		/// 
		/// On success, NS_OK and the trusted certificate that signed the
		/// Manifest are returned.
		/// 
		/// On failure, an error code is returned.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void VerifySignedManifestAsync(uint trustedRoot, [MarshalAs(UnmanagedType.Interface)] nsIInputStream aManifestStream, [MarshalAs(UnmanagedType.Interface)] nsIInputStream aSignatureStream, [MarshalAs(UnmanagedType.Interface)] nsIVerifySignedManifestCallback callback);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AddCert([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase certDER, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aTrust, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase aName);
		
		/// <summary>
		/// Warning: This interface is inteded to use only for testing only as:
		/// 1. It can create IO on the main thread.
		/// 2. It is in constant change, so in/out can change at any release.
		/// 
		/// Obtain the verification result for a cert given a particular usage.
		/// On success, the call returns 0, the chain built during verification,
		/// and whether the cert is good for EV usage.
		/// On failure, the call returns the PRErrorCode for the verification failure
		/// </summary>
		/// <param name="aCert">
		/// Obtain the stored trust of this certificate
		/// </param>
		/// <param name="aUsage">
		/// a integer representing the usage from NSS
		/// </param>
		/// <param name="aFlags">
		/// flags as described above
		/// </param>
		/// <param name="aHostname">
		/// the (optional) hostname to verify for
		/// </param>
		/// <param name="aTime">
		/// the time at which to verify, in seconds since the epoch
		/// </param>
		/// <param name="aVerifiedChain">
		/// chain of verification up to the root if success
		/// </param>
		/// <param name="aHasEVPolicy">
		/// bool that signified that the cert was an EV cert
		/// </param>
		/// <returns>
		/// 0 if success or the value or the error code for the verification
		/// failure
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int VerifyCertAtTime([MarshalAs(UnmanagedType.Interface)] nsIX509Cert aCert, long aUsage, uint aFlags, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aHostname, ulong aTime, [MarshalAs(UnmanagedType.Interface)] out nsIX509CertList aVerifiedChain, [MarshalAs(UnmanagedType.U1)] out bool aHasEVPolicy);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int VerifyCertNow([MarshalAs(UnmanagedType.Interface)] nsIX509Cert aCert, long aUsage, uint aFlags, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aHostname, [MarshalAs(UnmanagedType.Interface)] out nsIX509CertList aVerifiedChain, [MarshalAs(UnmanagedType.U1)] out bool aHasEVPolicy);
		
		/// <summary>
		/// Similar to the above, but asynchronous. As a result, use of this API is not
		/// limited to tests.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AsyncVerifyCertAtTime([MarshalAs(UnmanagedType.Interface)] nsIX509Cert aCert, long aUsage, uint aFlags, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aHostname, ulong aTime, [MarshalAs(UnmanagedType.Interface)] nsICertVerificationCallback aCallback);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ClearOCSPCache();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AddCertFromBase64([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase base64, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase aTrust, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase aName);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIX509CertList GetCerts();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIX509CertList GetEnterpriseRoots();
	}
}
