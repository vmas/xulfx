﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIPrincipal.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsIPrincipal ( "3da7b133-f1a0-4de9-a2bc-5c49014c1077" ) interface
	// </summary>
	public sealed class nsIPrincipalConsts
	{
		
		public const short APP_STATUS_NOT_INSTALLED = 0;
		
		public const short APP_STATUS_INSTALLED = 1;
		
		public const short APP_STATUS_PRIVILEGED = 2;
		
		public const short APP_STATUS_CERTIFIED = 3;
	}
	
	[ComImport]
	[Guid("3da7b133-f1a0-4de9-a2bc-5c49014c1077")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIPrincipal : nsISerializable
	{
		
		#region nsISerializable Members

		
		/// <summary>
		/// Initialize the object implementing nsISerializable, which must have
		/// been freshly constructed via CreateInstance.  All data members that
		/// can&apos;t be set to default values must have been serialized by write,
		/// and should be read from aInputStream in the same order by this method.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Read([MarshalAs(UnmanagedType.Interface)] nsIObjectInputStream aInputStream);
		
		/// <summary>
		/// Serialize the object implementing nsISerializable to aOutputStream, by
		/// writing each data member that must be recovered later to reconstitute
		/// a working replica of this object, in a canonical member and byte order,
		/// to aOutputStream.
		/// 
		/// NB: a class that implements nsISerializable *must* also implement
		/// nsIClassInfo, in particular nsIClassInfo::GetClassID.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Write([MarshalAs(UnmanagedType.Interface)] nsIObjectOutputStream aOutputStream);
		
		#endregion

		
		/// <summary>
		/// Returns whether the other principal is equivalent to this principal.
		/// Principals are considered equal if they are the same principal, or
		/// they have the same origin.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool Equals([MarshalAs(UnmanagedType.Interface)] nsIPrincipal other);
		
		/// <summary>
		/// Like equals, but takes document.domain changes into account.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool EqualsConsideringDomain([MarshalAs(UnmanagedType.Interface)] nsIPrincipal other);
		
		/// <summary>
		/// Returns a hash value for the principal.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetHashValueAttribute();
		
		/// <summary>
		/// The codebase URI to which this principal pertains.  This is
		/// generally the document URI.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIURI GetURIAttribute();
		
		/// <summary>
		/// The domain URI to which this principal pertains.
		/// This is null unless script successfully sets document.domain to our URI
		/// or a superdomain of our URI.
		/// Setting this has no effect on the URI.
		/// See https://developer.mozilla.org/en-US/docs/Web/Security/Same-origin_policy#Changing_origin
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIURI GetDomainAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetDomainAttribute([MarshalAs(UnmanagedType.Interface)] nsIURI value);
		
		/// <summary>
		/// Returns whether the other principal is equal to or weaker than this
		/// principal. Principals are equal if they are the same object or they
		/// have the same origin.
		/// 
		/// Thus a principal always subsumes itself.
		/// 
		/// The system principal subsumes itself and all other principals.
		/// 
		/// A null principal (corresponding to an unknown, hence assumed minimally
		/// privileged, security context) is not equal to any other principal
		/// (including other null principals), and therefore does not subsume
		/// anything but itself.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool Subsumes([MarshalAs(UnmanagedType.Interface)] nsIPrincipal other);
		
		/// <summary>
		/// Same as the previous method, subsumes(), but takes document.domain into
		/// account.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool SubsumesConsideringDomain([MarshalAs(UnmanagedType.Interface)] nsIPrincipal other);
		
		/// <summary>
		/// Checks whether this principal is allowed to load the network resource
		/// located at the given URI under the same-origin policy. This means that
		/// codebase principals are only allowed to load resources from the same
		/// domain, the system principal is allowed to load anything, and null
		/// principals can only load URIs where they are the principal. This is
		/// changed by the optional flag allowIfInheritsPrincipal (which defaults to
		/// false) which allows URIs that inherit their loader&apos;s principal.
		/// 
		/// If the load is allowed this function does nothing. If the load is not
		/// allowed the function throws NS_ERROR_DOM_BAD_URI.
		/// 
		/// NOTE: Other policies might override this, such as the Access-Control
		/// specification.
		/// NOTE: The &apos;domain&apos; attribute has no effect on the behaviour of this
		/// function.
		/// </summary>
		/// <param name="uri">
		/// The URI about to be loaded.
		/// </param>
		/// <param name="report">
		/// If true, will report a warning to the console service
		/// if the load is not allowed.
		/// </param>
		/// <param name="allowIfInheritsPrincipal">
		/// If true, the load is allowed if the
		/// loadee inherits the principal of the
		/// loader.
		/// </param>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_DOM_BAD_URI: if the load is not allowed.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void CheckMayLoad([MarshalAs(UnmanagedType.Interface)] nsIURI uri, [MarshalAs(UnmanagedType.U1)] bool report, [MarshalAs(UnmanagedType.U1)] bool allowIfInheritsPrincipal);
		
		/// <summary>
		/// A Content Security Policy associated with this principal.
		/// Use this function to query the associated CSP with this principal.
		/// Please *only* use this function to *set* a CSP when you know exactly what you are doing.
		/// Most likely you want to call ensureCSP instead of setCSP.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIContentSecurityPolicy GetCspAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetCspAttribute([MarshalAs(UnmanagedType.Interface)] nsIContentSecurityPolicy value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIContentSecurityPolicy EnsureCSP([MarshalAs(UnmanagedType.Interface)] nsIDOMDocument aDocument);
		
		/// <summary>
		/// A speculative Content Security Policy associated with this
		/// principal. Set during speculative loading (preloading) and
		/// used *only* for preloads.
		/// 
		/// If you want to query the CSP associated with that principal,
		/// then this is *not* what you want. Instead query &apos;csp&apos;.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIContentSecurityPolicy GetPreloadCspAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIContentSecurityPolicy EnsurePreloadCSP([MarshalAs(UnmanagedType.Interface)] nsIDOMDocument aDocument);
		
		/// <summary>
		/// The CSP of the principal in JSON notation.
		/// Note, that the CSP itself is not exposed to JS, but script
		/// should be able to obtain a JSON representation of the CSP.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetCspJSONAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// A dictionary of the non-default origin attributes associated with this
		/// nsIPrincipal.
		/// 
		/// Attributes are tokens that are taken into account when determining whether
		/// two principals are same-origin - if any attributes differ, the principals
		/// are cross-origin, even if the scheme, host, and port are the same.
		/// Attributes should also be considered for all security and bucketing decisions,
		/// even those which make non-standard comparisons (like cookies, which ignore
		/// scheme, or quotas, which ignore subdomains).
		/// 
		/// If you&apos;re looking for an easy-to-use canonical stringification of the origin
		/// attributes, see |originSuffix| below.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		JSVal GetOriginAttributesAttribute(System.IntPtr cx);
		
		/// <summary>
		/// A canonical representation of the origin for this principal. This
		/// consists of a base string (which, for codebase principals, is of the
		/// format scheme://host:port), concatenated with |originAttributes| (see
		/// below).
		/// 
		/// We maintain the invariant that principalA.equals(principalB) if and only
		/// if principalA.origin == principalB.origin.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetOriginAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase result);
		
		/// <summary>
		/// The base part of |origin| without the concatenation with |originSuffix|.
		/// This doesn&apos;t have the important invariants described above with |origin|,
		/// and as such should only be used for legacy situations.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetOriginNoSuffixAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase result);
		
		/// <summary>
		/// A string of the form !key1=value1&amp;key2=value2, where each pair represents
		/// an attribute with a non-default value. If all attributes have default
		/// values, this is the empty string.
		/// 
		/// The value of .originSuffix is automatically serialized into .origin, so any
		/// consumers using that are automatically origin-attribute-aware. Consumers with
		/// special requirements must inspect and compare .originSuffix manually.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetOriginSuffixAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase result);
		
		/// <summary>
		/// The base domain of the codebase URI to which this principal pertains
		/// (generally the document URI), handling null principals and
		/// non-hierarchical schemes correctly.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetBaseDomainAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase result);
		
		/// <summary>
		/// Gets the principal&apos;s app status, which indicates whether the principal
		/// corresponds to &quot;app code&quot;, and if it does, how privileged that code is.
		/// This method returns one of the APP_STATUS constants above.
		/// 
		/// Note that a principal may have
		/// 
		/// appId != nsIScriptSecurityManager::NO_APP_ID &amp;&amp;
		/// appId != nsIScriptSecurityManager::UNKNOWN_APP_ID
		/// 
		/// and still have appStatus == APP_STATUS_NOT_INSTALLED.  That&apos;s because
		/// appId identifies the app that contains this principal, but a window
		/// might be contained in an app and not be running code that the app has
		/// vouched for.  For example, the window might be inside an &lt;iframe
		/// mozbrowser&gt;, or the window&apos;s origin might not match the app&apos;s origin.
		/// 
		/// If you&apos;re doing a check to determine &quot;does this principal correspond to
		/// app code?&quot;, you must check appStatus; checking appId != NO_APP_ID is not
		/// sufficient.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		ushort GetAppStatusAttribute();
		
		/// <summary>
		/// Gets the id of the app this principal is inside.  If this principal is
		/// not inside an app, returns nsIScriptSecurityManager::NO_APP_ID.
		/// 
		/// Note that this principal does not necessarily have the permissions of
		/// the app identified by appId.  For example, this principal might
		/// correspond to an iframe whose origin differs from that of the app frame
		/// containing it.  In this case, the iframe will have the appId of its
		/// containing app frame, but the iframe must not run with the app&apos;s
		/// permissions.
		/// 
		/// Similarly, this principal might correspond to an &lt;iframe mozbrowser&gt;
		/// inside an app frame; in this case, the content inside the iframe should
		/// not have any of the app&apos;s permissions, even if the iframe is at the same
		/// origin as the app.
		/// 
		/// If you&apos;re doing a security check based on appId, you must check
		/// appStatus as well.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetAppIdAttribute();
		
		/// <summary>
		/// Gets the ID of the add-on this principal belongs to.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetAddonIdAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// Gets the id of the user context this principal is inside.  If this
		/// principal is inside the default userContext, this returns
		/// nsIScriptSecurityManager::DEFAULT_USER_CONTEXT_ID.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetUserContextIdAttribute();
		
		/// <summary>
		/// Gets the id of the private browsing state of the context containing
		/// this principal. If the principal has a private browsing value of 0, it
		/// is not in private browsing.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetPrivateBrowsingIdAttribute();
		
		/// <summary>
		/// Returns true iff the principal is inside an isolated mozbrowser element.
		/// &lt;iframe mozbrowser mozapp&gt; and &lt;xul:browser&gt; are not considered to be
		/// mozbrowser elements.  &lt;iframe mozbrowser noisolation&gt; does not count as
		/// isolated since isolation is disabled.  Isolation can only be disabled if
		/// the containing document is chrome.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetIsInIsolatedMozBrowserElementAttribute();
		
		/// <summary>
		/// Returns true if this principal has an unknown appId. This shouldn&apos;t
		/// generally be used. We only expose it due to not providing the correct
		/// appId everywhere where we construct principals.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetUnknownAppIdAttribute();
		
		/// <summary>
		/// Returns true iff this is a null principal (corresponding to an
		/// unknown, hence assumed minimally privileged, security context).
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetIsNullPrincipalAttribute();
		
		/// <summary>
		/// Returns true iff this principal corresponds to a codebase origin.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetIsCodebasePrincipalAttribute();
		
		/// <summary>
		/// Returns true iff this is an expanded principal.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetIsExpandedPrincipalAttribute();
		
		/// <summary>
		/// Returns true iff this is the system principal.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetIsSystemPrincipalAttribute();
		
		/// <summary>
		/// Returns true if this principal&apos;s origin is recognized as being on the
		/// whitelist of sites that can use the CSS Unprefixing Service.
		/// 
		/// (This interface provides a trivial implementation, just returning false;
		/// subclasses can implement something more complex as-needed.)
		/// </summary>
		[PreserveSig]
		[ThisCallCallingConvention]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool IsOnCSSUnprefixingWhitelist();
	}
	
	/// <summary>
	/// If nsSystemPrincipal is too risky to use, but we want a principal to access
	/// more than one origin, nsExpandedPrincipals letting us define an array of
	/// principals it subsumes. So script with an nsExpandedPrincipals will gain
	/// same origin access when at least one of its principals it contains gained
	/// sameorigin acccess. An nsExpandedPrincipal will be subsumed by the system
	/// principal, and by another nsExpandedPrincipal that has all its principals.
	/// It is added for jetpack content-scripts to let them interact with the
	/// content and a well defined set of other domains, without the risk of
	/// leaking out a system principal to the content. See: Bug 734891
	/// </summary>
	[ComImport]
	[Guid("f3e177df-6a5e-489f-80a7-2dd1481471d8")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIExpandedPrincipal
	{
		
		/// <summary>
		/// An array of principals that the expanded principal subsumes.
		/// Note: this list is not reference counted, it is shared, so
		/// should not be changed and should only be used ephemerally.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr GetWhiteListAttribute();
	}
}
