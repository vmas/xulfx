﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIScrollable.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsIScrollable ( "3507fc93-313e-4a4c-8ca8-4d0ea0f97315" ) interface
	// </summary>
	public sealed class nsIScrollableConsts
	{
		
		public const int ScrollOrientation_X = 1;
		
		public const int ScrollOrientation_Y = 2;
		
		public const int Scrollbar_Auto = 1;
		
		public const int Scrollbar_Never = 2;
		
		public const int Scrollbar_Always = 3;
	}
	
	/// <summary>
	/// The nsIScrollable is an interface that can be implemented by a control that
	/// supports scrolling.  This is a generic interface without concern for the
	/// type of content that may be inside.
	/// </summary>
	[ComImport]
	[Guid("3507fc93-313e-4a4c-8ca8-4d0ea0f97315")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIScrollable
	{
		
		/// <summary>
		/// Get or set the default scrollbar state for all documents in
		/// this shell.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetDefaultScrollbarPreferences(int scrollOrientation);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetDefaultScrollbarPreferences(int scrollOrientation, int scrollbarPref);
		
		/// <summary>
		/// Get information about whether the vertical and horizontal scrollbars are
		/// currently visible.  If you are only interested in one of the visibility
		/// settings pass nullptr in for the one you aren&apos;t interested in.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetScrollbarVisibility([MarshalAs(UnmanagedType.U1)] out bool verticalVisible, [MarshalAs(UnmanagedType.U1)] out bool horizontalVisible);
	}
}
