﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIXULWindow.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsIXULWindow ( "d6d7a014-e28d-4c9d-8727-1cf6d870619b" ) interface
	// </summary>
	public sealed class nsIXULWindowConsts
	{
		
		public const uint lowestZ = 0;
		
		public const uint loweredZ = 4;
		
		public const uint normalZ = 5;
		
		public const uint raisedZ = 6;
		
		public const uint highestZ = 9;
	}
	
	[ComImport]
	[Guid("d6d7a014-e28d-4c9d-8727-1cf6d870619b")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIXULWindow
	{
		
		/// <summary>
		/// The docshell owning the XUL for this window.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIDocShell GetDocShellAttribute();
		
		/// <summary>
		/// Indicates if this window is instrinsically sized.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetIntrinsicallySizedAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetIntrinsicallySizedAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		/// <summary>
		/// The primary content shell.
		/// 
		/// Note that this is a docshell tree item and therefore can not be assured of
		/// what object it is. It could be an editor, a docshell, or a browser object.
		/// Or down the road any other object that supports being a DocShellTreeItem
		/// Query accordingly to determine the capabilities.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIDocShellTreeItem GetPrimaryContentShellAttribute();
		
		/// <summary>
		/// In multiprocess case we may not have primaryContentShell but
		/// primaryTabParent.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsITabParent GetPrimaryTabParentAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void TabParentAdded([MarshalAs(UnmanagedType.Interface)] nsITabParent aTab, [MarshalAs(UnmanagedType.U1)] bool aPrimary);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void TabParentRemoved([MarshalAs(UnmanagedType.Interface)] nsITabParent aTab);
		
		/// <summary>
		/// The content shell specified by the supplied id.
		/// 
		/// Note that this is a docshell tree item and therefore can not be assured of
		/// what object it is.  It could be an editor, a docshell, or a browser object.
		/// Or down the road any other object that supports being a DocShellTreeItem
		/// Query accordingly to determine the capabilities.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIDocShellTreeItem GetContentShellById([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string ID);
		
		/// <summary>
		/// Tell this window that it has picked up a child XUL window
		/// </summary>
		/// <param name="aChild">
		/// the child window being added
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AddChildWindow([MarshalAs(UnmanagedType.Interface)] nsIXULWindow aChild);
		
		/// <summary>
		/// Tell this window that it has lost a child XUL window
		/// </summary>
		/// <param name="aChild">
		/// the child window being removed
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void RemoveChildWindow([MarshalAs(UnmanagedType.Interface)] nsIXULWindow aChild);
		
		/// <summary>
		/// Move the window to a centered position.
		/// </summary>
		/// <param name="aRelative">
		/// If not null, the window relative to which the window is
		/// moved. See aScreen parameter for details.
		/// </param>
		/// <param name="aScreen">
		/// PR_TRUE to center the window relative to the screen
		/// containing aRelative if aRelative is not null. If
		/// aRelative is null then relative to the screen of the
		/// opener window if it was initialized by passing it to
		/// nsWebShellWindow::Initialize. Failing that relative to
		/// the main screen.
		/// PR_FALSE to center it relative to aRelative itself.
		/// </param>
		/// <param name="aAlert">
		/// PR_TRUE to move the window to an alert position,
		/// generally centered horizontally and 1/3 down from the top.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Center([MarshalAs(UnmanagedType.Interface)] nsIXULWindow aRelative, [MarshalAs(UnmanagedType.U1)] bool aScreen, [MarshalAs(UnmanagedType.U1)] bool aAlert);
		
		/// <summary>
		/// Shows the window as a modal window. That is, ensures that it is visible
		/// and runs a local event loop, exiting only once the window has been closed.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ShowModal();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetZLevelAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetZLevelAttribute(uint value);
		
		/// <summary>
		/// contextFlags are from nsIWindowCreator2
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetContextFlagsAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetContextFlagsAttribute(uint value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetChromeFlagsAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetChromeFlagsAttribute(uint value);
		
		/// <summary>
		/// Begin assuming |chromeFlags| don&apos;t change hereafter, and assert
		/// if they do change.  The state change is one-way and idempotent.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AssumeChromeFlagsAreFrozen();
		
		/// <summary>
		/// Create a new window.
		/// </summary>
		/// <param name="aChromeFlags">
		/// see nsIWebBrowserChrome
		/// </param>
		/// <param name="aOpeningTab">
		/// the TabParent that requested this new window be opened.
		/// Can be left null.
		/// </param>
		/// <param name="aOpener">
		/// The window which is requesting that this new window be opened.
		/// </param>
		/// <returns>
		/// the newly minted window
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIXULWindow CreateNewWindow(int aChromeFlags, [MarshalAs(UnmanagedType.Interface)] nsITabParent aOpeningTab, [MarshalAs(UnmanagedType.Interface)] mozIDOMWindowProxy aOpener);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIXULBrowserWindow GetXULBrowserWindowAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetXULBrowserWindowAttribute([MarshalAs(UnmanagedType.Interface)] nsIXULBrowserWindow value);
		
		/// <summary>
		/// Back-door method to force application of chrome flags at a particular
		/// time.  Do NOT call this unless you know what you&apos;re doing!  In particular,
		/// calling this when this XUL window doesn&apos;t yet have a document in its
		/// docshell could cause problems.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ApplyChromeFlags();
		
		/// <summary>
		/// Given the dimensions of some content area held within this
		/// XUL window, and assuming that that content area will change
		/// its dimensions in linear proportion to the dimensions of this
		/// XUL window, changes the size of the XUL window so that the
		/// content area reaches a particular size.
		/// 
		/// We need to supply the content area dimensions because sometimes
		/// the child&apos;s nsDocShellTreeOwner needs to propagate a SizeShellTo
		/// call to the parent. But the shellItem argument of the call will
		/// not be available on the parent side.
		/// 
		/// Note: this is an internal method, other consumers should never call this.
		/// </summary>
		/// <param name="aDesiredWidth">
		/// The desired width of the content area in device pixels.
		/// </param>
		/// <param name="aDesiredHeight">
		/// The desired height of the content area in device pixels.
		/// </param>
		/// <param name="shellItemWidth">
		/// The current width of the content area.
		/// </param>
		/// <param name="shellItemHeight">
		/// The current height of the content area.
		/// </param>
		[PreserveSig]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SizeShellToWithLimit(int aDesiredWidth, int aDesiredHeight, int shellItemWidth, int shellItemHeight);
	}
}
