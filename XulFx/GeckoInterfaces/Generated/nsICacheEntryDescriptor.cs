﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsICacheEntryDescriptor.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	[ComImport]
	[Guid("90b17d31-46aa-4fb1-a206-473c966cbc18")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsICacheEntryDescriptor : nsICacheEntryInfo
	{
		
		#region nsICacheEntryInfo Members

		
		/// <summary>
		/// Get the client id associated with this cache entry.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))]
		new string GetClientIDAttribute();
		
		/// <summary>
		/// Get the id for the device that stores this cache entry.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))]
		new string GetDeviceIDAttribute();
		
		/// <summary>
		/// Get the key identifying the cache entry.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void GetKeyAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase result);
		
		/// <summary>
		/// Get the number of times the cache entry has been opened.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new int GetFetchCountAttribute();
		
		/// <summary>
		/// Get the last time the cache entry was opened (in seconds since the Epoch).
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new uint GetLastFetchedAttribute();
		
		/// <summary>
		/// Get the last time the cache entry was modified (in seconds since the Epoch).
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new uint GetLastModifiedAttribute();
		
		/// <summary>
		/// Get the expiration time of the cache entry (in seconds since the Epoch).
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new uint GetExpirationTimeAttribute();
		
		/// <summary>
		/// Get the cache entry data size.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new uint GetDataSizeAttribute();
		
		/// <summary>
		/// Find out whether or not the cache entry is stream based.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool IsStreamBased();
		
		#endregion

		
		/// <summary>
		/// Set the time at which the cache entry should be considered invalid (in
		/// seconds since the Epoch).
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetExpirationTime(uint expirationTime);
		
		/// <summary>
		/// Set the cache entry data size.  This will fail if the cache entry
		/// IS stream based.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetDataSize(uint size);
		
		/// <summary>
		/// Open blocking input stream to cache data.  This will fail if the cache
		/// entry IS NOT stream based.  Use the stream transport service to
		/// asynchronously read this stream on a background thread.  The returned
		/// stream MAY implement nsISeekableStream.
		/// </summary>
		/// <param name="offset">
		/// read starting from this offset into the cached data.  an offset
		/// beyond the end of the stream has undefined consequences.
		/// </param>
		/// <returns>
		/// blocking, unbuffered input stream.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIInputStream OpenInputStream(uint offset);
		
		/// <summary>
		/// Open blocking output stream to cache data.  This will fail if the cache
		/// entry IS NOT stream based.  Use the stream transport service to
		/// asynchronously write to this stream on a background thread.  The returned
		/// stream MAY implement nsISeekableStream.
		/// 
		/// If opening an output stream to existing cached data, the data will be
		/// truncated to the specified offset.
		/// </summary>
		/// <param name="offset">
		/// write starting from this offset into the cached data.  an offset
		/// beyond the end of the stream has undefined consequences.
		/// </param>
		/// <returns>
		/// blocking, unbuffered output stream.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIOutputStream OpenOutputStream(uint offset);
		
		/// <summary>
		/// Get/set the cache data element.  This will fail if the cache entry
		/// IS stream based.  The cache entry holds a strong reference to this
		/// object.  The object will be released when the cache entry is destroyed.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports GetCacheElementAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetCacheElementAttribute([MarshalAs(UnmanagedType.Interface)] nsISupports value);
		
		/// <summary>
		/// Stores the Content-Length specified in the HTTP header for this
		/// entry. Checked before we write to the cache entry, to prevent ever
		/// taking up space in the cache for an entry that we know up front
		/// is going to have to be evicted anyway. See bug 588507.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		long GetPredictedDataSizeAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetPredictedDataSizeAttribute(long value);
		
		/// <summary>
		/// Get the access granted to this descriptor.  See nsICache.idl for the
		/// definitions of the access modes and a thorough description of their
		/// corresponding meanings.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetAccessGrantedAttribute();
		
		/// <summary>
		/// Get/set the storage policy of the cache entry.  See nsICache.idl for
		/// the definitions of the storage policies.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetStoragePolicyAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetStoragePolicyAttribute(int value);
		
		/// <summary>
		/// Get the disk file associated with the cache entry.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIFile GetFileAttribute();
		
		/// <summary>
		/// Get/set security info on the cache entry for this descriptor.  This fails
		/// if the storage policy is not STORE_IN_MEMORY.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports GetSecurityInfoAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetSecurityInfoAttribute([MarshalAs(UnmanagedType.Interface)] nsISupports value);
		
		/// <summary>
		/// Get the size of the cache entry data, as stored. This may differ
		/// from the entry&apos;s dataSize, if the entry is compressed.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetStorageDataSizeAttribute();
		
		/// <summary>
		/// Doom the cache entry this descriptor references in order to slate it for
		/// removal.  Once doomed a cache entry cannot be undoomed.
		/// 
		/// A descriptor with WRITE access can doom the cache entry and choose to
		/// fail pending requests.  This means that pending requests will not get
		/// a cache descriptor.  This is meant as a tool for clients that wish to
		/// instruct pending requests to skip the cache.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Doom();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void DoomAndFailPendingRequests(int status);
		
		/// <summary>
		/// Asynchronously doom an entry. Listener will be notified about the status
		/// of the operation. Null may be passed if caller doesn&apos;t care about the
		/// result.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AsyncDoom([MarshalAs(UnmanagedType.Interface)] nsICacheListener listener);
		
		/// <summary>
		/// A writer must validate this cache object before any readers are given
		/// a descriptor to the object.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void MarkValid();
		
		/// <summary>
		/// Explicitly close the descriptor (optional).
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Close();
		
		/// <summary>
		/// Methods for accessing meta data.  Meta data is a table of key/value
		/// string pairs.  The strings do not have to conform to any particular
		/// charset, but they must be null terminated.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))]
		string GetMetaDataElement([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string key);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetMetaDataElement([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string key, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string value);
		
		/// <summary>
		/// Visitor will be called with key/value pair for each meta data element.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void VisitMetaData([MarshalAs(UnmanagedType.Interface)] nsICacheMetaDataVisitor visitor);
	}
	
	[ComImport]
	[Guid("22f9a49c-3cf8-4c23-8006-54efb11ac562")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsICacheMetaDataVisitor
	{
		
		/// <summary>
		/// Called for each key/value pair in the meta data for a cache entry
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool VisitMetaDataElement([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string key, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string value);
	}
}
