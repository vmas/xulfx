﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIClipboardOwner.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	[ComImport]
	[Guid("5a31c7a1-e122-11d2-9a57-000064657374")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIClipboardOwner
	{
		
		/// <summary>
		/// Notifies the owner of the clipboard transferable that the
		/// transferable is being removed from the clipboard
		/// </summary>
		/// <param name="aTransferable">
		/// The transferable
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void LosingOwnership([MarshalAs(UnmanagedType.Interface)] nsITransferable aTransferable);
	}
}
