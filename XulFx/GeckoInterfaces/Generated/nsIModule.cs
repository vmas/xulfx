﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIModule.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// The nsIModule interface.
	/// </summary>
	[ComImport]
	[Guid("7392d032-5371-11d3-994e-00805fd26fee")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIModule
	{
		
		/// <summary>
		/// Object Instance Creation
		/// 
		/// Obtains a Class Object from a nsIModule for a given CID and IID pair.
		/// This class object can either be query to a nsIFactory or a may be
		/// query to a nsIClassInfo.
		/// </summary>
		/// <param name="aCompMgr">
		/// : The global component manager
		/// </param>
		/// <param name="aClass">
		/// : ClassID of object instance requested
		/// </param>
		/// <param name="aIID">
		/// : IID of interface requested
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr GetClassObject([MarshalAs(UnmanagedType.Interface)] nsIComponentManager aCompMgr, ref System.Guid aClass, ref System.Guid aIID);
		
		/// <summary>
		/// One time registration callback
		/// 
		/// When the nsIModule is discovered, this method will be
		/// called so that any setup registration can be preformed.
		/// </summary>
		/// <param name="aCompMgr">
		/// : The global component manager
		/// </param>
		/// <param name="aLocation">
		/// : The location of the nsIModule on disk
		/// </param>
		/// <param name="aLoaderStr:">
		/// Opaque loader specific string
		/// </param>
		/// <param name="aType">
		/// : Loader Type being used to load this module
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void RegisterSelf([MarshalAs(UnmanagedType.Interface)] nsIComponentManager aCompMgr, [MarshalAs(UnmanagedType.Interface)] nsIFile aLocation, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aLoaderStr, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aType);
		
		/// <summary>
		/// One time unregistration callback
		/// 
		/// When the nsIModule is being unregistered, this method will be
		/// called so that any unregistration can be preformed
		/// </summary>
		/// <param name="aCompMgr">
		/// : The global component manager
		/// </param>
		/// <param name="aLocation">
		/// : The location of the nsIModule on disk
		/// </param>
		/// <param name="aLoaderStr">
		/// : Opaque loader specific string
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void UnregisterSelf([MarshalAs(UnmanagedType.Interface)] nsIComponentManager aCompMgr, [MarshalAs(UnmanagedType.Interface)] nsIFile aLocation, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string aLoaderStr);
		
		/// <summary>
		/// Module load management
		/// </summary>
		/// <param name="aCompMgr">
		/// : The global component manager
		/// </param>
		/// <returns>
		/// indicates to the caller if the module can be unloaded.
		/// Returning PR_TRUE isn&apos;t a guarantee that the module will be
		/// unloaded. It constitues only willingness of the module to be
		/// unloaded.  It is very important to ensure that no outstanding
		/// references to the module&apos;s code/data exist before returning
		/// PR_TRUE.
		/// Returning PR_FALSE guaratees that the module won&apos;t be unloaded.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool CanUnload([MarshalAs(UnmanagedType.Interface)] nsIComponentManager aCompMgr);
	}
}
