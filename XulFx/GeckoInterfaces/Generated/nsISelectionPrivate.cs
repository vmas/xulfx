﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsISelectionPrivate.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsISelectionPrivate ( "0c9f4f74-ee7e-4fe9-be6b-0ba856368178" ) interface
	// </summary>
	public sealed class nsISelectionPrivateConsts
	{
		
		public const short ENDOFPRECEDINGLINE = 0;
		
		public const short STARTOFNEXTLINE = 1;
		
		public const int TABLESELECTION_NONE = 0;
		
		public const int TABLESELECTION_CELL = 1;
		
		public const int TABLESELECTION_ROW = 2;
		
		public const int TABLESELECTION_COLUMN = 3;
		
		public const int TABLESELECTION_TABLE = 4;
		
		public const int TABLESELECTION_ALLCELLS = 5;
	}
	
	[ComImport]
	[Guid("0c9f4f74-ee7e-4fe9-be6b-0ba856368178")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsISelectionPrivate : nsISelection
	{
		
		#region nsISelection Members

		
		/// <summary>
		/// Returns the node in which the selection begins.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsIDOMNode GetAnchorNodeAttribute();
		
		/// <summary>
		/// The offset within the (text) node where the selection begins.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new int GetAnchorOffsetAttribute();
		
		/// <summary>
		/// Returns the node in which the selection ends.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsIDOMNode GetFocusNodeAttribute();
		
		/// <summary>
		/// The offset within the (text) node where the selection ends.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new int GetFocusOffsetAttribute();
		
		/// <summary>
		/// Indicates if the selection is collapsed or not.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool GetIsCollapsedAttribute();
		
		[PreserveSig]
		[ThisCallCallingConvention]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool Collapsed();
		
		/// <summary>
		/// Returns the number of ranges in the selection.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new int GetRangeCountAttribute();
		
		/// <summary>
		/// Returns the range at the specified index.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsIDOMRange GetRangeAt(int index);
		
		/// <summary>
		/// Collapses the selection to a single point, at the specified offset
		/// in the given DOM node. When the selection is collapsed, and the content
		/// is focused and editable, the caret will blink there.
		/// </summary>
		/// <param name="parentNode">
		/// The given dom node where the selection will be set
		/// </param>
		/// <param name="offset">
		/// Where in given dom node to place the selection (the offset into the given node)
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Collapse([MarshalAs(UnmanagedType.Interface)] nsIDOMNode parentNode, int offset);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void CollapseNative(System.IntPtr parentNode, int offset);
		
		/// <summary>
		/// Extends the selection by moving the selection end to the specified node and offset,
		/// preserving the selection begin position. The new selection end result will always
		/// be from the anchorNode to the new focusNode, regardless of direction.
		/// </summary>
		/// <param name="parentNode">
		/// The node where the selection will be extended to
		/// </param>
		/// <param name="offset">
		/// Where in node to place the offset in the new selection end
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Extend([MarshalAs(UnmanagedType.Interface)] nsIDOMNode parentNode, int offset);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void ExtendNative(System.IntPtr parentNode, int offset);
		
		/// <summary>
		/// Collapses the whole selection to a single point at the start
		/// of the current selection (irrespective of direction).  If content
		/// is focused and editable, the caret will blink there.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void CollapseToStart();
		
		/// <summary>
		/// Collapses the whole selection to a single point at the end
		/// of the current selection (irrespective of direction).  If content
		/// is focused and editable, the caret will blink there.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void CollapseToEnd();
		
		/// <summary>
		/// Indicates whether the node is part of the selection. If partlyContained
		/// is set to PR_TRUE, the function returns true when some part of the node
		/// is part of the selection. If partlyContained is set to PR_FALSE, the
		/// function only returns true when the entire node is part of the selection.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool ContainsNode([MarshalAs(UnmanagedType.Interface)] nsIDOMNode node, [MarshalAs(UnmanagedType.U1)] bool partlyContained);
		
		/// <summary>
		/// Adds all children of the specified node to the selection.
		/// </summary>
		/// <param name="parentNode">
		/// the parent of the children to be added to the selection.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void SelectAllChildren([MarshalAs(UnmanagedType.Interface)] nsIDOMNode parentNode);
		
		/// <summary>
		/// Adds a range to the current selection.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void AddRange([MarshalAs(UnmanagedType.Interface)] nsIDOMRange range);
		
		/// <summary>
		/// Removes a range from the current selection.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void RemoveRange([MarshalAs(UnmanagedType.Interface)] nsIDOMRange range);
		
		/// <summary>
		/// Removes all ranges from the current selection.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void RemoveAllRanges();
		
		/// <summary>
		/// Deletes this selection from document the nodes belong to.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void DeleteFromDocument();
		
		/// <summary>
		/// Returns the whole selection into a plain text string.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void ToString([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		/// <summary>
		/// Modifies the selection.  Note that the parameters are case-insensitive.
		/// </summary>
		/// <param name="alter">
		/// can be one of { &quot;move&quot;, &quot;extend&quot; }
		/// - &quot;move&quot; collapses the selection to the end of the selection and
		/// applies the movement direction/granularity to the collapsed
		/// selection.
		/// - &quot;extend&quot; leaves the start of the selection unchanged, and applies
		/// movement direction/granularity to the end of the selection.
		/// </param>
		/// <param name="direction">
		/// can be one of { &quot;forward&quot;, &quot;backward&quot;, &quot;left&quot;, &quot;right&quot; }
		/// </param>
		/// <param name="granularity">
		/// can be one of { &quot;character&quot;, &quot;word&quot;,
		/// &quot;line&quot;, &quot;lineboundary&quot; }
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Modify([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase alter, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase direction, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase granularity);
		
		#endregion

		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetInterlinePositionAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetInterlinePositionAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		System.IntPtr GetAncestorLimiterAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetAncestorLimiterAttribute(System.IntPtr value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void StartBatchChanges();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void EndBatchChanges();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ToStringWithFormat([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string formatType, uint flags, int wrapColumn, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase result);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AddSelectionListener([MarshalAs(UnmanagedType.Interface)] nsISelectionListener newListener);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void RemoveSelectionListener([MarshalAs(UnmanagedType.Interface)] nsISelectionListener listenerToRemove);
		
		/// <summary>
		/// Test if supplied range points to a single table element:
		/// Result is one of above constants. &quot;None&quot; means
		/// a table element isn&apos;t selected.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetTableSelectionType([MarshalAs(UnmanagedType.Interface)] nsIDOMRange range);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetCanCacheFrameOffsetAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetCanCacheFrameOffsetAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetCachedFrameOffset(System.IntPtr aFrame, int inOffset, System.IntPtr aPoint);
		
		/// <summary>
		/// Set the painting style for the range. The range must be a range in
		/// the selection. The textRangeStyle will be used by text frame
		/// when it is painting the selection.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetTextRangeStyle([MarshalAs(UnmanagedType.Interface)] nsIDOMRange range, System.IntPtr textRangeStyle);
		
		/// <summary>
		/// Get the direction of the selection.
		/// </summary>
		[PreserveSig]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		nsDirection GetSelectionDirection();
		
		[PreserveSig]
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetSelectionDirection(nsDirection aDirection);
		
		/// <summary>
		/// Returns the type of the selection (see nsISelectionController for
		/// available constants).
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		short GetTypeAttribute();
		
		/// <summary>
		/// Return array of ranges intersecting with the given DOM interval.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetRangesForInterval([MarshalAs(UnmanagedType.Interface)] nsIDOMNode beginNode, int beginOffset, [MarshalAs(UnmanagedType.Interface)] nsIDOMNode endNode, int endOffset, [MarshalAs(UnmanagedType.U1)] bool allowAdjacent, out uint resultCount, out System.IntPtr results);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetRangesForIntervalArray(System.IntPtr beginNode, int beginOffset, System.IntPtr endNode, int endOffset, [MarshalAs(UnmanagedType.U1)] bool allowAdjacent, System.IntPtr results);
		
		/// <summary>
		/// Scrolls a region of the selection, so that it is visible in
		/// the scrolled view.
		/// </summary>
		/// <param name="aRegion">
		/// - the region inside the selection to scroll into view
		/// (see selection region constants defined in
		/// nsISelectionController).
		/// </param>
		/// <param name="aIsSynchronous">
		/// - when true, scrolls the selection into view
		/// before returning. If false, posts a request which
		/// is processed at some point after the method returns.
		/// </param>
		/// <param name="aVPercent">
		/// - how to align the frame vertically.
		/// </param>
		/// <param name="aHPercent">
		/// - how to align the frame horizontally.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ScrollIntoView(short aRegion, [MarshalAs(UnmanagedType.U1)] bool aIsSynchronous, short aVPercent, short aHPercent);
		
		/// <summary>
		/// Scrolls a region of the selection, so that it is visible in
		/// the scrolled view.
		/// </summary>
		/// <param name="aRegion">
		/// - the region inside the selection to scroll into view
		/// (see selection region constants defined in
		/// nsISelectionController).
		/// </param>
		/// <param name="aIsSynchronous">
		/// - when true, scrolls the selection into view
		/// before returning. If false, posts a request which
		/// is processed at some point after the method returns.
		/// </param>
		/// <param name="aVertical">
		/// - how to align the frame vertically and when.
		/// See nsIPresShell.h:ScrollAxis for details.
		/// </param>
		/// <param name="aHorizontal">
		/// - how to align the frame horizontally and when.
		/// See nsIPresShell.h:ScrollAxis for details.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ScrollIntoViewInternal(short aRegion, [MarshalAs(UnmanagedType.U1)] bool aIsSynchronous, ScrollAxis aVertical, ScrollAxis aHorizontal);
		
		/// <summary>
		/// Modifies the cursor Bidi level after a change in keyboard direction
		/// </summary>
		/// <param name="langRTL">
		/// is PR_TRUE if the new language is right-to-left or
		/// PR_FALSE if the new language is left-to-right.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SelectionLanguageChange([MarshalAs(UnmanagedType.U1)] bool langRTL);
	}
}
