﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsICrashService.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsICrashService ( "f60d76e5-62c3-4f58-89f6-b726c2b7bc20" ) interface
	// </summary>
	public sealed class nsICrashServiceConsts
	{
		
		public const int PROCESS_TYPE_MAIN = 0;
		
		public const int PROCESS_TYPE_CONTENT = 1;
		
		public const int PROCESS_TYPE_PLUGIN = 2;
		
		public const int PROCESS_TYPE_GMPLUGIN = 3;
		
		public const int PROCESS_TYPE_GPU = 4;
		
		public const int CRASH_TYPE_CRASH = 0;
		
		public const int CRASH_TYPE_HANG = 1;
	}
	
	[ComImport]
	[Guid("f60d76e5-62c3-4f58-89f6-b726c2b7bc20")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsICrashService
	{
		
		/// <summary>
		/// Records the occurrence of a crash.
		/// </summary>
		/// <param name="processType">
		/// One of the PROCESS_TYPE constants defined below.
		/// </param>
		/// <param name="crashType">
		/// One of the CRASH_TYPE constants defined below.
		/// </param>
		/// <param name="id">
		/// Crash ID. Likely a UUID.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AddCrash(int processType, int crashType, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase id);
	}
}
