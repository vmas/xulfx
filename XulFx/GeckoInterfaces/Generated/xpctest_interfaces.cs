﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file xpctest_interfaces.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	[ComImport]
	[Guid("3c8fd2f5-970c-42c6-b5dd-cda1c16dcfd8")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIXPCTestInterfaceA
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))]
		string GetNameAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string value);
	}
	
	[ComImport]
	[Guid("ff528c3a-2410-46de-acaa-449aa6403a33")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIXPCTestInterfaceB
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))]
		string GetNameAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string value);
	}
	
	[ComImport]
	[Guid("401cf1b4-355b-4cee-b7b3-c7973aee49bd")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIXPCTestInterfaceC
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetSomeIntegerAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetSomeIntegerAttribute(int value);
	}
}
