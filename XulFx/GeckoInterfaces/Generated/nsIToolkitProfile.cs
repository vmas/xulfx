﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIToolkitProfile.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// Hold on to a profile lock. Once you release the last reference to this
	/// interface, the profile lock is released.
	/// </summary>
	[ComImport]
	[Guid("7c58c703-d245-4864-8d75-9648ca4a6139")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIProfileLock
	{
		
		/// <summary>
		/// The main profile directory.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIFile GetDirectoryAttribute();
		
		/// <summary>
		/// A directory corresponding to the main profile directory that exists for
		/// the purpose of storing data on the local filesystem, including cache
		/// files or other data files that may not represent critical user data.
		/// (e.g., this directory may not be included as part of a backup scheme.)
		/// 
		/// In some cases, this directory may just be the main profile directory.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIFile GetLocalDirectoryAttribute();
		
		/// <summary>
		/// The timestamp of an existing profile lock at lock time.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		ulong GetReplacedLockTimeAttribute();
		
		/// <summary>
		/// Unlock the profile.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Unlock();
	}
	
	/// <summary>
	/// A interface representing a profile.
	/// </summary>
	[ComImport]
	[Guid("7422b090-4a86-4407-972e-75468a625388")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIToolkitProfile
	{
		
		/// <summary>
		/// The location of the profile directory.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIFile GetRootDirAttribute();
		
		/// <summary>
		/// The location of the profile local directory, which may be the same as
		/// the root directory.  See nsIProfileLock::localDirectory.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIFile GetLocalDirAttribute();
		
		/// <summary>
		/// The name of the profile.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase result);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase value);
		
		/// <summary>
		/// Removes the profile from the registry of profiles.
		/// </summary>
		/// <param name="removeFiles">
		/// Indicates whether or not the profile directory should be
		/// removed in addition.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Remove([MarshalAs(UnmanagedType.U1)] bool removeFiles);
		
		/// <summary>
		/// Lock this profile using platform-specific locking methods.
		/// </summary>
		/// <param name="lockFile">
		/// If locking fails, this may return a lockFile object
		/// which can be used in platform-specific ways to
		/// determine which process has the file locked. Null
		/// may be passed.
		/// </param>
		/// <returns>
		/// An interface which holds a profile lock as long as you reference
		/// it.
		/// </returns>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_FILE_ACCESS_DENIED: if the profile was already locked.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIProfileLock Lock([MarshalAs(UnmanagedType.Interface)] out nsIProfileUnlocker aUnlocker);
	}
}
