﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIWebVTTParserWrapper.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// Interface for a wrapper of a JS WebVTT parser (vtt.js).
	/// </summary>
	[ComImport]
	[Guid("8dfe016e-1701-4618-9f5e-9a6154e853f0")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIWebVTTParserWrapper
	{
		
		/// <summary>
		/// Loads the JS WebVTTParser and sets it to use the passed window to create
		/// VTTRegions and VTTCues. This function must be called before calling
		/// parse, flush, or watch.
		/// </summary>
		/// <param name="window">
		/// The window that the parser will use to create VTTCues and
		/// VTTRegions.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void LoadParser([MarshalAs(UnmanagedType.Interface)] mozIDOMWindow window);
		
		/// <summary>
		/// Attempts to parse the stream&apos;s data as WebVTT format. When it successfully
		/// parses a WebVTT region or WebVTT cue it will create a VTTRegion or VTTCue
		/// object and pass it back to the callee through its callbacks.
		/// </summary>
		/// <param name="data">
		/// The buffer that contains the WebVTT data received by the
		/// Necko consumer so far.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Parse([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(ACStringMarshaler))] nsACStringBase data);
		
		/// <summary>
		/// Flush indicates that no more data is expected from the stream. As such the
		/// parser should try to parse any kind of partial data it has.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Flush();
		
		/// <summary>
		/// Set this parser object to use an nsIWebVTTListener object for its onCue
		/// and onRegion callbacks.
		/// </summary>
		/// <param name="callback">
		/// The nsIWebVTTListener object that exposes onCue and
		/// onRegion callbacks for the parser.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Watch([MarshalAs(UnmanagedType.Interface)] nsIWebVTTListener callback);
		
		/// <summary>
		/// Convert the text content of a WebVTT cue to a document fragment so that
		/// we can display it on the page.
		/// </summary>
		/// <param name="window">
		/// A window object with which the document fragment will be
		/// created.
		/// </param>
		/// <param name="cue">
		/// The cue whose content will be converted to a document
		/// fragment.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIDOMHTMLElement ConvertCueToDOMTree([MarshalAs(UnmanagedType.Interface)] mozIDOMWindow window, [MarshalAs(UnmanagedType.Interface)] nsISupports cue);
		
		/// <summary>
		/// Compute the display state of the VTTCues in cues along with any VTTRegions
		/// that they might be in. First, it computes the positioning and styling of
		/// the cues and regions passed and converts them into a DOM tree rooted at
		/// a containing HTMLDivElement. It then adjusts those computed divs for
		/// overlap avoidance using the dimensions of &apos;overlay&apos;. Finally, it adds the
		/// computed divs to the VTTCues display state property for use later.
		/// </summary>
		/// <param name="window">
		/// A window object with which it will create the DOM tree
		/// and containing div element.
		/// </param>
		/// <param name="cues">
		/// An array of VTTCues who need there display state to be
		/// computed.
		/// </param>
		/// <param name="overlay">
		/// The HTMLElement that the cues will be displayed within.
		/// </param>
		/// <param name="controls">
		/// The video control element that will affect cues position.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void ProcessCues([MarshalAs(UnmanagedType.Interface)] mozIDOMWindow window, [MarshalAs(UnmanagedType.Interface)] nsIVariant cues, [MarshalAs(UnmanagedType.Interface)] nsISupports overlay, [MarshalAs(UnmanagedType.Interface)] nsISupports controls);
	}
}
