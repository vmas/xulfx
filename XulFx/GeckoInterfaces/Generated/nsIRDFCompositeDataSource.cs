﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIRDFCompositeDataSource.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// An nsIRDFCompositeDataSource composes individual data sources, providing
	/// the illusion of a single, coherent RDF graph.
	/// </summary>
	[ComImport]
	[Guid("96343820-307c-11d2-bc15-00805f912fe7")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIRDFCompositeDataSource : nsIRDFDataSource
	{
		
		#region nsIRDFDataSource Members

		
		/// <summary>
		/// The &quot;URI&quot; of the data source. This used by the RDF service&apos;s
		/// |GetDataSource()| method to cache datasources.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))]
		new string GetURIAttribute();
		
		/// <summary>
		/// Find an RDF resource that points to a given node over the
		/// specified arc &amp; truth value
		/// </summary>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_RDF_NO_VALUE: if there is no source that leads
		/// to the target with the specified property.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsIRDFResource GetSource([MarshalAs(UnmanagedType.Interface)] nsIRDFResource aProperty, [MarshalAs(UnmanagedType.Interface)] nsIRDFNode aTarget, [MarshalAs(UnmanagedType.U1)] bool aTruthValue);
		
		/// <summary>
		/// Find all RDF resources that point to a given node over the
		/// specified arc &amp; truth value
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsISimpleEnumerator GetSources([MarshalAs(UnmanagedType.Interface)] nsIRDFResource aProperty, [MarshalAs(UnmanagedType.Interface)] nsIRDFNode aTarget, [MarshalAs(UnmanagedType.U1)] bool aTruthValue);
		
		/// <summary>
		/// Find a child of that is related to the source by the given arc
		/// arc and truth value
		/// </summary>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_RDF_NO_VALUE: if there is no target accessible from the
		/// source via the specified property.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsIRDFNode GetTarget([MarshalAs(UnmanagedType.Interface)] nsIRDFResource aSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aProperty, [MarshalAs(UnmanagedType.U1)] bool aTruthValue);
		
		/// <summary>
		/// Find all children of that are related to the source by the given arc
		/// arc and truth value.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsISimpleEnumerator GetTargets([MarshalAs(UnmanagedType.Interface)] nsIRDFResource aSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aProperty, [MarshalAs(UnmanagedType.U1)] bool aTruthValue);
		
		/// <summary>
		/// Add an assertion to the graph.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Assert([MarshalAs(UnmanagedType.Interface)] nsIRDFResource aSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aProperty, [MarshalAs(UnmanagedType.Interface)] nsIRDFNode aTarget, [MarshalAs(UnmanagedType.U1)] bool aTruthValue);
		
		/// <summary>
		/// Remove an assertion from the graph.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Unassert([MarshalAs(UnmanagedType.Interface)] nsIRDFResource aSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aProperty, [MarshalAs(UnmanagedType.Interface)] nsIRDFNode aTarget);
		
		/// <summary>
		/// Change an assertion from
		/// 
		/// [aSource]--[aProperty]--&gt;[aOldTarget]
		/// 
		/// to
		/// 
		/// [aSource]--[aProperty]--&gt;[aNewTarget]
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Change([MarshalAs(UnmanagedType.Interface)] nsIRDFResource aSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aProperty, [MarshalAs(UnmanagedType.Interface)] nsIRDFNode aOldTarget, [MarshalAs(UnmanagedType.Interface)] nsIRDFNode aNewTarget);
		
		/// <summary>
		/// &apos;Move&apos; an assertion from
		/// 
		/// [aOldSource]--[aProperty]--&gt;[aTarget]
		/// 
		/// to
		/// 
		/// [aNewSource]--[aProperty]--&gt;[aTarget]
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Move([MarshalAs(UnmanagedType.Interface)] nsIRDFResource aOldSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aNewSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aProperty, [MarshalAs(UnmanagedType.Interface)] nsIRDFNode aTarget);
		
		/// <summary>
		/// Query whether an assertion exists in this graph.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool HasAssertion([MarshalAs(UnmanagedType.Interface)] nsIRDFResource aSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aProperty, [MarshalAs(UnmanagedType.Interface)] nsIRDFNode aTarget, [MarshalAs(UnmanagedType.U1)] bool aTruthValue);
		
		/// <summary>
		/// Add an observer to this data source. If the datasource
		/// supports observers, the datasource source should hold a strong
		/// reference to the observer.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void AddObserver([MarshalAs(UnmanagedType.Interface)] nsIRDFObserver aObserver);
		
		/// <summary>
		/// Remove an observer from this data source.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void RemoveObserver([MarshalAs(UnmanagedType.Interface)] nsIRDFObserver aObserver);
		
		/// <summary>
		/// Get a cursor to iterate over all the arcs that point into a node.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsISimpleEnumerator ArcLabelsIn([MarshalAs(UnmanagedType.Interface)] nsIRDFNode aNode);
		
		/// <summary>
		/// Get a cursor to iterate over all the arcs that originate in
		/// a resource.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsISimpleEnumerator ArcLabelsOut([MarshalAs(UnmanagedType.Interface)] nsIRDFResource aSource);
		
		/// <summary>
		/// Retrieve all of the resources that the data source currently
		/// refers to.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsISimpleEnumerator GetAllResources();
		
		/// <summary>
		/// Returns whether a given command is enabled for a set of sources.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool IsCommandEnabled([MarshalAs(UnmanagedType.Interface)] nsISupports aSources, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aCommand, [MarshalAs(UnmanagedType.Interface)] nsISupports aArguments);
		
		/// <summary>
		/// Perform the specified command on set of sources.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void DoCommand([MarshalAs(UnmanagedType.Interface)] nsISupports aSources, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aCommand, [MarshalAs(UnmanagedType.Interface)] nsISupports aArguments);
		
		/// <summary>
		/// Returns the set of all commands defined for a given source.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsISimpleEnumerator GetAllCmds([MarshalAs(UnmanagedType.Interface)] nsIRDFResource aSource);
		
		/// <summary>
		/// Returns true if the specified node is pointed to by the specified arc.
		/// Equivalent to enumerating ArcLabelsIn and comparing for the specified arc.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool HasArcIn([MarshalAs(UnmanagedType.Interface)] nsIRDFNode aNode, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aArc);
		
		/// <summary>
		/// Returns true if the specified node has the specified outward arc.
		/// Equivalent to enumerating ArcLabelsOut and comparing for the specified arc.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool HasArcOut([MarshalAs(UnmanagedType.Interface)] nsIRDFResource aSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aArc);
		
		/// <summary>
		/// Notify observers that the datasource is about to send several
		/// notifications at once.
		/// This must be followed by calling endUpdateBatch(), otherwise
		/// viewers will get out of sync.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void BeginUpdateBatch();
		
		/// <summary>
		/// Notify observers that the datasource has completed issuing
		/// a notification group.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void EndUpdateBatch();
		
		#endregion

		
		/// <summary>
		/// Set this value to &lt;code&gt;true&lt;/code&gt; if the composite datasource
		/// may contains at least one datasource that has &lt;em&gt;negative&lt;/em&gt;
		/// assertions. (This is the default.)
		/// 
		/// Set this value to &lt;code&gt;false&lt;/code&gt; if none of the datasources
		/// being composed contains a negative assertion. This allows the
		/// composite datasource to perform some query optimizations.
		/// 
		/// By default, this value is &lt;code&gt;true&lt;/true&gt;.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetAllowNegativeAssertionsAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetAllowNegativeAssertionsAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		/// <summary>
		/// Set to &lt;code&gt;true&lt;/code&gt; if the composite datasource should
		/// take care to coalesce duplicate arcs when returning values from
		/// queries. (This is the default.)
		/// 
		/// Set to &lt;code&gt;false&lt;/code&gt; if the composite datasource shouldn&apos;t
		/// bother to check for duplicates. This allows the composite
		/// datasource to more efficiently answer queries.
		/// 
		/// By default, this value is &lt;code&gt;true&lt;/code&gt;.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetCoalesceDuplicateArcsAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetCoalesceDuplicateArcsAttribute([MarshalAs(UnmanagedType.U1)] bool value);
		
		/// <summary>
		/// Add a datasource the the composite data source.
		/// </summary>
		/// <param name="aDataSource">
		/// the datasource to add to composite
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AddDataSource([MarshalAs(UnmanagedType.Interface)] nsIRDFDataSource aDataSource);
		
		/// <summary>
		/// Remove a datasource from the composite data source.
		/// </summary>
		/// <param name="aDataSource">
		/// the datasource to remove from the composite
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void RemoveDataSource([MarshalAs(UnmanagedType.Interface)] nsIRDFDataSource aDataSource);
		
		/// <summary>
		/// Retrieve the datasources in the composite data source.
		/// </summary>
		/// <returns>
		/// an nsISimpleEnumerator that will enumerate each
		/// of the datasources in the composite
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISimpleEnumerator GetDataSources();
	}
}
