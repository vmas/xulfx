﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIGlobalHistory2.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	[ComImport]
	[Guid("cf777d42-1270-4b34-be7b-2931c93feda5")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIGlobalHistory2
	{
		
		/// <summary>
		/// Add a URI to global history
		/// </summary>
		/// <param name="aURI">
		/// the URI of the page
		/// </param>
		/// <param name="aRedirect">
		/// whether the URI was redirected to another location;
		/// this is &apos;true&apos; for the original URI which is
		/// redirected.
		/// </param>
		/// <param name="aToplevel">
		/// whether the URI is loaded in a top-level window
		/// </param>
		/// <param name="aReferrer">
		/// the URI of the referring page
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AddURI([MarshalAs(UnmanagedType.Interface)] nsIURI aURI, [MarshalAs(UnmanagedType.U1)] bool aRedirect, [MarshalAs(UnmanagedType.U1)] bool aToplevel, [MarshalAs(UnmanagedType.Interface)] nsIURI aReferrer);
		
		/// <summary>
		/// Checks to see whether the given URI is in history.
		/// </summary>
		/// <param name="aURI">
		/// the uri to the page
		/// </param>
		/// <returns>
		/// true if a URI has been visited
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool IsVisited([MarshalAs(UnmanagedType.Interface)] nsIURI aURI);
		
		/// <summary>
		/// Set the page title for the given uri. URIs that are not already in
		/// global history will not be added.
		/// </summary>
		/// <param name="aURI">
		/// the URI for which to set to the title
		/// </param>
		/// <param name="aTitle">
		/// the page title
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetPageTitle([MarshalAs(UnmanagedType.Interface)] nsIURI aURI, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aTitle);
	}
}
