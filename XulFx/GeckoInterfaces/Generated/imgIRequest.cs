﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file imgIRequest.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for imgIRequest ( "db0a945c-3883-424a-98d0-2ee0523b0255" ) interface
	// </summary>
	public sealed class imgIRequestConsts
	{
		
		public const int STATUS_NONE = 0;
		
		public const int STATUS_SIZE_AVAILABLE = 1;
		
		public const int STATUS_LOAD_COMPLETE = 2;
		
		public const int STATUS_ERROR = 4;
		
		public const int STATUS_FRAME_COMPLETE = 8;
		
		public const int STATUS_DECODE_COMPLETE = 16;
		
		public const int STATUS_IS_ANIMATED = 32;
		
		public const int STATUS_HAS_TRANSPARENCY = 64;
		
		public const int CORS_NONE = 1;
		
		public const int CORS_ANONYMOUS = 2;
		
		public const int CORS_USE_CREDENTIALS = 3;
	}
	
	/// <summary>
	/// imgIRequest interface
	/// </summary>
	/// <remarks>
	/// See imagelib2
	/// </remarks>
	[ComImport]
	[Guid("db0a945c-3883-424a-98d0-2ee0523b0255")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface imgIRequest : nsIRequest
	{
		
		#region nsIRequest Members

		
		/// <summary>
		/// The name of the request.  Often this is the URI of the request.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void GetNameAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AUTF8StringMarshaler))] nsAUTF8StringBase result);
		
		/// <summary>
		/// Indicates whether the request is pending. nsIRequest::isPending is
		/// true when there is an outstanding asynchronous event that will make
		/// the request no longer be pending.  Requests do not necessarily start
		/// out pending; in some cases, requests have to be explicitly initiated
		/// (e.g. nsIChannel implementations are only pending once asyncOpen
		/// returns successfully).
		/// 
		/// Requests can become pending multiple times during their lifetime.
		/// </summary>
		/// <returns>
		/// FALSE if the request has reached completion (e.g., after
		/// OnStopRequest has fired).
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool IsPending();
		
		/// <summary>
		/// The error status associated with the request.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new int GetStatusAttribute();
		
		/// <summary>
		/// Cancels the current request.  This will close any open input or
		/// output streams and terminate any async requests.  Users should
		/// normally pass NS_BINDING_ABORTED, although other errors may also
		/// be passed.  The error passed in will become the value of the
		/// status attribute.
		/// 
		/// Implementations must not send any notifications (e.g. via
		/// nsIRequestObserver) synchronously from this function. Similarly,
		/// removal from the load group (if any) must also happen asynchronously.
		/// 
		/// Requests that use nsIStreamListener must not call onDataAvailable
		/// anymore after cancel has been called.
		/// </summary>
		/// <param name="aStatus">
		/// the reason for canceling this request.
		/// 
		/// NOTE: most nsIRequest implementations expect aStatus to be a
		/// failure code; however, some implementations may allow aStatus to
		/// be a success code such as NS_OK.  In general, aStatus should be
		/// a failure code.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Cancel(int aStatus);
		
		/// <summary>
		/// Suspends the current request.  This may have the effect of closing
		/// any underlying transport (in order to free up resources), although
		/// any open streams remain logically opened and will continue delivering
		/// data when the transport is resumed.
		/// 
		/// Calling cancel() on a suspended request must not send any
		/// notifications (such as onstopRequest) until the request is resumed.
		/// 
		/// NOTE: some implementations are unable to immediately suspend, and
		/// may continue to deliver events already posted to an event queue. In
		/// general, callers should be capable of handling events even after
		/// suspending a request.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Suspend();
		
		/// <summary>
		/// Resumes the current request.  This may have the effect of re-opening
		/// any underlying transport and will resume the delivery of data to
		/// any open streams.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void Resume();
		
		/// <summary>
		/// The load group of this request.  While pending, the request is a
		/// member of the load group.  It is the responsibility of the request
		/// to implement this policy.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		new nsILoadGroup GetLoadGroupAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void SetLoadGroupAttribute([MarshalAs(UnmanagedType.Interface)] nsILoadGroup value);
		
		/// <summary>
		/// The load flags of this request.  Bits 0-15 are reserved.
		/// 
		/// When added to a load group, this request&apos;s load flags are merged with
		/// the load flags of the load group.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new uint GetLoadFlagsAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void SetLoadFlagsAttribute(uint value);
		
		#endregion

		
		/// <summary>
		/// the image container...
		/// </summary>
		/// <returns>
		/// the image object associated with the request.
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		imgIContainer GetImageAttribute();
		
		/// <summary>
		/// Status flags of the STATUS_* variety.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetImageStatusAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetImageErrorCodeAttribute();
		
		/// <summary>
		/// The URI the image load was started with.  Note that this might not be the
		/// actual URI for the image (e.g. if HTTP redirects happened during the
		/// load).
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIURI GetURIAttribute();
		
		/// <summary>
		/// The URI of the resource we ended up loading after all redirects, etc.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIURI GetCurrentURIAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		imgINotificationObserver GetNotificationObserverAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))]
		string GetMimeTypeAttribute();
		
		/// <summary>
		/// Clone this request; the returned request will have aObserver as the
		/// observer.  aObserver will be notified synchronously (before the clone()
		/// call returns) with all the notifications that have already been dispatched
		/// for this image load.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		imgIRequest Clone([MarshalAs(UnmanagedType.Interface)] imgINotificationObserver aObserver);
		
		/// <summary>
		/// The principal gotten from the channel the image was loaded from.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIPrincipal GetImagePrincipalAttribute();
		
		/// <summary>
		/// Whether the request is multipart (ie, multipart/x-mixed-replace)
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetMultipartAttribute();
		
		/// <summary>
		/// The CORS mode that this image was loaded with.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetCORSModeAttribute();
		
		/// <summary>
		/// Cancels this request as in nsIRequest::Cancel(); further, also nulls out
		/// decoderObserver so it gets no further notifications from us.
		/// 
		/// NOTE: You should not use this in any new code; instead, use cancel(). Note
		/// that cancel() is asynchronous, which means that some time after you call
		/// it, the listener/observer will get an OnStopRequest(). This means that, if
		/// you&apos;re the observer, you can&apos;t call cancel() from your destructor.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void CancelAndForgetObserver(int aStatus);
		
		/// <summary>
		/// Requests a synchronous decode for the image.
		/// 
		/// imgIContainer has a startDecoding() method, but callers may want to request
		/// a decode before the container has necessarily been instantiated. Calling
		/// startDecoding() on the imgIRequest simply forwards along the request if the
		/// container already exists, or calls it once the container becomes available
		/// if it does not yet exist.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void StartDecoding();
		
		/// <summary>
		/// Locks an image. If the image does not exist yet, locks it once it becomes
		/// available. The lock persists for the lifetime of the imgIRequest (until
		/// unlockImage is called) even if the underlying image changes.
		/// 
		/// If you don&apos;t call unlockImage() by the time this imgIRequest goes away, it
		/// will be called for you automatically.
		/// </summary>
		/// <remarks>
		/// See imgIContainer::lockImage for documentation of the underlying call.
		/// </remarks>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void LockImage();
		
		/// <summary>
		/// Unlocks an image.
		/// </summary>
		/// <remarks>
		/// See imgIContainer::unlockImage for documentation of the underlying call.
		/// </remarks>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void UnlockImage();
		
		/// <summary>
		/// If this image is unlocked, discard the image&apos;s decoded data.  If the image
		/// is locked or is already discarded, do nothing.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void RequestDiscard();
		
		/// <summary>
		/// If this request is for an animated image, the method creates a new
		/// request which contains the current frame of the image.
		/// Otherwise returns the same request.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		imgIRequest GetStaticRequest();
		
		/// <summary>
		/// Requests that the image animate (if it has an animation).
		/// </summary>
		/// <remarks>
		/// See Image::IncrementAnimationConsumers for documentation of the
		/// underlying call.
		/// </remarks>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void IncrementAnimationConsumers();
		
		/// <summary>
		/// Tell the image it can forget about a request that the image animate.
		/// </summary>
		/// <remarks>
		/// See Image::DecrementAnimationConsumers for documentation of the
		/// underlying call.
		/// </remarks>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void DecrementAnimationConsumers();
	}
}
