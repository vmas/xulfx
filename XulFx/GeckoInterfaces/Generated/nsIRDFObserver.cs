﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIRDFObserver.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	[ComImport]
	[Guid("3cc75360-484a-11d2-bc16-00805f912fe7")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIRDFObserver
	{
		
		/// <summary>
		/// This method is called whenever a new assertion is made
		/// in the data source
		/// </summary>
		/// <param name="aDataSource">
		/// the datasource that is issuing
		/// the notification.
		/// </param>
		/// <param name="aSource">
		/// the subject of the assertion
		/// </param>
		/// <param name="aProperty">
		/// the predicate of the assertion
		/// </param>
		/// <param name="aTarget">
		/// the object of the assertion
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnAssert([MarshalAs(UnmanagedType.Interface)] nsIRDFDataSource aDataSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aProperty, [MarshalAs(UnmanagedType.Interface)] nsIRDFNode aTarget);
		
		/// <summary>
		/// This method is called whenever an assertion is removed
		/// from the data source
		/// </summary>
		/// <param name="aDataSource">
		/// the datasource that is issuing
		/// the notification.
		/// </param>
		/// <param name="aSource">
		/// the subject of the assertion
		/// </param>
		/// <param name="aProperty">
		/// the predicate of the assertion
		/// </param>
		/// <param name="aTarget">
		/// the object of the assertion
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnUnassert([MarshalAs(UnmanagedType.Interface)] nsIRDFDataSource aDataSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aProperty, [MarshalAs(UnmanagedType.Interface)] nsIRDFNode aTarget);
		
		/// <summary>
		/// This method is called when the object of an assertion
		/// changes from one value to another.
		/// </summary>
		/// <param name="aDataSource">
		/// the datasource that is issuing
		/// the notification.
		/// </param>
		/// <param name="aSource">
		/// the subject of the assertion
		/// </param>
		/// <param name="aProperty">
		/// the predicate of the assertion
		/// </param>
		/// <param name="aOldTarget">
		/// the old object of the assertion
		/// </param>
		/// <param name="aNewTarget">
		/// the new object of the assertion
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnChange([MarshalAs(UnmanagedType.Interface)] nsIRDFDataSource aDataSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aProperty, [MarshalAs(UnmanagedType.Interface)] nsIRDFNode aOldTarget, [MarshalAs(UnmanagedType.Interface)] nsIRDFNode aNewTarget);
		
		/// <summary>
		/// This method is called when the subject of an assertion
		/// changes from one value to another.
		/// </summary>
		/// <param name="aDataSource">
		/// the datasource that is issuing
		/// the notification.
		/// </param>
		/// <param name="aOldSource">
		/// the old subject of the assertion
		/// </param>
		/// <param name="aNewSource">
		/// the new subject of the assertion
		/// </param>
		/// <param name="aProperty">
		/// the predicate of the assertion
		/// </param>
		/// <param name="aTarget">
		/// the object of the assertion
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnMove([MarshalAs(UnmanagedType.Interface)] nsIRDFDataSource aDataSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aOldSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aNewSource, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aProperty, [MarshalAs(UnmanagedType.Interface)] nsIRDFNode aTarget);
		
		/// <summary>
		/// This method is called when a datasource is about to
		/// send several notifications at once. The observer can
		/// use this as a cue to optimize its behavior. The observer
		/// can expect the datasource to call endUpdateBatch() when
		/// the group of notifications has completed.
		/// </summary>
		/// <param name="aDataSource">
		/// the datasource that is going to
		/// be issuing the notifications.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnBeginUpdateBatch([MarshalAs(UnmanagedType.Interface)] nsIRDFDataSource aDataSource);
		
		/// <summary>
		/// This method is called when a datasource has completed
		/// issuing a notification group.
		/// </summary>
		/// <param name="aDataSource">
		/// the datasource that has finished
		/// issuing a group of notifications
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void OnEndUpdateBatch([MarshalAs(UnmanagedType.Interface)] nsIRDFDataSource aDataSource);
	}
}
