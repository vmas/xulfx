﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file rdfITripleVisitor.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// Interface used in RDF to enumerate triples.
	/// Also used by rdfIDataSource::getAllSubjects, then aPredicate,
	/// aObject and aTruthValue are ignored.
	/// </summary>
	[ComImport]
	[Guid("aafea151-c271-4505-9978-a100d292800c")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface rdfITripleVisitor
	{
		
		/// <summary>
		/// Callback function for returning query results.
		/// </summary>
		/// <param name="aSubject,">
		/// aPredicate, aObject describe the (sub-)arc
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Visit([MarshalAs(UnmanagedType.Interface)] nsIRDFNode aSubject, [MarshalAs(UnmanagedType.Interface)] nsIRDFResource aPredicate, [MarshalAs(UnmanagedType.Interface)] nsIRDFNode aObject, [MarshalAs(UnmanagedType.U1)] bool aTruthValue);
	}
}
