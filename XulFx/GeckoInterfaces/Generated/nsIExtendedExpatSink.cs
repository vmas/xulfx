﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIExtendedExpatSink.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// This interface provides notification of syntax-level events.
	/// </summary>
	[ComImport]
	[Guid("5e3e4f0c-7b77-47ca-a7c5-a3d87f2a9c82")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIExtendedExpatSink : nsIExpatSink
	{
		
		#region nsIExpatSink Members

		
		/// <summary>
		/// Called to handle the opening tag of an element.
		/// </summary>
		/// <param name="aName">
		/// the fully qualified tagname of the element
		/// </param>
		/// <param name="aAtts">
		/// the array of attribute names and values.  There are
		/// aAttsCount/2 names and aAttsCount/2 values, so the total number of
		/// elements in the array is aAttsCount.  The names and values
		/// alternate.  Thus, if we number attributes starting with 0,
		/// aAtts[2*k] is the name of the k-th attribute and aAtts[2*k+1] is
		/// the value of that attribute  Both explicitly specified attributes
		/// and attributes that are defined to have default values in a DTD are
		/// present in aAtts.
		/// </param>
		/// <param name="aAttsCount">
		/// the number of elements in aAtts.
		/// </param>
		/// <param name="aLineNumber">
		/// the line number of the start tag in the data stream.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void HandleStartElement([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aName, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex=2)] System.IntPtr[] aAtts, uint aAttsCount, uint aLineNumber);
		
		/// <summary>
		/// Called to handle the closing tag of an element.
		/// </summary>
		/// <param name="aName">
		/// the fully qualified tagname of the element
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void HandleEndElement([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aName);
		
		/// <summary>
		/// Called to handle a comment
		/// </summary>
		/// <param name="aCommentText">
		/// the text of the comment (not including the
		/// &quot;&lt;!--&quot; and &quot;--&gt;&quot;)
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void HandleComment([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aCommentText);
		
		/// <summary>
		/// Called to handle a CDATA section
		/// </summary>
		/// <param name="aData">
		/// the text in the CDATA section.  This is null-terminated.
		/// </param>
		/// <param name="aLength">
		/// the length of the aData string
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void HandleCDataSection([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler), SizeParamIndex=1)] string aData, uint aLength);
		
		/// <summary>
		/// Called to handle the doctype declaration
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void HandleDoctypeDecl([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aSubset, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aName, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aSystemId, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aPublicId, [MarshalAs(UnmanagedType.Interface)] nsISupports aCatalogData);
		
		/// <summary>
		/// Called to handle character data.  Note that this does NOT get
		/// called for the contents of CDATA sections.
		/// </summary>
		/// <param name="aData">
		/// the data to handle.  aData is NOT NULL-TERMINATED.
		/// </param>
		/// <param name="aLength">
		/// the length of the aData string
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void HandleCharacterData([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler), SizeParamIndex=1)] string aData, uint aLength);
		
		/// <summary>
		/// Called to handle a processing instruction
		/// </summary>
		/// <param name="aTarget">
		/// the PI target (e.g. xml-stylesheet)
		/// </param>
		/// <param name="aData">
		/// all the rest of the data in the PI
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void HandleProcessingInstruction([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aTarget, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aData);
		
		/// <summary>
		/// Handle the XML Declaration.
		/// </summary>
		/// <param name="aVersion">
		/// The version string, can be null if not specified.
		/// </param>
		/// <param name="aEncoding">
		/// The encoding string, can be null if not specified.
		/// </param>
		/// <param name="aStandalone">
		/// -1, 0, or 1 indicating respectively that there was no
		/// standalone parameter in the declaration, that it was
		/// given as no, or that it was given as yes.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		new void HandleXMLDeclaration([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aVersion, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aEncoding, int aStandalone);
		
		/// <summary>
		/// Ask the content sink if the expat driver should log an error to the console.
		/// </summary>
		/// <param name="aErrorText">
		/// Error message to pass to content sink.
		/// </param>
		/// <param name="aSourceText">
		/// Source text of the document we&apos;re parsing.
		/// </param>
		/// <param name="aError">
		/// Script error object with line number &amp; column number
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		new bool ReportError([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aErrorText, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aSourceText, [MarshalAs(UnmanagedType.Interface)] nsIScriptError aError);
		
		#endregion

		
		/// <summary>
		/// Called at the beginning of the DTD, before any entity or notation
		/// events.
		/// </summary>
		/// <param name="aDoctypeName">
		/// The document type name.
		/// </param>
		/// <param name="aSysid">
		/// The declared system identifier for the external DTD subset,
		/// or null if none was declared.
		/// </param>
		/// <param name="aPubid">
		/// The declared public identifier for the external DTD subset,
		/// or null if none was declared.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void HandleStartDTD([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aDoctypeName, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aSysid, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aPubid);
		
		/// <summary>
		/// Called when a prefix mapping starts to be in-scope, before any
		/// startElement events.
		/// </summary>
		/// <param name="aPrefix">
		/// The Namespace prefix being declared. An empty string
		/// is used for the default element namespace, which has
		/// no prefix.
		/// </param>
		/// <param name="aUri">
		/// The Namespace URI the prefix is mapped to.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void HandleStartNamespaceDecl([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aPrefix, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aUri);
		
		/// <summary>
		/// Called when a prefix mapping is no longer in-scope, after any
		/// endElement events.
		/// </summary>
		/// <param name="aPrefix">
		/// The prefix that was being mapped. This is the empty string
		/// when a default mapping scope ends.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void HandleEndNamespaceDecl([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aPrefix);
		
		/// <summary>
		/// This is called for a declaration of notation.  The base argument is
		/// whatever was set by XML_SetBase. aNotationName will never be
		/// null. The other arguments can be.
		/// </summary>
		/// <param name="aNotationName">
		/// The notation name.
		/// </param>
		/// <param name="aSysId">
		/// The notation&apos;s system identifier, or null if none was given.
		/// </param>
		/// <param name="aPubId">
		/// The notation&apos;s pubilc identifier, or null if none was given.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void HandleNotationDecl([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aNotationName, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aSysid, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aPubid);
		
		/// <summary>
		/// This is called for a declaration of an unparsed (NDATA) entity.
		/// aName, aSysid and aNotationName arguments will never be
		/// null. The other arguments may be.
		/// </summary>
		/// <param name="aName">
		/// The unparsed entity&apos;s name.
		/// </param>
		/// <param name="aSysId">
		/// The notation&apos;s system identifier.
		/// </param>
		/// <param name="aPubId">
		/// The notation&apos;s pubilc identifier, or null if none was given.
		/// </param>
		/// <param name="aNotationName">
		/// The name of the associated notation.
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void HandleUnparsedEntityDecl([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aName, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aSysid, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aPubid, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string aNotationName);
	}
}
