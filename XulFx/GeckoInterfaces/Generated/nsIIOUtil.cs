﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIIOUtil.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// nsIIOUtil provdes various xpcom/io-related utility methods.
	/// </summary>
	[ComImport]
	[Guid("e8152f7f-4209-4c63-ad23-c3d2aa0c5a49")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIIOUtil
	{
		
		/// <summary>
		/// Test whether an input stream is buffered.  See nsStreamUtils.h
		/// documentation for NS_InputStreamIsBuffered for the definition of
		/// &quot;buffered&quot; used here and for edge-case behavior.
		/// </summary>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_INVALID_POINTER: if null is passed in.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool InputStreamIsBuffered([MarshalAs(UnmanagedType.Interface)] nsIInputStream aStream);
		
		/// <summary>
		/// Test whether an output stream is buffered.  See nsStreamUtils.h
		/// documentation for NS_OutputStreamIsBuffered for the definition of
		/// &quot;buffered&quot; used here and for edge-case behavior.
		/// </summary>
		/// <exception cref="System.Runtime.InteropServices.COMException">
		/// NS_ERROR_INVALID_POINTER: if null is passed in.
		/// </exception>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool OutputStreamIsBuffered([MarshalAs(UnmanagedType.Interface)] nsIOutputStream aStream);
	}
}
