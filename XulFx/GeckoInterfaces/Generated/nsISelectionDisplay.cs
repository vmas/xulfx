﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsISelectionDisplay.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsISelectionDisplay ( "0ddf9e1c-1dd2-11b2-a183-908a08aa75ae" ) interface
	// </summary>
	public sealed class nsISelectionDisplayConsts
	{
		
		public const short DISPLAY_TEXT = 1;
		
		public const short DISPLAY_IMAGES = 2;
		
		public const short DISPLAY_FRAMES = 4;
		
		public const short DISPLAY_ALL = 7;
	}
	
	[ComImport]
	[Guid("0ddf9e1c-1dd2-11b2-a183-908a08aa75ae")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsISelectionDisplay
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetSelectionFlags(short toggle);
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		short GetSelectionFlags();
	}
}
