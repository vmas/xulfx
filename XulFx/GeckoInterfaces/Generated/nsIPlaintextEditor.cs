﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIPlaintextEditor.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsIPlaintextEditor ( "b74fb158-1265-4102-91eb-edd0136b49f8" ) interface
	// </summary>
	public sealed class nsIPlaintextEditorConsts
	{
		
		public const int eEditorPlaintextMask = 1;
		
		public const int eEditorSingleLineMask = 2;
		
		public const int eEditorPasswordMask = 4;
		
		public const int eEditorReadonlyMask = 8;
		
		public const int eEditorDisabledMask = 16;
		
		public const int eEditorFilterInputMask = 32;
		
		public const int eEditorMailMask = 64;
		
		public const int eEditorEnableWrapHackMask = 128;
		
		public const int eEditorWidgetMask = 256;
		
		public const int eEditorNoCSSMask = 512;
		
		public const int eEditorAllowInteraction = 1024;
		
		public const int eEditorDontEchoPassword = 2048;
		
		public const int eEditorRightToLeft = 4096;
		
		public const int eEditorLeftToRight = 8192;
		
		public const int eEditorSkipSpellCheck = 16384;
		
		public const int eNewlinesPasteIntact = 0;
		
		public const int eNewlinesPasteToFirst = 1;
		
		public const int eNewlinesReplaceWithSpaces = 2;
		
		public const int eNewlinesStrip = 3;
		
		public const int eNewlinesReplaceWithCommas = 4;
		
		public const int eNewlinesStripSurroundingWhitespace = 5;
	}
	
	[ComImport]
	[Guid("b74fb158-1265-4102-91eb-edd0136b49f8")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIPlaintextEditor
	{
		
		/// <summary>
		/// The length of the contents in characters.
		/// XXX change this type to &apos;unsigned long&apos;
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetTextLengthAttribute();
		
		/// <summary>
		/// The maximum number of characters allowed.
		/// default: -1 (unlimited).
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetMaxTextLengthAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetMaxTextLengthAttribute(int value);
		
		/// <summary>
		/// Get and set the body wrap width.
		/// 
		/// Special values:
		/// 0 = wrap to window width
		/// -1 = no wrap at all
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetWrapWidthAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetWrapWidthAttribute(int value);
		
		/// <summary>
		/// Similar to the setter for wrapWidth, but just sets the editor
		/// internal state without actually changing the content being edited
		/// to wrap at that column.  This should only be used by callers who
		/// are sure that their content is already set up correctly.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetWrapColumn(int aWrapColumn);
		
		/// <summary>
		/// Get and set newline handling.
		/// 
		/// Values are the constants defined above.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		int GetNewlineHandlingAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetNewlineHandlingAttribute(int value);
		
		/// <summary>
		/// Inserts a string at the current location,
		/// given by the selection.
		/// If the selection is not collapsed, the selection is deleted
		/// and the insertion takes place at the resulting collapsed selection.
		/// </summary>
		/// <param name="aString">
		/// the string to be inserted
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void InsertText([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(AStringMarshaler))] nsAStringBase aStringToInsert);
		
		/// <summary>
		/// Insert a line break into the content model.
		/// The interpretation of a break is up to the implementation:
		/// it may enter a character, split a node in the tree, etc.
		/// This may be more efficient than calling InsertText with a newline.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void InsertLineBreak();
	}
}
