﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIClassInfo.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsIClassInfo ( "a60569d7-d401-4677-ba63-2aa5971af25d" ) interface
	// </summary>
	public sealed class nsIClassInfoConsts
	{
		
		public const uint SINGLETON = (1 << 0);
		
		public const uint THREADSAFE = (1 << 1);
		
		public const uint MAIN_THREAD_ONLY = (1 << 2);
		
		public const uint DOM_OBJECT = (1 << 3);
		
		public const uint PLUGIN_OBJECT = (1 << 4);
		
		public const uint SINGLETON_CLASSINFO = (1 << 5);
		
		public const uint CONTENT_NODE = (1 << 6);
		
		public const uint RESERVED = (1U << 31);
	}
	
	/// <summary>
	/// Provides information about a specific implementation class.  If you want
	/// your class to implement nsIClassInfo, see nsIClassInfoImpl.h for
	/// instructions--you most likely do not want to inherit from nsIClassInfo.
	/// </summary>
	[ComImport]
	[Guid("a60569d7-d401-4677-ba63-2aa5971af25d")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIClassInfo
	{
		
		/// <summary>
		/// Get an ordered list of the interface ids that instances of the class
		/// promise to implement. Note that nsISupports is an implicit member
		/// of any such list and need not be included.
		/// 
		/// Should set *count = 0 and *array = null and return NS_OK if getting the
		/// list is not supported.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetInterfaces(out uint count, out System.IntPtr array);
		
		/// <summary>
		/// Return an object to assist XPConnect in supplying JavaScript-specific
		/// behavior to callers of the instance object, or null if not needed.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsIXPCScriptable GetScriptableHelper();
		
		/// <summary>
		/// A contract ID through which an instance of this class can be created
		/// (or accessed as a service, if |flags &amp; SINGLETON|), or null.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))]
		string GetContractIDAttribute();
		
		/// <summary>
		/// A human readable string naming the class, or null.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))]
		string GetClassDescriptionAttribute();
		
		/// <summary>
		/// A class ID through which an instance of this class can be created
		/// (or accessed as a service, if |flags &amp; SINGLETON|), or null.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Struct)]
		UUID GetClassIDAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		uint GetFlagsAttribute();
		
		/// <summary>
		/// Also a class ID through which an instance of this class can be created
		/// (or accessed as a service, if |flags &amp; SINGLETON|).  If the class does
		/// not have a CID, it should return NS_ERROR_NOT_AVAILABLE.  This attribute
		/// exists so C++ callers can avoid allocating and freeing a CID, as would
		/// happen if they used classID.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Struct)]
		UUID GetClassIDNoAllocAttribute();
	}
}
