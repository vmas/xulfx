﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file mozISpellCheckingEngine.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// This interface represents a SpellChecker.
	/// </summary>
	[ComImport]
	[Guid("8ba643a4-7ddc-4662-b976-7ec123843f10")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface mozISpellCheckingEngine
	{
		
		/// <summary>
		/// The name of the current dictionary. Is either a value from
		/// getDictionaryList or the empty string if no dictionary is selected.
		/// Setting this attribute to a value not in getDictionaryList will throw
		/// NS_ERROR_FILE_NOT_FOUND.
		/// 
		/// If the dictionary is changed to no dictionary (the empty string), an
		/// observer is allowed to set another dictionary before it returns.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))]
		string GetDictionaryAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetDictionaryAttribute([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string value);
		
		/// <summary>
		/// The language this spellchecker is using when checking
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))]
		string GetLanguageAttribute();
		
		/// <summary>
		/// Does the engine provide its own personal dictionary?
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetProvidesPersonalDictionaryAttribute();
		
		/// <summary>
		/// Does the engine provide its own word utils?
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool GetProvidesWordUtilsAttribute();
		
		/// <summary>
		/// The name of the engine
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))]
		string GetNameAttribute();
		
		/// <summary>
		/// a string indicating the copyright of the engine
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))]
		string GetCopyrightAttribute();
		
		/// <summary>
		/// the personal dictionary
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		mozIPersonalDictionary GetPersonalDictionaryAttribute();
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void SetPersonalDictionaryAttribute([MarshalAs(UnmanagedType.Interface)] mozIPersonalDictionary value);
		
		/// <summary>
		/// Get the list of dictionaries
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void GetDictionaryList(out System.IntPtr dictionaries, out uint count);
		
		/// <summary>
		/// check a word
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool Check([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string word);
		
		/// <summary>
		/// get a list of suggestions for a misspelled word
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Suggest([MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(WStringMarshaler))] string word, out System.IntPtr suggestions, out uint count);
		
		/// <summary>
		/// Load dictionaries from the specified dir
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void LoadDictionariesFromDir([MarshalAs(UnmanagedType.Interface)] nsIFile dir);
		
		/// <summary>
		/// Add dictionaries from a directory to the spell checker
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void AddDirectory([MarshalAs(UnmanagedType.Interface)] nsIFile dir);
		
		/// <summary>
		/// Remove dictionaries from a directory from the spell checker
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void RemoveDirectory([MarshalAs(UnmanagedType.Interface)] nsIFile dir);
	}
}
