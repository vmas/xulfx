﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIDOMDOMException.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	// <summary>
	// Constants for nsIDOMDOMException ( "5bd766d3-57a9-4833-995d-dbe21da29595" ) interface
	// </summary>
	public sealed class nsIDOMDOMExceptionConsts
	{
		
		public const ushort INDEX_SIZE_ERR = 1;
		
		public const ushort DOMSTRING_SIZE_ERR = 2;
		
		public const ushort HIERARCHY_REQUEST_ERR = 3;
		
		public const ushort WRONG_DOCUMENT_ERR = 4;
		
		public const ushort INVALID_CHARACTER_ERR = 5;
		
		public const ushort NO_DATA_ALLOWED_ERR = 6;
		
		public const ushort NO_MODIFICATION_ALLOWED_ERR = 7;
		
		public const ushort NOT_FOUND_ERR = 8;
		
		public const ushort NOT_SUPPORTED_ERR = 9;
		
		public const ushort INUSE_ATTRIBUTE_ERR = 10;
		
		public const ushort INVALID_STATE_ERR = 11;
		
		public const ushort SYNTAX_ERR = 12;
		
		public const ushort INVALID_MODIFICATION_ERR = 13;
		
		public const ushort NAMESPACE_ERR = 14;
		
		public const ushort INVALID_ACCESS_ERR = 15;
		
		public const ushort VALIDATION_ERR = 16;
		
		public const ushort TYPE_MISMATCH_ERR = 17;
		
		public const ushort SECURITY_ERR = 18;
		
		public const ushort NETWORK_ERR = 19;
		
		public const ushort ABORT_ERR = 20;
		
		public const ushort URL_MISMATCH_ERR = 21;
		
		public const ushort QUOTA_EXCEEDED_ERR = 22;
		
		public const ushort TIMEOUT_ERR = 23;
		
		public const ushort INVALID_NODE_TYPE_ERR = 24;
		
		public const ushort DATA_CLONE_ERR = 25;
		
		public const ushort INVALID_POINTER_ERR = 26;
	}
	
	/// <summary>
	/// In general, DOM methods return specific error values in ordinary
	/// processing situations, such as out-of-bound errors.
	/// However, DOM operations can raise exceptions in &quot;exceptional&quot;
	/// circumstances, i.e., when an operation is impossible to perform
	/// (either for logical reasons, because data is lost, or because the
	/// implementation has become unstable)
	/// 
	/// For more information on this interface please see
	/// http://dvcs.w3.org/hg/domcore/raw-file/tip/Overview.html#domexception
	/// </summary>
	[ComImport]
	[Guid("5bd766d3-57a9-4833-995d-dbe21da29595")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIDOMDOMException
	{
		
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		ushort GetCodeAttribute();
	}
}
