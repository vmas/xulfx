﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsIBrowserGlue.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// nsIBrowserGlue is a dirty and rather fluid interface to host shared utility
	/// methods used by browser UI code, but which are not local to a browser window.
	/// The component implementing this interface is meant to be a singleton
	/// (service) and should progressively replace some of the shared &quot;glue&quot; code
	/// scattered in browser/base/content (e.g. bits of utilOverlay.js,
	/// contentAreaUtils.js, globalOverlay.js, browser.js), avoiding dynamic
	/// inclusion and initialization of a ton of JS code for *each* window.
	/// Dued to its nature and origin, this interface won&apos;t probably be the most
	/// elegant or stable in the mozilla codebase, but its aim is rather pragmatic:
	/// 1) reducing the performance overhead which affects browser window load;
	/// 2) allow global hooks (e.g. startup and shutdown observers) which survive
	/// browser windows to accomplish browser-related activities, such as shutdown
	/// sanitization (see bug #284086)
	/// </summary>
	[ComImport]
	[Guid("b0e7c156-d00c-4605-a77d-27c7418f23ae")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIBrowserGlue
	{
		
		/// <summary>
		/// Deletes privacy sensitive data according to user preferences
		/// </summary>
		/// <param name="aParentWindow">
		/// an optionally null window which is the parent of the
		/// sanitization dialog
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void Sanitize([MarshalAs(UnmanagedType.Interface)] nsIDOMWindow aParentWindow);
	}
}
