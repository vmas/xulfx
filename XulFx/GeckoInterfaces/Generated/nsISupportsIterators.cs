﻿#pragma warning disable 1591, 1572, 1573
// --------------------------------------------------------------------------------------------
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// 
// Software distributed under the License is distributed on an "AS IS" basis,
// WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
// for the specific language governing rights and limitations under the
// License.
// --------------------------------------------------------------------------------------------
// Generated by XPIDLImporter from file nsISupportsIterators.idl
// 
// You should use these interfaces when you access the COM objects defined in the mentioned
// IDL file.
// --------------------------------------------------------------------------------------------
namespace Gecko.Interfaces
{
	using System;
	using System.Runtime.InteropServices;
	using System.Runtime.CompilerServices;
	using SpiderMonkey;
	using Gecko.CustomMarshalers;
	
	
	/// <summary>
	/// ...
	/// </summary>
	[ComImport]
	[Guid("7330650e-1dd2-11b2-a0c2-9ff86ee97bed")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIOutputIterator
	{
		
		/// <summary>
		/// Put |anElementToPut| into the underlying container or sequence at the position currently pointed to by this iterator.
		/// The iterator and the underlying container or sequence cooperate to |Release()|
		/// the replaced element, if any and if necessary, and to |AddRef()| the new element.
		/// 
		/// The result is undefined if this iterator currently points outside the
		/// useful range of the underlying container or sequence.
		/// </summary>
		/// <param name="anElementToPut">
		/// the element to place into the underlying container or sequence
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void PutElement([MarshalAs(UnmanagedType.Interface)] nsISupports anElementToPut);
		
		/// <summary>
		/// Advance this iterator to the next position in the underlying container or sequence.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void StepForward();
	}
	
	/// <summary>
	/// ...
	/// </summary>
	[ComImport]
	[Guid("85585e12-1dd2-11b2-a930-f6929058269a")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIInputIterator
	{
		
		/// <summary>
		/// Retrieve (and |AddRef()|) the element this iterator currently points to.
		/// 
		/// The result is undefined if this iterator currently points outside the
		/// useful range of the underlying container or sequence.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports GetElement();
		
		/// <summary>
		/// Advance this iterator to the next position in the underlying container or sequence.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void StepForward();
		
		/// <summary>
		/// Test if |anotherIterator| points to the same position in the underlying container or sequence.
		/// 
		/// The result is undefined if |anotherIterator| was not created by or for the same underlying container or sequence.
		/// </summary>
		/// <param name="anotherIterator">
		/// another iterator to compare against, created by or for the same underlying container or sequence
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool IsEqualTo([MarshalAs(UnmanagedType.Interface)] nsISupports anotherIterator);
		
		/// <summary>
		/// Create a new iterator pointing to the same position in the underlying container or sequence to which this iterator currently points.
		/// The returned iterator is suitable for use in a subsequent call to |isEqualTo()| against this iterator.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports Clone();
	}
	
	/// <summary>
	/// ...
	/// </summary>
	[ComImport]
	[Guid("8da01646-1dd2-11b2-98a7-c7009045be7e")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIForwardIterator
	{
		
		/// <summary>
		/// Retrieve (and |AddRef()|) the element this iterator currently points to.
		/// 
		/// The result is undefined if this iterator currently points outside the
		/// useful range of the underlying container or sequence.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports GetElement();
		
		/// <summary>
		/// Put |anElementToPut| into the underlying container or sequence at the position currently pointed to by this iterator.
		/// The iterator and the underlying container or sequence cooperate to |Release()|
		/// the replaced element, if any and if necessary, and to |AddRef()| the new element.
		/// 
		/// The result is undefined if this iterator currently points outside the
		/// useful range of the underlying container or sequence.
		/// </summary>
		/// <param name="anElementToPut">
		/// the element to place into the underlying container or sequence
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void PutElement([MarshalAs(UnmanagedType.Interface)] nsISupports anElementToPut);
		
		/// <summary>
		/// Advance this iterator to the next position in the underlying container or sequence.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void StepForward();
		
		/// <summary>
		/// Test if |anotherIterator| points to the same position in the underlying container or sequence.
		/// 
		/// The result is undefined if |anotherIterator| was not created by or for the same underlying container or sequence.
		/// </summary>
		/// <param name="anotherIterator">
		/// another iterator to compare against, created by or for the same underlying container or sequence
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool IsEqualTo([MarshalAs(UnmanagedType.Interface)] nsISupports anotherIterator);
		
		/// <summary>
		/// Create a new iterator pointing to the same position in the underlying container or sequence to which this iterator currently points.
		/// The returned iterator is suitable for use in a subsequent call to |isEqualTo()| against this iterator.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports Clone();
	}
	
	/// <summary>
	/// ...
	/// </summary>
	[ComImport]
	[Guid("948defaa-1dd1-11b2-89f6-8ce81f5ebda9")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIBidirectionalIterator
	{
		
		/// <summary>
		/// Retrieve (and |AddRef()|) the element this iterator currently points to.
		/// 
		/// The result is undefined if this iterator currently points outside the
		/// useful range of the underlying container or sequence.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports GetElement();
		
		/// <summary>
		/// Put |anElementToPut| into the underlying container or sequence at the position currently pointed to by this iterator.
		/// The iterator and the underlying container or sequence cooperate to |Release()|
		/// the replaced element, if any and if necessary, and to |AddRef()| the new element.
		/// 
		/// The result is undefined if this iterator currently points outside the
		/// useful range of the underlying container or sequence.
		/// </summary>
		/// <param name="anElementToPut">
		/// the element to place into the underlying container or sequence
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void PutElement([MarshalAs(UnmanagedType.Interface)] nsISupports anElementToPut);
		
		/// <summary>
		/// Advance this iterator to the next position in the underlying container or sequence.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void StepForward();
		
		/// <summary>
		/// Move this iterator to the previous position in the underlying container or sequence.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void StepBackward();
		
		/// <summary>
		/// Test if |anotherIterator| points to the same position in the underlying container or sequence.
		/// 
		/// The result is undefined if |anotherIterator| was not created by or for the same underlying container or sequence.
		/// </summary>
		/// <param name="anotherIterator">
		/// another iterator to compare against, created by or for the same underlying container or sequence
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool IsEqualTo([MarshalAs(UnmanagedType.Interface)] nsISupports anotherIterator);
		
		/// <summary>
		/// Create a new iterator pointing to the same position in the underlying container or sequence to which this iterator currently points.
		/// The returned iterator is suitable for use in a subsequent call to |isEqualTo()| against this iterator.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports Clone();
	}
	
	/// <summary>
	/// ...
	/// </summary>
	[ComImport]
	[Guid("9bd6fdb0-1dd1-11b2-9101-d15375968230")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface nsIRandomAccessIterator
	{
		
		/// <summary>
		/// Retrieve (and |AddRef()|) the element this iterator currently points to.
		/// 
		/// The result is undefined if this iterator currently points outside the
		/// useful range of the underlying container or sequence.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports GetElement();
		
		/// <summary>
		/// Retrieve (and |AddRef()|) an element at some offset from where this iterator currently points.
		/// The offset may be negative.  |getElementAt(0)| is equivalent to |getElement()|.
		/// 
		/// The result is undefined if this iterator currently points outside the
		/// useful range of the underlying container or sequence.
		/// </summary>
		/// <param name="anOffset">
		/// a |0|-based offset from the position to which this iterator currently points
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports GetElementAt(int anOffset);
		
		/// <summary>
		/// Put |anElementToPut| into the underlying container or sequence at the position currently pointed to by this iterator.
		/// The iterator and the underlying container or sequence cooperate to |Release()|
		/// the replaced element, if any and if necessary, and to |AddRef()| the new element.
		/// 
		/// The result is undefined if this iterator currently points outside the
		/// useful range of the underlying container or sequence.
		/// </summary>
		/// <param name="anElementToPut">
		/// the element to place into the underlying container or sequence
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void PutElement([MarshalAs(UnmanagedType.Interface)] nsISupports anElementToPut);
		
		/// <summary>
		/// Put |anElementToPut| into the underlying container or sequence at the position |anOffset| away from that currently pointed to by this iterator.
		/// The iterator and the underlying container or sequence cooperate to |Release()|
		/// the replaced element, if any and if necessary, and to |AddRef()| the new element.
		/// |putElementAt(0, obj)| is equivalent to |putElement(obj)|.
		/// 
		/// The result is undefined if this iterator currently points outside the
		/// useful range of the underlying container or sequence.
		/// </summary>
		/// <param name="anOffset">
		/// a |0|-based offset from the position to which this iterator currently points
		/// </param>
		/// <param name="anElementToPut">
		/// the element to place into the underlying container or sequence
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void PutElementAt(int anOffset, [MarshalAs(UnmanagedType.Interface)] nsISupports anElementToPut);
		
		/// <summary>
		/// Advance this iterator to the next position in the underlying container or sequence.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void StepForward();
		
		/// <summary>
		/// Move this iterator by |anOffset| positions in the underlying container or sequence.
		/// |anOffset| may be negative.  |stepForwardBy(1)| is equivalent to |stepForward()|.
		/// |stepForwardBy(0)| is a no-op.
		/// </summary>
		/// <param name="anOffset">
		/// a |0|-based offset from the position to which this iterator currently points
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void StepForwardBy(int anOffset);
		
		/// <summary>
		/// Move this iterator to the previous position in the underlying container or sequence.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void StepBackward();
		
		/// <summary>
		/// Move this iterator backwards by |anOffset| positions in the underlying container or sequence.
		/// |anOffset| may be negative.  |stepBackwardBy(1)| is equivalent to |stepBackward()|.
		/// |stepBackwardBy(n)| is equivalent to |stepForwardBy(-n)|.  |stepBackwardBy(0)| is a no-op.
		/// </summary>
		/// <param name="anOffset">
		/// a |0|-based offset from the position to which this iterator currently points
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		void StepBackwardBy(int anOffset);
		
		/// <summary>
		/// Test if |anotherIterator| points to the same position in the underlying container or sequence.
		/// 
		/// The result is undefined if |anotherIterator| was not created by or for the same underlying container or sequence.
		/// </summary>
		/// <param name="anotherIterator">
		/// another iterator to compare against, created by or for the same underlying container or sequence
		/// </param>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.U1)]
		bool IsEqualTo([MarshalAs(UnmanagedType.Interface)] nsISupports anotherIterator);
		
		/// <summary>
		/// Create a new iterator pointing to the same position in the underlying container or sequence to which this iterator currently points.
		/// The returned iterator is suitable for use in a subsequent call to |isEqualTo()| against this iterator.
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall, MethodCodeType=MethodCodeType.Runtime)]
		[return: System.Runtime.InteropServices.MarshalAs(UnmanagedType.Interface)]
		nsISupports Clone();
	}
}
