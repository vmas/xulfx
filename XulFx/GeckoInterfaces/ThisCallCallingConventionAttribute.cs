﻿using System;

namespace Gecko.Interfaces
{
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class ThisCallCallingConventionAttribute : Attribute
	{

	}
}
