﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Gecko
{
	public class CreateServiceEventArgs : CancelEventArgs
	{
		private string _contractID;

		public CreateServiceEventArgs(string contractID)
		{
			_contractID = contractID;
		}

		public string ContractID
		{
			get { return _contractID; }
		}

	}
}
