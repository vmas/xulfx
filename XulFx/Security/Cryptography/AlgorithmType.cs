﻿using System;

namespace Gecko.Security.Cryptography
{
	public enum AlgorithmType : short
	{
		Rc4 = 1,
		AesCbc = 2,
		Hmac = 257
	}
}
