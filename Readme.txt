Xulfx is licensed under the Mozilla Public License Version.

== Changelog ==

== v52.9.1.0 ==

1. Various bug fixes.

== v52.9.0.0 ==

1. Update to Firefox SDK v52.9.0
2. Various bug fixes.

== v52.8.0.0 ==

1. Update to Firefox SDK v52.8.0

== v52.0.0.0 ==

1. Initial version supporting Firefox SDK v52.0

== v45.5.0.0 ==

1. Fix memory leak.
2. Various bug fixes.

== v45.0.1.0 ==

1. Add Scroll, Wheel, DOMWindowCreated event handlers.
2. Add the Request property to GeckoDocumentCompletedEventArgs, GeckoNavigatingEventArgs.

== v45.0.0.0 ==

1. Initial version supporting Firefox SDK v45.0

== v38.6.1.0 ==

1. Add overloads for graphics functions.
2. Update JavaScript version for sandboxed scripts to latest.
3. Fix GeckoXULWindow.Close().

== v38.6.0.0 ==

1. Add KeyboadEvent, TextInputProcessor, some parts of UIEvent.
2. Add the Navigate function overload that can take the Referrer Policy.
3. Fix memory leak (remove a reference to disposed WebViews from a context menu).
4. Add example how to observe the Console API output.
5. Tested with Gecko 38.0 - 38.6.

== v38.2.2.0 ==

1. Make Left, Right, Up, Down, Home, End, PageUp, PageDown keys work.
2. Fix possible URIFormatException's (.NET 3.5 SP1 can't parse URI's like http://123.��/).
3. Added some properties and methods.

== v38.2.1.0 ==

1. Added the MozWndProc() method to the WebView (Winforms).
2. Added FocusService (wrapper for nsIFocusManager).
3. Various bug fixes.

== v38.2.0.0 ==

1. Implemented an asynchronous read from the InputStream
2. Implemented the LoadContextInfo.
3. Improve JS-proxy implementation.
4. Various bug fixes.

== v38.0.5.0 ==

1. Implement XPath.

== v38.0.0.1 ==

1. Enable disk cache.
2. Update DownloadManager.
3. Various bug fixes.

== v38.0.0.0 ==

1. Initial version supporting firefox/xulrunner v38.0 (beta 8)

== v31.0.2.1 ==

1. Fix StackOverflowException

== v31.0.2.0 ==

1. Implements the DownloadManager, WindowlessView, TreeWalker, new Certificate overriding API.
2. Updated the JS interop.
3. Various bug fixes.

== v31.0.1.0 ==

1. Allows meta refresh.
2. Fix crash (after window.close()).

== v31.0.0.0 ==

1. Initial version supporting firefox/xulrunner v31.0

